<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="theme-color" content="#212529" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"/>
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Fresh gallery</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet" type="text/css">
{{--  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">--}}
{{--  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>--}}
{{--  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>--}}
{{--  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>--}}
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset('agency.css') }}" rel="stylesheet">
  <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">

</head>

<body id="page-top">

  <div class="d-flex" id="wrapper">

    <?php
    $article_uri = ['healthy_info','healthy_info/read/{id}','healthy_info/search/{keywords}','fresh_edu','fresh_edu/search/{keywords}','fresh_edu/read/{id}'];
    $beliProduk = ['/','product','product/category','product/category/{slug}','product/detail/{slug}','product/search/{key}','home'];
//      $navTop2 = ['product/detail/{slug}','product/category','product/category/{slug}','product/search/{key}','order/list/{param?}','people'];
    ?>

    <script>
      const AUTH = !!('{{ Auth::user() }}');
      const TOKEN = '{{ csrf_token() }}';
      const CURRENT_URI = '{{Route::current()->uri}}';
      const TEXT = {
        tandai_lokasi: 'Tandai lokasi dalam peta'
      }
    </script>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    @if(in_array(Route::current()->uri, $beliProduk))
      @include('components.sidebar')
    @endif
    @if(in_array(Route::current()->uri, $article_uri))
      @include('components.navbar_top_article')
    @else
      @include('components.navbar_top')
    @endif
    <div id="page-content-wrapper">
      @yield('content')
      @if(!(Route::current()->uri == 'about'))
        @include('components.footer')
      @endif
    </div>
    @if(in_array(Route::current()->uri, $beliProduk))
      @include('components.navbar_bottom')
    @endif
  </div>
  <script src="{{ asset('js/state.js') }}"></script>


  @if(in_array(Route::current()->uri, $beliProduk))
    <script src="{{ asset('js/beliProduk.js') }}"></script>
    <script>
        setTimeout(setInitialProduk, 1000);
    </script>
  @endif


  <!-- Plugin JavaScript -->
  <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Contact form JavaScript -->
  <script src="{{ asset('js/jqBootstrapValidation.js') }}"></script>
  <script src="{{ asset('js/contact_me.js') }}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset('js/agency.js') }}"></script>

  {{--Environment Variable--}}
  <script src="{{ asset('js/env.js') }}"></script>

  <script>
    $("#sidebar-closed").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    $("#sidebar-back").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
