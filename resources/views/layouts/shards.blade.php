<!doctype html>
<html class="no-js h-100" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Fresh Gallery</title>
  <meta name="description" content="Fresh Gallery">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="{{ asset('style_shards/shards-dashboards.1.1.0.min.css') }}">
  <link rel="stylesheet" href="{{ asset('style_shards/extras.1.1.0.min.css') }}">
  <script async defer src="https://buttons.github.io/buttons.js"></script>
</head>
<body class="h-100">
<div class="container-fluid">
{{--  @if(!in_array(Route::currentRouteName(), ['login','register','/']))--}}
    <div class="row">
      @include('components.shards.sidebar')

      <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        @include('components.shards.top_navbar')

        <div class="main-content-container container-fluid px-1">
          @yield('content')
        </div>

        @include('components.shards.footer')
      </main>
    </div>
{{--  @else--}}
{{--    <div class="vertical-center">--}}
{{--      @yield('content')--}}
{{--    </div>--}}
{{--  @endif--}}
</div>

<script>
    const AUTH = !!('{{ Auth::user() }}');
    const TOKEN = '{{ csrf_token() }}';
    const CURRENT_URI = '{{Route::current()->uri}}';
    const TEXT = {
        tandai_lokasi: 'Tandai lokasi dalam peta'
    }
</script>
<script src="{{ asset('js/state.js') }}"></script>

<script src="{{ asset('jquery/jquery.min.js') }}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
</body>
</html>

<style>
  .vertical-center {
    min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
    min-height: 100vh; /* These two lines are counted as one :-)       */

    display: flex;
    align-items: center;
  }
</style>