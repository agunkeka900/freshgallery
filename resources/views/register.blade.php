@extends('layouts.main')

@section('content')

  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <div class="card shadow-sm">
            {{--<div class="card-header">--}}
              {{--<h5 class="card-title mb-0 float-left">Register</h5>--}}
            {{--</div>--}}
            <div class="card-body">
              <div class="row">
                <img src="{{ asset('img/logo_vertikal.jpeg') }}" alt="logo" height="100px" class="mx-auto">
                {{--<div class="col-12">--}}
                  {{--<div class="col-lg-6 offset-lg-3 pt-4">--}}
                    {{--<a class="btn btn-lg btn-google btn-block text-uppercase btn-outline" href="{{ route('login.provider', 'google') }}" style="font-weight: normal; text-transform: none !important;">--}}
                      {{--<div class="row">--}}
                        {{--<div class="col-2"><img style="height: 25px; margin-top: -4px" src="https://d3nn82uaxijpm6.cloudfront.net/assets/website/landing-page/icon-google-d0e460839c4717a1dc562f7233331668a2674805b8b2df3eac5ec7fd2d6add46.svg"></div>--}}
                        {{--<div class="col-10">Sign up with Google</div>--}}
                      {{--</div>--}}
                    {{--</a>--}}
                  {{--</div>--}}
                {{--</div>--}}
              </div>
              <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf

                <div class="form-group row">
                  <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Name') }}</label>

                  <div class="col-md-6 col-sm-8">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                  <div class="col-md-6 col-sm-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <label for="password" class="col-sm-4 col-form-label text-md-right">{{ __('Password') }}</label>
                  <div class="col-md-6 col-sm-8">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" required>
                    @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <label for="password-confirm" class="col-sm-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                  <div class="col-md-6 col-sm-8">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-12 col-md-10">
                    <button type="submit" class="btn btn-primary float-right">
                      {{ __('Register') }}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <style>

    .btn-google {
      color: #545454;
      background-color: #ffffff;
      box-shadow: 0 1px 2px 1px #ddd;
      height: 54px;
      line-height: 38px;
    }
  </style>
@endsection