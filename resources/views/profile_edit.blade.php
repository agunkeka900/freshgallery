@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="row text-center">
      <div class="col"></div>
      <div class="col-md-4">
        <span class="fa-stack fa-4x">
          <i class="fas fa-circle fa-stack-2x text-primary"></i>
          <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
        </span>
        <form class="mt-5" method="post" action="{{ route('user/updateProfile') }}">
          {{ csrf_field() }}
          <div class="form-group row text-left">
            <label for="nama" class="col-sm-4 col-form-label">Nama</label>
            <div class="col-sm-8">
              <input type="text" id="nama" class="form-control" placeholder="Nama" name="nama" value="{{ $nama }}">
            </div>
          </div>
          <div class="form-group row text-left">
            <label for="gender" class="col-sm-4 col-form-label">Jenis kelamin</label>
            <div class="col-sm-8">
              <select class="form-control" id="gender" name="gender">
                <option disabled selected>Jenis kelamin</option>
                @foreach($itemGender as $i)
                  <option id="{{ 'gender'.$i['id'] }}" value="{{ $i['id'] }}">{{ $i['keterangan'] }}</option>
                @endforeach
              </select>
            </div>
            <script>
              document.getElementById('gender'+'{{ $data['id_gender'] }}').setAttribute('selected', '');
            </script>
          </div>
          <div class="form-group row text-left">
            <label for="tgl_lahir" class="col-sm-4 col-form-label">Tanggal lahir</label>
            <div class="col-sm-8">
              <input type="date" id="tgl_lahir" class="form-control" name="tgl_lahir" value="{{ $data['tgl_lahir'] }}">
            </div>
          </div>
          <div class="form-group row text-left">
            <label for="telp" class="col-sm-4 col-form-label">Nomor telpon</label>
            <div class="col-sm-8">
              <input type="text" id="telp" class="form-control" name="telp" value="{{ $data['telp'] }}">
            </div>
          </div>
          <div class="form-group row text-left">
            <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
            <div class="col-sm-8">
              <input type="text" id="alamat" class="form-control" name="alamat" value="{{ $data['alamat'] }}">
            </div>
          </div>
          <button type="submit" class="btn btn-primary float-right">Simpan</button>
          <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ route('user/getProfile') }}'">Kembali</button>
        </form>
      </div>
      <div class="col"></div>
    </div>
  </section>
@endsection