<title>Invoice</title>
<link rel="stylesheet" href="{{ URL::asset('css/pdf.css') }}">
{{--<body style="background: red">--}}
{{--<body style="background: url('{{ asset('img/logo_horizontal.jpg') }}'); background-repeat: no-repeat;">--}}
<body style="background: url('{{ asset('img/paid-stamp.png') }}'); background-repeat: no-repeat; background-position-x: 100px">
  <table
  {{--  border="1" --}}
    class="header">
    <tr>
      <td style="width: 60%">
        <table
  {{--        border="1"--}}
               class="container">
          <tr>
            <td colspan="2" style="padding-bottom: 10px !important;">
              <img src="{{ asset('img/logo_horizontal.jpg') }}" alt="" width="150px">
            </td>
          </tr>
          <tr>
            <td class="w130"><b>Nomor Invoice</b></td>
            <td class="font-primary"><b>{{ $trn['no_pesanan'] }}</b></td>
          </tr>
          <tr>
            <td class="font-9pt"><b>Atas nama</b></td>
            <td class="font-9pt">{{ $trn['nama'] }}</td>
          </tr>
          <tr>
            <td class="font-9pt"><b>Tanggal Pesan</b></td>
            <td class="font-9pt">{{ $trn['tgl_pesan'] }}</td>
          </tr>
          <tr>
            <td class="font-9pt"><b>Metode Pembayaran</b></td>
            <td class="font-9pt">{{ $trn['metode_bayar'] }}</td>
          </tr>
          <tr>
            <td class="font-9pt"><b>Catatan</b></td>
            <td class="font-9pt">{{ $trn['catatan'] ?: '-' }}</td>
          </tr>
        </table>
      </td>
      <td style="width: 40%">
        <table
  {{--                border="1"--}}
          class="container">
          <tr>
            <td><b>Tujuan Pengiriman:</b></td>
          </tr>
          <tr>
            <td class="font-9pt"><b>{{ $trn['label'] }}</b></td>
          </tr>
          <tr>
            <td class="font-9pt">
              {{ $trn['address'] }}
              <br> {{ ucwords(strtolower($trn['region'])) }}
  {{--            <br> 6282235821085--}}
            </td>
          </tr>
          <tr>
            <td class="font-9pt">Waktu kirim {{ $trn['waktu_kirim'] }}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table class="body">
    <tr>
      <th class="ta-l">Nama Produk</th>
      <th class="ta-r">Harga</th>
      <th>Satuan</th>
      <th>Jumlah</th>
      <th class="ta-r">Subtotal</th>
    </tr>
    @foreach($trnd as $t)
      <tr>
        <td>{{ $t['nama'] }}</td>
        <td class="ta-r">Rp {{ number_format($t['harga'], 0, ',', '.') }}</td>
        <td class="ta-c">{{ $t['satuan'] }}</td>
        <td class="ta-c">{{ number_format($t['qty'], 0, ',', '.') }}</td>
        <td class="ta-r">Rp {{ number_format($t['total_harga'], 0, ',', '.') }}</td>
      </tr>
      <tr>
        <td colspan="5" style="padding: 0 15px">
          <div style="border-bottom: #e0e0e0 thin solid; "></div>
        </td>
      </tr>
    @endforeach
    <tr>
      <td colspan="4" class="ta-r"><b>Total harga</b></td>
      <td class="ta-r"><b>Rp {{ number_format($trn['total_harga'], 0, ',', '.') }}</b></td>
    </tr>
  </table>
</body>