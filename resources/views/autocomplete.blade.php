<div class="autocomplete" style="width:300px;">
  <input id="myInput" type="text" name="myCountry" placeholder="Country">
  <input id="myInput_id" type="hidden" name="myCountry_id" placeholder="Country">
</div>

<link href="{{ asset('css/autocomplete.css') }}" rel="stylesheet">
<script src="{{ asset('js/autocomplete.js') }}"></script>
<script>
    let city = JSON.parse('{!! $city !!}');
    autocomplete(document.getElementById("myInput"), city);
</script>