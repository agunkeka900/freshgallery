@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col"></div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header text-center">
              {{--<h5 class="card-title mb-0">Profile</h5>--}}
              <span class="fa-stack fa-4x">
                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                <i class="fas fa-user fa-stack-1x fa-inverse"></i>
              </span>
              <h4 class="service-heading">{{ $nama }}</h4>
            </div>
            <div class="card-body">
              <table class="table">
                <tbody>
                @foreach($data as $d)
                  <tr>
                    <th scope="row" class="text-md-left">{{ $d[0] }}</th>
                    @if($d[1] != "")
                      <td class="text-md-left">{{ $d[1] }}</td>
                    @else
                      <td class="text-md-left text-secondary">Belum ditentukan</td>
                    @endif
                  </tr>
                @endforeach
                </tbody>
              </table>
              <button type="button" class="btn btn-primary float-right"
                      onclick="location.href = '{{ route('user/getEditProfile') }}'">Edit profile</button>
            </div>
          </div>
        </div>
        <div class="col"></div>
      </div>
    </div>
  </section>
@endsection