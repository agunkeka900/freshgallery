@extends('layouts.main')

@section('content')

  <section class="page-section">
    <div class="container">
      <div class="col-lg-8 mx-auto">
        <div class="card shadow-sm">
          {{--<div class="card-header">--}}
            {{--<h5 class="card-title mb-0 float-left">Login</h5>--}}
          {{--</div>--}}
          <div class="card-body">
            <div class="row">
              <img src="{{ asset('img/logo_vertikal.jpeg') }}" alt="logo" height="100px" class="mx-auto">
              {{--<div class="col-12 font-weight-bold text-center py-4">Login</div>--}}
              <div class="col-12 mt-4">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                  @csrf

                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right" for="email">{{ __('Email') }}</label>
                    <div class="col-sm-8 col-md-6">
                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                             name="email" value="{{ old('email') }}" required autofocus>
                      @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="password" class="col-sm-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-sm-8 col-md-6">
                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                             name="password" required>
                      @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-6 offset-sm-4">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                          {{ __('Remember Me') }}
                        </label>
                      </div>
                    </div>
                  </div>

                  <input id="redirect" type="hidden" name="redirect" value="">

                  <div class="form-group row">
                    <div class="col-sm-12 col-md-10">
                      <button type="submit" class="btn btn-primary float-right">
                        {{ __('Login') }}
                      </button>
                      <a class="btn btn-white float-right font-weight-normal text-secondary mr-3" href="{{ url('people/view/forgot-password') }}">Lupa password?</a>
                    </div>
                    {{--<div class="col-md-6 offset-md-4 pt-4">--}}
                      {{--<a class="btn btn-lg btn-google btn-block text-uppercase btn-outline" href="{{ route('login.provider', 'google') }}" style="font-weight: normal; text-transform: none !important;">--}}
                        {{--<div class="row">--}}
                          {{--<div class="col-2"><img style="height: 25px; margin-top: -4px" src="https://d3nn82uaxijpm6.cloudfront.net/assets/website/landing-page/icon-google-d0e460839c4717a1dc562f7233331668a2674805b8b2df3eac5ec7fd2d6add46.svg"></div>--}}
                          {{--<div class="col-10">Log in using Google</div>--}}
                        {{--</div>--}}
                      {{--</a>--}}
                    {{--</div>--}}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
    let param = window.location.pathname;
    if(param.includes('redirect=')){
        param = param.split('redirect=')[1];
        document.getElementById('redirect').value = param;
    }
  </script>
  <style>

    .btn-google {
      color: #545454;
      background-color: #ffffff;
      box-shadow: 0 1px 2px 1px #ddd;
      height: 54px;
      line-height: 38px;
    }
  </style>
@endsection