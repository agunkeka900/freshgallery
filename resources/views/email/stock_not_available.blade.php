<html>
<head>
  <style>
    *{
      font-family: 'Nunito', sans-serif;
      font-size: 11pt;
      color: #333;
    }
    a{
      text-decoration: none;
    }
    .container{
      width: 600px;
      margin-left: auto;
      margin-right: auto;
      border: 1px solid #ddd;
      border-radius: 15px;
      padding: 50px;
      /*box-shadow: 0 1px 6px 0 rgba(0,0,0,0.2) !important;*/
    }
    .table-activity{
      border-spacing: 0;
    }
    .table-activity tr th{
      border-top: 1px solid #333;
      border-bottom: 1px solid #333;
      border-left: 1px solid #333;
      padding: 7px;
      font-size: 10pt !important;
    }
    .table-activity tr th:last-child{
      border-right: 1px solid #333;
    }
    .table-activity tr td{
      border-bottom: 1px solid #333;
      border-left: 1px solid #333;
      padding: 5px;
      text-align: right;
      font-size: 10pt !important;
    }
    .table-activity tr td:last-child{
      border-right: 1px solid #333;
    }
  </style>
</head>
<body>
<table class="container">
  <tr>
    <td align="center" colspan="3">
      <img src="http://freshgallery-bali.epizy.com/public/img/logo_vertikal.jpeg?i=1" alt="logo" width="150px" style="margin-bottom: 50px">
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <div style="padding-bottom: 20px; color: black; font-size: 16pt; font-weight: bold">Informasi Pesanan</div>
    </td>
  </tr>
  <tr>
    <td colspan="3">Hallo <b>{{ $trn['nama'] }}</b> </td>
  </tr>
  <tr>
    <td colspan="3" style="font-size: 12pt; padding-bottom: 20px">Mohon maaf, semua produk yang anda pesan <b>tidak tersedia!</b></td>
  </tr>

  <tr>
    <td>Nomor pesanan</td>
    <td style="with: 10px">: </td>
    <td><b>{{ $trn['no_pesanan'] }}</b></td>
  </tr>
  <tr>
    <td>Kirim ke</td>
    <td>: </td>
    <td><b>{{ $trn['label'] }}</b></td>
  </tr>
  <tr>
    <td>Alamat pengiriman</td>
    <td>: </td>
    <td><b>{{ $trn['address'] }}</b></td>
  </tr>
  <tr>
    <td>Wilayah</td>
    <td>: </td>
    <td><b>{{ ucwords(strtolower($trn['region'])) }}</b></td>
  </tr>
  <tr>
    <td>Waktu Pengiriman</td>
    <td>: </td>
    <td><b>{{ $trn['waktu_kirim'] }}</b></td>
  </tr>
  <tr>
    <td>Estimasi Pengiriman</td>
    <td>: </td>
    <td><b>{{ $trn['estimasi_pengiriman'] }}</b></td>
  </tr>
  <tr>
    <td>Catatan</td>
    <td>: </td>
    <td><b>{{ $trn['catatan'] ?: '-' }}</b></td>
  </tr>
  <tr>
    <td>Metode pembayaran</td>
    <td>: </td>
    <td><b>{{ $trn['metode_bayar'] }}</b></td>
  </tr>

  <tr>
    <td colspan="3" style="padding-top: 20px">
      <table class="table-activity" style="width: 100%">
        <tr>
          <th>Nama produk</th>
          <th>Harga</th>
          <th>Satuan</th>
          <th>Jumlah</th>
          <th style="border-right:1px solid #333">Ketersediaan</th>
        </tr>
        @foreach($trnd as $t)
          <tr>
            <td style="text-align: left">{{ $t['nama'] }}</td>
            <td>Rp {{ number_format($t['harga'], 0, ',', '.') }}</td>
            <td style="text-align: left">{{ $t['satuan'] }}</td>
            <td>{{ number_format($t['qty'], 0, ',', '.') }}</td>
            <td style="border-right:1px solid #333"><font style="color: red;">Tidak tersedia</font></td>
          </tr>
        @endforeach
        {{--<tr>--}}
          {{--<td colspan="4" style="text-align: right; border: none"><b>Total harga :</b></td>--}}
          {{--<td style="border-right:1px solid #333"><b>Rp {{ number_format($trn['total_harga'], 0, ',', '.') }}</b></td>--}}
        {{--</tr>--}}
      </table>
    </td>
  </tr>

  {{--    Footer--}}
  <tr>
    <td colspan="3">
      <div style="width: 140px; margin: auto; padding-top: 50px; padding-bottom: 20px; border-bottom: 1px solid #b5b5b5">
      {{--<div style="width: 250px; margin: auto; padding-top: 50px; padding-bottom: 20px; border-bottom: 1px solid #b5b5b5">--}}
        <a href="https://web.facebook.com/Fresh-Gallery-Bali-286563148707066" target="_blank" style="margin-left: 35px">
          <img src="https://app.bibit.id/assets/images/facebook-circle-gray.png" width="35px" alt="facebook" style="margin-right: 10px">
        </a>
        <a href="https://www.instagram.com/fresh.gallery.bali" target="_blank">
          <img src="https://app.bibit.id/assets/images/instagram-circle-gray.png" width="35px" alt="instagram" style="margin-right: 10px">
        </a>
        {{--<a href="http://twitter.com/#!/stikomersbali" target="_blank">--}}
          {{--<img src="https://app.bibit.id/assets/images/twitter-circle-outline-gray.png" width="35px" alt="twitter" style="margin-right: 10px">--}}
        {{--</a>--}}
        {{--<a href="https://stikom-bali.ac.id/" target="_blank">--}}
          {{--<img src="https://app.bibit.id/assets/images/web-circle-gray.png" width="35px" alt="www">--}}
        {{--</a>--}}
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="3" style="text-align: center; padding-top: 20px; color: #b5b5b5; font-weight: bold; font-size: 10pt">
      © {{ date('Y') }} Fresh Gallery Bali
    </td>
  </tr>
</table>
</body>
</html>
