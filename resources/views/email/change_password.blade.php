<html>
<head>
  <style>
    *{
      font-family: 'Nunito', sans-serif;
      font-size: 11pt;
      color: #333;
    }
    a{
      text-decoration: none;
    }
    .container{
      width: 600px;
      margin-left: auto;
      margin-right: auto;
      border: 1px solid #ddd;
      border-radius: 15px;
      padding: 50px;
      /*box-shadow: 0 1px 6px 0 rgba(0,0,0,0.2) !important;*/
    }
    .table-activity{
      border-spacing: 0;
    }
    .table-activity tr th{
      border-top: 1px solid #333;
      border-bottom: 1px solid #333;
      border-left: 1px solid #333;
      padding: 7px;
      font-size: 10pt !important;
    }
    .table-activity tr th:last-child{
      border-right: 1px solid #333;
    }
    .table-activity tr td{
      border-bottom: 1px solid #333;
      border-left: 1px solid #333;
      padding: 5px;
      text-align: right;
      font-size: 10pt !important;
    }
    .table-activity tr td:last-child{
      border-right: 1px solid #333;
    }
  </style>
</head>
<body>
<table class="container">
  <tr>
    <td align="center" colspan="3">
      <img src="http://freshgallery-bali.epizy.com/public/img/logo_vertikal.jpeg?i=1" alt="logo" width="150px" style="margin-bottom: 50px">
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <div style="padding-bottom: 20px; color: black; font-size: 16pt; font-weight: bold">{{ $title }} Fresh Gallery</div>
    </td>
  </tr>
  <tr>
    <td colspan="3">Hallo {{ $name }}</td>
  </tr>
  <tr>
    <td colspan="3" style="font-size: 12pt; padding-bottom: 20px">Silahkan klik link berikut untuk mengubah password</td>
  </tr>
  <tr>
    <td colspan="3" style="font-size: 12pt; padding-bottom: 20px">
      <a href="http://203.128.79.238/inventory/backup/public/people/view/change-password/{{ substr(md5(time()), 0, 31).$id }}" style="color: blue" target="_blank">
      {{--<a href="http://127.0.0.1/freshGallery/public/people/view/change-password/{{ substr(md5(time()), 0, 31).$id }}" style="color: blue" target="_blank">--}}
        http://203.128.79.238/inventory/backup/public/people/view/change-password/{{ substr(md5(time()), 0, 31).$id }}
        {{--http://127.0.0.1/freshGallery/public/people/view/change-password/{{ substr(md5(time()), 0, 31).$id }}--}}
      </a>
    </td>
  </tr>

  

  {{--    Footer--}}
  <tr>
    <td colspan="3">
      <div style="width: 140px; margin: auto; padding-top: 50px; padding-bottom: 20px; border-bottom: 1px solid #b5b5b5">
        {{--<div style="width: 250px; margin: auto; padding-top: 50px; padding-bottom: 20px; border-bottom: 1px solid #b5b5b5">--}}
        <a href="https://web.facebook.com/Fresh-Gallery-Bali-286563148707066" target="_blank" style="margin-left: 35px">
          <img src="https://app.bibit.id/assets/images/facebook-circle-gray.png" width="35px" alt="facebook" style="margin-right: 10px">
        </a>
        <a href="https://www.instagram.com/fresh.gallery.bali" target="_blank">
          <img src="https://app.bibit.id/assets/images/instagram-circle-gray.png" width="35px" alt="instagram" style="margin-right: 10px">
        </a>
        {{--<a href="http://twitter.com/#!/stikomersbali" target="_blank">--}}
        {{--<img src="https://app.bibit.id/assets/images/twitter-circle-outline-gray.png" width="35px" alt="twitter" style="margin-right: 10px">--}}
        {{--</a>--}}
        {{--<a href="https://stikom-bali.ac.id/" target="_blank">--}}
        {{--<img src="https://app.bibit.id/assets/images/web-circle-gray.png" width="35px" alt="www">--}}
        {{--</a>--}}
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="3" style="text-align: center; padding-top: 20px; color: #b5b5b5; font-weight: bold; font-size: 10pt">
      © {{ date('Y') }} Fresh Gallery Bali
    </td>
  </tr>
</table>
</body>
</html>