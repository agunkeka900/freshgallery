@extends('layouts.main')

@section('content')
  <!-- Header -->
  <header class="masthead">
    <div class="container">
      <div class="intro-text">
        <div class="intro-heading text-uppercase">Fresh Gallery</div>
        <div class="intro-lead-in">Fresh for Everyone</div>
        <a class="btn btn-outline-primary btn-xl text-uppercase js-scroll-trigger" href="#produk">lihat produk kami</a>
      </div>
    </div>
  </header>

  <!-- Produk -->
  <section class="page-section" id="produk" style="padding-top: 50px">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Produk</h2>
          <h3 class="section-subheading text-muted">3 jenis produk unggulan kami</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-lg-10 col-sm-12 mx-auto">
          <div class="col-md-4 float-left pl-2 pr-2 pb-4">
            <div class="card shadow-sm mx-auto col-md-12 col-9">
              <div class="card-body">
                <img class="rounded-circle img-fluid mx-auto" width="150px" src="{{ asset('img/buah.jpg') }}" alt="">
                <h4 class="service-heading">Buah</h4>
                <p class="text-muted">Fresh Gallery hadir dengan berbagai jenis buah hasil panen lokal pertanian</p>
              </div>
              <div class="card-footer bg-white border-0">
                <a href="{{ url('product/category/buah-segar') }}">
                  <button class="btn btn-outline-primary">Selengkapnya</button>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-4 float-left pl-2 pr-2 pb-4">
            <div class="card shadow-sm mx-auto col-md-12 col-9">
              <div class="card-body">
                <img class="rounded-circle img-fluid mx-auto" width="150px" src="{{ asset('img/sayur.jpg') }}" alt="">
                <h4 class="service-heading">Sayur</h4>
                <p class="text-muted">Berbagai sayuran tersedia disini dan terjamin kesegarannya</p>
              </div>
              <div class="card-footer bg-white border-0">
                <a href="{{ url('product/category/sayuran') }}">
                  <button class="btn btn-outline-primary">Selengkapnya</button>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-4 float-left pl-2 pr-2 pb-4">
            <div class="card shadow-sm mx-auto col-md-12 col-9">
              <div class="card-body">
                <img class="rounded-circle img-fluid mx-auto" width="150px" src="{{ asset('img/bibit.jpg') }}" alt="">
                <h4 class="service-heading">Bibit</h4>
                <p class="text-muted">Selain buah dan sayuran, kami juga menyediakan berbagai bibit berkualitas</p>
              </div>
              <div class="card-footer bg-white border-0">
                <a href="{{ url('product/category/bibit') }}">
                  <button class="btn btn-outline-primary">Selengkapnya</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <!-- Tentang kami -->
  <section class="page-section" id="about" style="padding-top: 70px">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center pb-4">
          <h2 class="section-heading text-uppercase">Tentang kami</h2>
          {{--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>--}}
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 pr-5 pl-5">
          <div class="timeline-body">
            <p class="text-muted">Fresh Gallery memiliki visi untuk menjadi layanan terbaik bagi petani lokal dalam memperbaiki sektor pertanian dengan pemanfaatan teknologi. Maka dari itu, kami memegang semangat "Fresh for Everyone", dimana kesegaran dalam hal apapun adalah hak semua orang.</p>
          </div>
          <div class="timeline-body">
            <p class="text-muted">Misi kami: Menyediakan akses pasar dan pengembangan teknologi di bidang pertanian, menjaga kualitas produk pertanian, serta menyediakan informasi untuk petani muda karena menjadi petani itu keren.</p>
          </div>
          <div class="timeline-body">
            <p class="text-muted">Untuk kedepannya, kami akan mengembangkan Agri-tech untuk memaksimalkan kualitas pertanian lokal karena kita adalah negara Agraris, petani adalah ujung tombak perekonomian kita.</p>
          </div>
        </div>
        <div class="col-sm-6 pl-5 pr-5">
          <div class="timeline-body">
            <p class="text-muted">Fresh Gallery terfokus pada produk buah dan sayur yang terjaga kualitasnya dan merupakan hasil pertanian lokal. Tim kami dengan setia akan mengantarkan hasil pertanian yang masih segar langsung ke konsumen. </p>
          </div>
          <div class="timeline-body">
            <p class="text-muted">Fresh Gallery hadir untuk menjawab mimpi-mimpi petani untuk menikmati hasil yang adil dari hasil kerja keras mereka. Kami juga akan turut serta bekerja keras dalam melestarikan generasi petani.</p>
          </div>
          <div class="timeline-body">
            <p class="text-muted">Dengan semangat "Fresh for Everyone", kami harap semangat ini juga ada pada diri Anda. Mari bergabung bersama kami untuk mengembalikan nama bahwa Indonesia adalah negara agraris.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Contact -->
  <section class="page-section" id="contact" style="padding-top: 180px; padding-bottom: 50px">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Kontak kami</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-9 col-sm-8 col-md-6 col-lg-5 offset-2 offset-sm-3 offset-md-4 offset-lg-4 p-2">
          <a href="https://web.facebook.com/Fresh-Gallery-Bali-286563148707066/" target="_blank" class="row text-white">
            <div class="col-1 offset-lg-1">
              <i class="fab fa-facebook-f"></i>
            </div>
            <div class="col">
              Fresh Gallery Bali
            </div>
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-9 col-sm-8 col-md-6 col-lg-5 offset-2 offset-sm-3 offset-md-4 offset-lg-4 p-2">
          <a href="https://www.instagram.com/fresh.gallery.bali/" target="_blank" class="row text-white">
            <div class="col-1 offset-lg-1">
              <i class="fab fa-instagram"></i>
            </div>
            <div class="col">
              fresh.gallery.bali
            </div>
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-9 col-sm-8 col-md-6 col-lg-5 offset-2 offset-sm-3 offset-md-4 offset-lg-4 p-2">
          <a href="https://wa.me/6285964271302" target="_blank" class="row text-white">
            <div class="col-1 offset-lg-1">
              <i class="fab fa-whatsapp"></i>
            </div>
            <div class="col">
              085964271302
            </div>
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-9 col-sm-8 col-md-6 col-lg-5 offset-2 offset-sm-3 offset-md-4 offset-lg-4 p-2">
          <a href="mailto:freshgallery.bali@gmail.com" target="_blank" class="row text-white">
            <div class="col-1 offset-lg-1">
              <i class="far fa-envelope"></i>
            </div>
            <div class="col">
              freshgallery.bali@gmail.com
            </div>
          </a>
        </div>
      </div>
      <div class="row text-center text-white">
        <div class="col" style="padding-top: 150px">
          <span class="copyright">Copyright &copy; Fresh Gallery 2019</span>
        </div>
      </div>
    </div>
  </section>
@endsection