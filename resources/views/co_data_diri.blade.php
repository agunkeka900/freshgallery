@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title mb-0">Data Anda</h5>
          </div>
          <div class="card-body">
            <form method="post" action="{{ route('checkout/saveDataDiri') }}">
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              {{ csrf_field() }}
              <div class="form-group row text-left">
                <label for="nama" class="col-12 col-form-label text-left">Nama</label>
                <div class="col-12">
                  <input type="text" id="nama" class="form-control" placeholder="Nama" name="nama" value="{{ old('nama') }}">
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="email" class="col-12 col-form-label text-left">Email</label>
                <div class="col-12">
                  <input type="text" id="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="telp" class="col-12 col-form-label text-left">Nomor Telpon</label>
                <div class="col-12">
                  <input type="text" id="telp" class="form-control" placeholder="Nomor Telpon" name="telp" value="{{ old('telp') }}">
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="gender" class="col-12 col-form-label">Jenis kelamin</label>
                <div class="col-12">
                  <select class="form-control" id="gender" name="gender">
                    <option disabled selected>Jenis kelamin</option>
                    @foreach($itemGender as $i)
                      <option id="{{ 'gender'.$i['id'] }}" value="{{ $i['id'] }}">{{ $i['keterangan'] }}</option>
                    @endforeach
                  </select>
                  <script>document.getElementById('gender'+'{{ old('gender') }}').setAttribute('selected','')</script>
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="tgl_lahir" class="col-12 col-form-label">Tanggal lahir</label>
                <div class="col-12">
                  <input type="date" id="tgl_lahir" class="form-control" name="tgl_lahir" value="{{ old('tgl_lahir') }}">
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="alamat" class="col-12 col-form-label">Alamat</label>
                <div class="col-12">
                  <input type="text" id="alamat" class="form-control" name="alamat" value="{{ old('nama') }}">
                </div>
              </div>
              <button type="submit" class="btn btn-primary float-right">Lanjut ke pembayaran</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12"></div>
    </div>
  </section>
@endsection
