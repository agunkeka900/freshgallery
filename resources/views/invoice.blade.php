<html>
<head>
  <title>Invoice</title>
</head>
<body>
<table class="wrapper" cellspacing="0">
  <tr>
    <td style="width: 150px">Nama Pelanggan</td>
    <td>: {{ $trn['nama'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Nomor telpon</td>
    <td>: {{ $trn['telp'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Email</td>
    <td>: {{ $trn['email'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Waktu pengiriman</td>
    <td>: {{ $trn['waktu_kirim'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Alamat pengiriman</td>
    <td>: {{ $trn['alamat_kirim'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Catatan</td>
    <td>: {{ $trn['alamat_kirim'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Metode pembayaran</td>
    <td>: {{ $trn['metode_bayar'] }}</td>
  </tr>
  <tr>
    <td style="width: 150px">Total pembayaran</td>
    <td>: {{ $trn['total_harga'] }}</td>
  </tr>
  <tr>
    <td colspan="2">
      <table class="barang" cellspacing="0">
        <tr>
          <th>No</th>
          <th>Nama produk</th>
          <th>Harga</th>
          <th>Jumlah</th>
          <th class="br">Sub total</th>
        </tr>
        @foreach($trnd as $t)
          <tr>
            <td class="ta-c">{{ $no++ }}</td>
            <td>{{ $t['nama'] }}</td>
            <td class="ta-r">{{ $t['harga'] }}</td>
            <td class="ta-c">{{ $t['qty'] }}</td>
            <td class="br ta-r">{{ $t['total_harga'] }}</td>
          </tr>
        @endforeach
        <tr>
          <td colspan="4" style="border: none"></td>
          <td class="ta-r" style="font-weight: bold; border: none;">{{ $trn['total_harga'] }}</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>

<style>
  *{
    font-family: sans-serif;
  }
  .wrapper{
    margin-left: auto;
    margin-right: auto;
    width: 600px;
    border: 1px solid grey;
    border-radius: 5px;
    padding: 20px;
  }
  .wrapper tr td{
    padding: 7px;
  }
  .barang{
    width: 100%;
    margin-top: 20px;
  }
  .barang tr th{
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-bottom: 1px solid black;
    padding: 5px;
  }
  .barang tr td{
    border-bottom: 1px solid black;
    border-left: 1px solid black;
    padding: 5px;
    font-size: 11pt;
  }
  .br{
    border-right: 1px solid black;
  }
  .ta-c{
    text-align: center;
  }
  .ta-r{
    text-align: right;
  }
</style>