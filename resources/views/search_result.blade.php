@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container" style="max-width: 1140px">
      <div class="row px-2">
        @if($len)
          <h5>Hasil pencarian "{{ Route::current()->parameters()['key'] }}"</h5>
        @else
          <h5>Produk terkait "{{ Route::current()->parameters()['key'] }}"</h5>
        @endif
      </div>
      <div class="row mb-5 px-1">
        @foreach(($len ? $data : $terkait) as $k)
          @include('components.item_produk')
        @endforeach
      </div>
    </div>
  </section>
@endsection