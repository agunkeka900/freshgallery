<div class="modal fade bd-example-modal-lg" id="order-detail-modal" tabindex="-1" role="dialog" aria-labelledby="order-detail-modal-title" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="order-detail-modal-title">Detail Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="order-detail-modal-close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="order-detail-modal-content">
      </div>
    </div>
  </div>
</div>