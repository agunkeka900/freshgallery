{{--{{ dd($data) }}--}}
@foreach($data as $d)
  <div class="card card-produk mb-4" style="min-height: 100px; width: 100%">
    <div class="row mx-0">
      <div class="col-12 border-bottom py-2 px-3 font-12px">{{ $d['trn']['tgl_pesan'] }}</div>
    </div>

    <div class="row mx-0 py-3 border-bottom">
      <div class="col-md-4 col-sm-6 border-right px-1 py-1">
        <div class="col-12 font-12px">Nomor Pesanan</div>
        <div class="col-12 font-14px font-weight-bold text-primary font-14px">{{ $d['trn']['no_pesanan'] }}</div>
      </div>
      <div class="col-md-4 col-sm-6 border-right px-1 py-1 font-dg">
        <div class="col-12 font-12px">Status</div>
        <div class="col-12 font-14px font-weight-bold">{{ $d['trn']['status'] }}</div>
      </div>
      <div class="col-md-4 col-sm-6 px-1 py-1 font-dg">
        <div class="col-12 font-12px">Total Belanja</div>
        <div class="col-12 font-16px font-weight-bold text-primary">Rp {{ number_format($d['trn']['total_harga'], 0, ',', '.') }}</div>
      </div>
    </div>

    @foreach($d['trnd'] as $trnd)
      <div class="row py-3 mx-3 border-bottom font-dg">
        <div class="col-md-5 col-sm-12 container-produk px-0">
          <div style="width: 56px; float: left">
            <div class="img-produk" style="width: 56px; height: 56px; float: left; background-image: url('{{ asset('img_produk/'.$trnd['foto']) }}');"></div>
          </div>
          <div style="float: left">
            <div class="font-weight-bold font-14px px-3">
              {{ $trnd['nama'] }}
              @if($trnd['status'] == 5)
                <font style="color: #E60050; font-size: 12px">(tidak tersedia)</font>
              @endif
            </div>
            <div class="font-12px px-3">Rp {{ number_format($trnd['harga'], 0, ',', '.') }} / {{ $trnd['satuan'] }}</div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 px-0">
          <div style="float: left; height: 100%" class="border-right container-jumlah">
            <div class="col-12 font-12px">Jumlah</div>
            <div class="col-12 font-14px font-weight-bold">{{ $trnd['qty'] }}</div>
          </div>
          <div style="float: left">
            <div class="col-12 font-12px">Total harga produk</div>
            <div class="col-12 font-14px font-weight-bold">Rp {{ number_format($trnd['total_harga'], 0, ',', '.') }}</div>
          </div>
        </div>
      </div>
    @endforeach

    <div class="row mx-0">
      <div class="col-12 p-3">
        <div style="float: left; width: 240px; cursor: pointer" id="order-list-btn-detail-{{ $d['trn']['id'] }}">
          <i class="fas fa-eye font-dg" style="font-size: 20px; float: left; width: 25px"></i>
          <div class="font-14px font-dg pl-2" style="width: 200px; float: left" onclick="showOrderDetail('{{ $d['trn']['id'] }}', '{{url('order/detail')}}')">Lihat Detail Pesanan</div>
        </div>
        <div class="spinner-border spinner-border-sm" role="status"
             id="order-list-loading-detail-{{ $d['trn']['id'] }}"
             style="color: gray; border-width: 0.15em; display: none;"></div>
        @if(in_array($d['trn']['flag_status'], [6]))
          <div style="float: left; width: 230px; cursor: pointer" id="order-list-btn-continue-{{ $d['trn']['id'] }}">
            <i class="fas fa-check font-dg" style="font-size: 20px; float: left; width: 25px"></i>
            <div class="font-14px font-dg pl-2" style="width: 200px; float: left" onclick="continueOrder('{{ $d['trn']['id'] }}', '{{url('')}}')">Lanjutkan Pesanan</div>
          </div>
        @endif
        @if(in_array($d['trn']['flag_status'], [1,2,6]))
          <div style="float: left; width: 230px; cursor: pointer" id="order-list-btn-cancel-{{ $d['trn']['id'] }}">
            <i class="fas fa-times font-dg" style="font-size: 20px; float: left; width: 25px"></i>
            <div class="font-14px font-dg pl-2" style="width: 200px; float: left" onclick="cancelOrder('{{ $d['trn']['id'] }}', '{{url('')}}')">Batalkan Pesanan</div>
          </div>
        @endif
      </div>
    </div>
  </div>
@endforeach
@if(count($data) == 0)
  <div class="row">
    <div class="col-12 text-center">Data tidak tersedia</div>
  </div>
@endif

<style>
  .container-produk{
    border-right-color:rgb(222, 226, 230);
    border-right-style:solid;
    border-right-width:1px;
  }
  .container-jumlah{
    padding: 0;
  }
  @media (max-width: 768px){
    .container-produk{
      border-right: none;
    }
    .container-jumlah{
      padding-left: 58px;
    }
  }
</style>