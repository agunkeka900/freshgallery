<div class="container-fluid">
  {{--Nomor Invoice--}}
  <div class="row mx-0 px-0 pb-3 border-bottom">
    <div class="col-6 px-0">
      <div class="row mx-0">
        <div class="col-12 px-0 font-12px font-dgl">
          Nomor Invoice
        </div>
        <div class="col-12 px-0 font-14px text-primary font-weight-bold">
          {{ $trn['no_pesanan'] }}
        </div>
        <div class="col-12 px-0 font-12px font-dgl pt-3">
          Status
        </div>
        <div class="col-12 px-0 font-14px font-dg font-weight-bold">
          {{ $trn['status'] }}
        </div>
        <div class="col-12 px-0 font-12px font-dgl pt-3">
          Tanggal Pesan
        </div>
        <div class="col-12 px-0 font-14px font-dg font-weight-bold">
          {{ $trn['tgl_pesan'] }}
        </div>
      </div>
    </div>
    <div class="col-6 px-0">
      <div class="row mx-0">
        <div class="col-12 px-0">
          @if(Auth::user()->role_user == '3' && $trn['flag_status'] != '4')
            <button class="btn btn-primary float-right" style="width: 150px"
                    onclick="orderCompleted('{{ $trn['id'] }}', '{{ url('') }}')">Selesai</button>
          @elseif(Auth::user()->role_user != '3' && $trn['flag_status'] == '4')
            <button class="btn btn-primary float-right" style="width: 150px">Beli lagi</button>
          @endif
        </div>
      </div>
    </div>
  </div>

  {{--Daftar produk--}}
  <div class="row mx-0 px-0 border-bottom pt-3">
    <div class="col-6 px-0 font-weight-bold font-12px font-dg">
      Daftar produk
    </div>
    <div class="col-6 font-weight-bold font-12px font-dg">
      Harga produk
    </div>
    @foreach($trnd as $t)
    <div class="col-12 px-0">
      <div class="row py-3 mx-0 font-dg" style="border-bottom: thin rgb(224, 224, 224) dashed">
        <div class="col-6 border-right px-0">
          <div class="img-produk" style="width: 43px; height: 43px; float: left; background-image: url('{{ asset('img_produk/'.$t['foto']) }}');"></div>
          <div style="float: left">
            <div class="font-weight-bold font-12px px-3">
              {{ $t['nama'] }}
              @if($t['status'] == 5)
                <font style="color: #E60050; font-size: 12px">(tidak tersedia)</font>
              @endif
            </div>
            <div class="font-12px px-3">{{ $t['qty'] }} Produk x Rp {{ number_format($t['harga'], 0, ',', '.') }}</div>
          </div>
        </div>
        <div class="col-6 px-0">
          <div class="col-12 font-12px font-weight-bold text-primary">Rp {{ number_format($t['total_harga'], 0, ',', '.') }}</div>
        </div>
      </div>
    </div>
    @endforeach
  </div>

  {{--Pengiriman--}}
  <div class="row mx-0 px-0 border-bottom py-3">
    <div class="col-12 px-0 pb-2 font-weight-bold font-12px font-dg">
      Pengiriman
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Kirim ke</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">{{ $trn['label'] }}</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Alamat Pengiriman</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">{{ $trn['address'] }}</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Wilayah</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">{{ $trn['region'] }}</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Waktu Pengiriman</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">{{ $trn['waktu_kirim'] }}</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Estimasi Pengiriman</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">{{ $trn['estimasi_pengiriman'] }}</div>
    </div>
  </div>

  {{--Pembayaran--}}
  <div class="row mx-0 px-0 border-bottom py-3">
    <div class="col-12 px-0 pb-2 font-weight-bold font-12px font-dg">
      Pembayaran
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Total harga ({{ $trn['qty_total'] }} Produk)</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">Rp {{ number_format($trn['total_harga'], 0, ',', '.') }}</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Total Ongkos Kirim</div>
      <div class="font-dg font-12px font-weight-bold float-left py-1 pl-2">Bebas Ongkir</div>
    </div>
    <div class="col-12 py-0 px-0">
      <div style="width: 200px;" class="font-dg font-12px float-left border-right py-1">Total Bayar</div>
      <div class="font-12px font-weight-bold float-left py-1 pl-2 text-primary">Rp {{ number_format($trn['total_harga'], 0, ',', '.') }}</div>
    </div>
  </div>
</div>