<div class="modal fade bd-example-modal-lg" id="order-filter-modal" tabindex="-1" role="dialog" aria-labelledby="order-filter-modal-title" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="order-filter-modal-title">Filter Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="order-filter-modal-close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12 py-0 px-0 border-right">
            <div class="form-group text-left mb-0">
              <label for="order-list-filter-status" class="col-12 pt-0 col-form-label text-left">Filter Status</label>
              <div class="col-12 py-0">
                <select class="form-control" id="order-list-filter-status" onchange="debounceFilterOrder('{{url('')}}')">
                  <option value="all" selected>Semua</option>
                  @foreach($statuses as $s)
                    <option value="{{ $s['status'] }}" {{ $status == $s['status'] ? 'selected' : '' }}>{{ $s['keterangan2'] }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-12 py-3 border-right">
            <div class="form-group text-left row mb-0">
              <label for="order-list-filter-start-date" class="col-6 pt-0 pr-1 col-form-label text-left">Filter Tanggal Awal</label>
              <label for="order-list-filter-end-date" class="col-6 pt-0 pl-1 col-form-label text-left">Filter Tanggal Akhir</label>
              <div class="col-6 pr-1 py-0">
                <input type="date" class="form-control" id="order-list-filter-start-date" value="{{ $initial_date }}" oninput="debounceFilterOrder('{{url('')}}')">
              </div>
              <div class="col-6 pl-1 py-0">
                <input type="date" class="form-control" id="order-list-filter-end-date" value="{{ date('Y-m-d') }}" oninput="debounceFilterOrder('{{url('')}}')">
              </div>
            </div>
          </div>
          <div class="col-12 py-0 px-0">
            <div class="form-group text-left mb-0">
              <label for="order-list-filter-search" class="col-12 pt-0 col-form-label text-left">Cari</label>
              <div class="col-12 py-0">
                <input type="text" class="form-control" id="order-list-filter-search" placeholder="Cari Nama Barang & No Pesanan" oninput="debounceFilterOrder('{{url('')}}')">
              </div>
            </div>
          </div>
          {{--<div class="col-12 pt-3 px-3">--}}
            {{--<button class="btn btn-primary float-right">Filter</button>--}}
          {{--</div>--}}
        </div>
      </div>
    </div>
  </div>
</div>