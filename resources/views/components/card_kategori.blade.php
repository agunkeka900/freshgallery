<div class="col-kat">
  <a href="{{ url('product/category/'.$k['slug']) }}">
{{--  <a href="{{ $k['route'] != null ? url($k['route'], ['id' => $k['param']]) : '#' }}">--}}
    <div class="card-kat">
      <div class="row">
        <div class="icon-kat">
          @if(strpos($k['icon'], '.') === false)
            <i class="{{ $k['icon'] }}"></i>
          @else
            <img src="{{ asset('img/'.$k['icon']) }}" alt="img_kategori" class="img-kat">
          @endif
        </div>
        <table class="text-kat">
          <tr>
            <td>{{ $k['nama'] }}</td>
          </tr>
        </table>
      </div>
    </div>
  </a>
</div>