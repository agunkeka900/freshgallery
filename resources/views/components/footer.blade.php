<section class="page-section border-top pt-3 pb-5">
  <div class="container-fluid pb-3" style="padding-left: 10px; padding-right: 10px; max-width: 1140px;">
    <div class="row">
      <div class="col-6 col-sm-4 col-md-3" style="line-height: 30px">
        <span class="font-weight-bold" style="font-size: 16px">Fresh Gallery</span><br>
        <a href="{{ url('about') }}" class="text-secondary">Tentang Fresh Gallery</a><br>
        <a href="{{ url('healthy_info') }}" class="text-secondary">Healthy Info</a><br>
        <a href="{{ url('fresh_edu') }}" class="text-secondary">Fresh Edu</a><br>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2" style="line-height: 30px">
        <div class="font-weight-bold col-12" style="font-size: 16px">Ikuti Kami</div>
        <div class="font-weight-bold col-12">
          <a href="https://web.facebook.com/Fresh-Gallery-Bali-286563148707066/" target="_blank">
            <img src="https://ecs7.tokopedia.net/assets-tokopedia-lite/v2/zeus/production/90f8d7cf.svg" alt="Logo Facebook">
          </a>
          <a href="https://www.instagram.com/fresh.gallery.bali/" target="_blank">
            <img src="https://ecs7.tokopedia.net/assets-tokopedia-lite/v2/zeus/production/6bbf7298.svg" alt="Logo Instagram">
          </a>
        </div>
      </div>
      <div class="col-sm-2 col-md-2 hide-kategori">
        <span class="font-weight-bold" style="font-size: 16px">Kategori</span><br>
        <a href="{{ url('product/category/buah-segar') }}" class="text-secondary">Buah Segar</a><br>
        <a href="{{ url('product/category/sayuran') }}" class="text-secondary">Sayuran</a><br>
        <a href="{{ url('product/category/tanaman-hias') }}" class="text-secondary">Tanaman Hias</a><br>
        <a href="{{ url('product/category/bibit-buah') }}" class="text-secondary">Bibit Buah</a><br>
        <a href="{{ url('product/category/bibit-sayur') }}" class="text-secondary">Bibit Sayur</a><br>
      </div>
      <div class="col-sm-2 col-md-2 hide-kategori">
        <span class="font-weight-bold" style="font-size: 16px; color: rgba(0, 0, 0, 0)">Kategori</span><br>
        <a href="{{ url('product/category/tanaman-herbal') }}" class="text-secondary">Tanaman Herbal</a><br>
        <a href="{{ url('product/category/jamur') }}" class="text-secondary">Jamur</a><br>
        <a href="{{ url('product/category/produk-olahan') }}" class="text-secondary">Produk Olahan</a><br>
        <a href="{{ url('product/category/baru') }}" class="text-secondary">Baru</a><br>
        <a href="{{ url('product/category/terlaris') }}" class="text-secondary">Terlaris</a><br>
      </div>
      <div class="col-12 col-sm-4 col-md-3 text-secondary text-center pt-4">
        <div class="mx-auto pb-2" style="width: fit-content">
          <img src="{{ asset('img/logo_horizontal.jpg') }}" height="55px" alt="logo_horizontal" >
        </div>
        <span>© {{ date('Y') }}  Fresh Gallery</span>
      </div>
    </div>
  </div>
</section>

<style>
  @media(max-width: 720px){
    .hide-kategori{
      display: none;
    }
  }
</style>