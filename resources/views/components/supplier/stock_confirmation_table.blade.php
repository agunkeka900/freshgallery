<table class="table table-sm mb-0">
  <thead class="bg-light">
  <tr>
    <th scope="col" class="border-0" style="min-width: 35px">No pesanan</th>
    <th scope="col" class="border-0">Nama pemesan</th>
    <th scope="col" class="border-0">Tanggal</th>
    <th scope="col" class="border-0">Nama produk</th>
    <th scope="col" class="border-0">Jumlah</th>
    <th scope="col" class="border-0">Satuan</th>
    <th scope="col" class="text-right border-0" style="min-width: 110px;">Aksi</th>
  </tr>
  </thead>
  <tbody>
  @foreach($confirmation_lists as $i=>$lk)
    @for($b=0; $b<count($lk['product']); $b++)
      @php $brg = $lk['product']; @endphp
      <tr>
        @if($b==0)
          <td id="td1-{{$i}}" class="border-right" rowspan="{{ count($lk['product']) }}" onmouseenter="hoverTable('td1-{{$i}}')" onmouseleave="hoverTable('td1-{{$i}}', false)">{{ $lk['no_pesanan'] }}</td>
          <td id="td2-{{$i}}" class="border-right" rowspan="{{ count($lk['product']) }}" onmouseenter="hoverTable('td2-{{$i}}')" onmouseleave="hoverTable('td2-{{$i}}', false)">{{ $lk['nama_pemesan'] }}</td>
          <td id="td3-{{$i}}" class="border-right" rowspan="{{ count($lk['product']) }}" onmouseenter="hoverTable('td3-{{$i}}')" onmouseleave="hoverTable('td3-{{$i}}', false)">{{ $lk['tanggal'] }}</td>
        @endif
        <td id="td4-{{$i.'-'.$b}}" class="border-right" onmouseenter="hoverTable('td4-{{$i.'-'.$b}}')" onmouseleave="hoverTable('td4-{{$i.'-'.$b}}', false)">{{ $brg[$b]['nama'] }}</td>
        <td id="td5-{{$i.'-'.$b}}" class="border-right" onmouseenter="hoverTable('td5-{{$i.'-'.$b}}')" onmouseleave="hoverTable('td5-{{$i.'-'.$b}}', false)">{{ $brg[$b]['qty'] }}</td>
        <td id="td6-{{$i.'-'.$b}}" class="border-right" onmouseenter="hoverTable('td6-{{$i.'-'.$b}}')" onmouseleave="hoverTable('td6-{{$i.'-'.$b}}', false)">{{ $brg[$b]['satuan'] }}</td>
        <td id="td7-{{$i.'-'.$b}}" class="text-right" onmouseenter="hoverTable('td7-{{$i.'-'.$b}}')" onmouseleave="hoverTable('td7-{{$i.'-'.$b}}', false)">
        <button type="submit"
                id="stock-confirmation-btn-reject-{{$brg[$b]['id']}}"
                class="btn btn-sm  btn-outline-danger float-right"
                style="font-size: 16px; padding: 0 4px"
                onclick="reject({{$brg[$b]['id']}})">
          <i class="material-icons">close</i>
        </button>
        <button type="button"
                id="stock-confirmation-btn-confirm-{{$brg[$b]['id']}}"
                class="btn btn-sm btn-outline-primary float-right mr-1"
                style="font-size: 12px; padding: 3px 4px"
                onclick="confirm({{$brg[$b]['id']}})">
          Konfirmasi
        </button>
        </td>
      </tr>
    @endfor
  @endforeach
  </tbody>
</table>