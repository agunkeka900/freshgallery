<div class="col-produk">
  <div class="card card-produk">
    <a href="{{ url('product/detail/'.$k['slug']) }}" style="cursor: pointer">
{{--    <a href="{{ route('produk/detail', ['id' => $k['slug']]) }}" style="cursor: pointer">--}}
      <div class="card-img img-produk" style="background-image: url('{{ asset('img_produk/'.$k['foto']) }}');"></div>
      <div class="card-body p-2" style="line-height: 0; height: 99px">
        <div class="mb-0 box-nama">{{ $k['nama'] }}</div>
        <p class="card-text mb-0 box-harga">
          <span class="text-primary font-harga">Rp {{  number_format($k['harga'],0,",",".") }}</span>
          <span class="text-secondary" style="font-size: 10px">/ {{ $k['satuan'] }}</span>
        </p>
        <p class="card-text mb-0 box-tempat">
          <i class="fas fa-map-marker-alt"></i> {{ $k['tempat'] }}
        </p>
      </div>
    </a>
    <div class="card-body p-1">
      <button
        onclick="tambahBeli({{ $k }})"
        class="btn btn-primary"
        style="width: 100%; text-transform: none"
        id="btnBeli{{$k['id']}}">
        <i class="fas fa-shopping-cart pr-2"></i>Beli
      </button>
      <div class="row" id="row{{ $k['id'] }}" style="display: none">
        <div class="col-4 pr-0">
          <button
            onclick="kurangBeli({{ $k }})"
            style="width: 100%; background: #ebebeb"
            class="btn">
            <i class="fas fa-minus text-secondary"></i>
          </button>
        </div>
        <div class="col-4 px-1">
          <table style="width: 100%">
            <tr>
              <td height="34px">
                <input type="number" value="0" id="jumlah{{ $k['id'] }}"
                       onfocusout="setJumlahPerItemCart({{ $k }}, 'item')"
                       class="jumlah-item-produk">
              </td>
            </tr>
          </table>
        </div>
        <div class="col-4 pl-0">
          <button
            onclick="tambahBeli({{ $k }})"
            style="width: 100%; background: #ebebeb"
            class="btn">
            <i class="fas fa-plus text-secondary"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>