<div class="bg-white" id="sidebar-wrapper">
  <div class="sidebar-heading bg-primary text-white h5 mb-0 pb-0" style="height: 59px">
    <div class="float-left pt-1">Keranjang Belanja</div>
    <div class="float-right pt-1 btn-back">
      <i class="fas fa-arrow-left" id="sidebar-back"></i>
    </div>
  </div>
  <div class="list-group list-group-flush">
  </div>
  <div class="container-isi ml-2 overflow-auto" id="cartIsi"></div>
  <div class="container-checkout">
    <table width="100%" style="height: 60px;">
      <tr>
        <td width="60px" style="text-align: center; vertical-align: middle">
          <i class="fas fa-shopping-cart" style="font-size: 20px;"> </i>
          <span id="cartJumlah2" style="font-size: 14px;"></span>
        </td>
        <td class="border-left pl-2">
          <span id="cartHarga2" style="font-weight: 600"></span>
        </td>
        <td>
          <a href="{{ url('checkout/guest') }}" id="btn-checkout" class="btn btn-primary float-right mr-2"
             style="text-transform: none">
            Checkout
          </a>
          <div id="btn-checkout-loading"
               class="spinner-border spinner-border-sm float-left ml-2" role="status"
               style="color: gray; border-width: 0.15em; display: none; margin-top: 10px"></div>
        </td>
      </tr>
    </table>
  </div>
</div>
<div class="bg-dark" id="sidebar-closed"></div>


