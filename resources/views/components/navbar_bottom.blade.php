<nav class="navbar fixed-bottom navbar-light bg-white" id="navbarBottom" style="display: none; z-index: 98;">
  <div class="container px-0" id="menu-toggle">
    <div class="row px-0 mx-0" style="width: 100%">
      <table width="100%">
        <tr>
          <td width="80px">
            <i class="fas fa-shopping-cart" style="font-size: 30px;"> </i>
            <span id="cartJumlah"></span>
          </td>
          <td class="border-left pl-4">
            <span id="cartHarga"> </span>
          </td>
          <td>
            <button class="btn btn-primary float-right mr-0" style="text-transform: none">Lanjut</button>
          </td>
        </tr>
      </table>
    </div>
  </div>
</nav>
<style>
  .navbar{
    -webkit-box-shadow: 0 -3px 3px 0 rgba(0,0,0,0.1);
    -moz-box-shadow: 0 -3px 3px 0 rgba(0,0,0,0.1);
    box-shadow: 0 -3px 3px 0 rgba(0,0,0,0.1);
  }
</style>