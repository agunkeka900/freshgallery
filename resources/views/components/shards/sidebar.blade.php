<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
  <div class="main-navbar">
    <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0" style="height: 60px">
{{--      <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">--}}
        <div class="d-table m-auto">
{{--          <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="images/shards-dashboards-logo.svg" alt="Shards Dashboard">--}}
{{--          <span class="d-none d-md-inline ml-1">SMS Broadcast</span>--}}
        </div>
{{--      </a>--}}
      <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
        <i class="material-icons">&#xE5C4;</i>
      </a>
    </nav>
  </div>
  <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
    <div class="input-group input-group-seamless ml-3">
      <div class="input-group-prepend">
        <div class="input-group-text">
          <i class="fas fa-search"></i>
        </div>
      </div>
      <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
  </form>
  <div class="nav-wrapper">
    @if(\Illuminate\Support\Facades\Auth::user()->role_user == '3')
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('admin/dashboard') }}">
            <i class="material-icons">fastfood</i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('waiting-list/admin') }}">
            <i class="material-icons">fastfood</i>
            <span>Waiting List</span>
          </a>
        </li>
        <li class="pt-3 pl-4" style="font-weight: bold">
          Data Master
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('product/admin') }}">
            <i class="material-icons">fastfood</i>
            <span>Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('product-type/admin') }}">
            <i class="material-icons">category</i>
            <span>Jenis Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('supplier/admin') }}">
            <i class="material-icons">store_mall_directory</i>
            <span>Supplier</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('article/admin') }}">
            <i class="material-icons">menu_book</i>
            <span>Artikel</span>
          </a>
        </li>
        <li class="pt-3 pl-4" style="font-weight: bold">
          Transaksi
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('supplier/stock-confirmation') }}">
            <i class="material-icons">fastfood</i>
            <span>Konfirmasi Stok</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('transaction/admin') }}">
            <i class="material-icons">fastfood</i>
            <span>Semua Transaksi</span>
          </a>
        </li>
      </ul>
    @elseif(\Illuminate\Support\Facades\Auth::user()->role_user == '4')
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('supplier/dashboard') }}">
            <i class="material-icons">fastfood</i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('supplier/stock-confirmation') }}">
            <i class="material-icons">fastfood</i>
            <span>Konfirmasi Stok</span>
          </a>
        </li>
        <li class="pt-3 pl-4" style="font-weight: bold">
          Data Master
        </li>
        <li class="nav-item">
          <a class="nav-link " id="menu-laporan" href="{{ url('product/admin') }}">
            <i class="material-icons">fastfood</i>
            <span>Produk</span>
          </a>
        </li>
      </ul>
    @endif
  </div>
</aside>

<script>
  const route = '{{ Route::current()->uri }}';
  let arrId = [
    {
      id: 'menu-home',
      route: ['home']
    },{
      id: 'menu-calon',
      route: ['calon','calon/add']
    },{
      id: 'menu-sekolah',
      route: ['sekolah','sekolah/add']
    },{
      id: 'menu-sms',
      route: ['sms']
    },{
      id: 'menu-email',
      route: []
    },{
      id: 'menu-laporan',
      route: []
    }
  ];
  for(let i=0; i<arrId.length; i++){
      for(let p=0; p<arrId[i].route.length; p++){
          if(arrId[i].route[p] === route){
              document.getElementById(arrId[i].id).classList.add('active');
          }
      }
  }
</script>