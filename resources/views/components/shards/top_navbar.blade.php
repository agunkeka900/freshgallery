<div class="main-navbar sticky-top bg-white" style="height: 60px">
  <!-- Main Navbar -->
  <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
    <div class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
      <div class="input-group input-group-seamless ml-3">
        <div style="
          width: 120px;
          height: 35px;
          margin-top: 13px;
          background-image: url({{ asset('img/logo_horizontal.jpg') }});
          background-size: cover;
          background-repeat: no-repeat;">
        </div>
        <div style="
        font-weight: bold;
        line-height: 20px;
        padding-left: 10px;
        padding-top: 15px;">
        </div>
      </div>
    </div>
    <ul class="navbar-nav border-left flex-row " style="height: 60px">
{{--      Logout--}}
      <li class="nav-item dropdown" style="min-width: 180px">
        <a class="nav-link dropdown-toggle text-nowrap pl-3 pr-4" style="margin-top: 10px" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
{{--          <img class="user-avatar rounded-circle mr-2" src="images/avatars/0.jpg" alt="User Avatar">--}}
{{--          <div class="user-avatar rounded-circle mr-2 float-left" style="margin-top: -5px">--}}
{{--            <i class="material-icons" style="font-size: 1.5625rem">person</i>--}}
{{--          </div>--}}
          <span class="d-none d-md-inline-block float-left">{{ Auth::user()->name }} </span>
        </a>
        <div class="dropdown-menu dropdown-menu-small">
          <a class="dropdown-item" href="user-profile-lite.html">
            <i class="material-icons">&#xE7FD;</i> Profile</a>
{{--          <a class="dropdown-item" href="components-blog-posts.html">--}}
{{--            <i class="material-icons">vertical_split</i> Blog Posts</a>--}}
{{--          <a class="dropdown-item" href="add-new-post.html">--}}
{{--            <i class="material-icons">note_add</i> Add New Post</a>--}}
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-danger" href="{{ route('logout') }}"
             onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
          >
            <i class="material-icons text-danger">&#xE879;</i> Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
    <nav class="nav">
      <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
        <i class="material-icons">&#xE5D2;</i>
      </a>
    </nav>
  </nav>
</div>