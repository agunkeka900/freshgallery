<!-- Bootstrap assets -->
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--}}

<!-- Leaflet assets -->
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">

<div class="modal fade bd-example-modal-xl" id="address-geolocation" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Pilih Lokasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="address-geolocation-close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-0" style="padding: 0">
        <input type="hidden" id="latlng">
        <div class="container-fluid p-0" style="height: 600px" id="address-geolocation-container">
          <div id="map" style = "width:100%; height:inherit"></div>
          <div class='pointer'></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
    let map = L.map('map').setView([-8.5044378,115.0531128], 10);
    let searchControl = new L.esri.Controls.Geosearch().addTo(map);
    let marker = L.marker([-8.653663, 115.219288]).addTo(map);
    let popup = L.popup();

    L.control.scale().addTo(map);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ3VuZ2VrYSIsImEiOiJjazd5Y2lpOHMwNXo3M2VwYjl2NDIwbXdxIn0._0pm-FHSFPflU2ZQ96h4nw', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 25,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZ3VuZ2VrYSIsImEiOiJjazd5Y2lpOHMwNXo3M2VwYjl2NDIwbXdxIn0._0pm-FHSFPflU2ZQ96h4nw'
    }).addTo(map);


    setTimeout(function(){$('.pointer').fadeOut('slow');},3400);
    function onMapClick(e) {
        const latlng = e.latlng.toString().substring(7,40).split(')')[0];
        let html;
        document.getElementById('latlng').value = latlng;
        if(ELEMENT.checkout.address.mode === 'add'){
            html = "<br>" +
                "<button class='btn btn-primary' " +
                "onclick='document.getElementById(`address-geolocation-close`).click(); " +
                "document.getElementById(`aa-geolocation`).value = " +
                "document.getElementById(`latlng`).value;" +
                "document.getElementById(`aa-geolocation-text`).innerText = " +
                "document.getElementById(`latlng`).value;" +
                "document.getElementById(`aa-geolocation-reset-btn`).style.display = `block`;'>submit</button>";
        }
        else{
            html = "<br>" +
                "<button class='btn btn-primary' " +
                "onclick='document.getElementById(`address-geolocation-close`).click(); " +
                "document.getElementById(`ae-geolocation`).value = " +
                "document.getElementById(`latlng`).value;" +
                "document.getElementById(`ae-geolocation-text`).innerText = " +
                "document.getElementById(`latlng`).value;" +
                "document.getElementById(`ae-geolocation-reset-btn`).style.display = `block`;'>submit</button>";
        }
        marker.setLatLng(e.latlng);
        marker.bindPopup(html).openPopup();
    }

    map.on('click', onMapClick);

    $('#address-geolocation').on('shown.bs.modal', function() {
        map.invalidateSize();
    });

</script>