<div class="modal fade bd-example-modal-lg" id="address-modal" tabindex="-1" role="dialog" aria-labelledby="address-modal-title" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="address-modal-title">Tambah Alamat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="address-modal-close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="address-modal-content">
          <div id="address-list" class="row">@include('components.checkout.address_list')</div>
          <div id="address-add" style="display: none">@include('components.checkout.address_add')</div>
          <div id="address-edit" style="display: none">@include('components.checkout.address_edit')</div>
{{--          <div id="address-geolocation" style="display: none">@include('components.checkout.address_geolocation')</div>--}}
        </div>
      </div>
    </div>
  </div>
</div>