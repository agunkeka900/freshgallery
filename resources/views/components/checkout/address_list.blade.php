<div class="row">
  <div class="col">
    <button id="open-address-add" class="btn btn-outline-secondary btn-lg"
            onclick="showAddress('add')"
            style="text-transform: none; width: 100%">Tambah Alamat Baru</button>
  </div>
</div>
<div class="row">
  <div class="col-12 pt-2">
    <input type="text" id="address-search" class="form-control" autocomplete="off"
           placeholder="Cari alamat pengiriman" oninput="debounceSearchAddress(this.value)">
  </div>
</div>
<div class="row" id="address-list-data"></div>


