<form method="post" action="{{ url('checkout/address') }}" id="aa-form">
  @csrf
  <div class="form-group row text-left">
    <label for="aa-label" class="col-12 col-form-label text-left">Label alamat</label>
    <div class="col-12">
      <input type="text" id="aa-label" class="form-control" autocomplete="off"
             placeholder="Contoh: Alamat Rumah, Kantor, Lapangan" name="label">
    </div>
  </div>
  <div class="form-group row text-left">
    <label for="aa-region" class="col-12 col-form-label text-left">Kota/Kabupaten, Kecamatan, Desa/Kelurahan</label>
    <div class="col-12">
      <input type="text" id="aa-region" class="form-control" autocomplete="off"
             placeholder="Contoh: Denpasar, Denpasar Barat, Pemecutan" name="village">
      <input id="aa-region_id" type="hidden" name="village_id">
    </div>
  </div>
  <div class="form-group row text-left">
    <label for="aa-complete-address" class="col-12 col-form-label text-left">Alamat Lengkap</label>
    <div class="col-12">
      <textarea id="aa-complete-address" class="form-control" name="alamat" autocomplete="off"
                placeholder="Isi dengan nama jalan, nama gang, nomor rumah atau nama gedung"></textarea>
    </div>
  </div>
  <div class="form-group row text-left">
    <label class="col-12 col-form-label text-left">Pin Alamat (Opsional)</label>
    <div class="col-12">
      <div class="card-map">
        <div id="aa-geolocation-text" style="float: left">Tandai lokasi dalam peta</div>
        <button type="button" class="btn-tandai-lokasi" data-toggle="modal" data-target="#address-geolocation"
                onclick="showAddress(null); setMapHeight()">Tandai Lokasi</button>
        <button type="button" class="btn-tandai-lokasi" id="aa-geolocation-reset-btn" style="display: none; margin-right: 10px"
                onclick="resetGeolocation()"><span aria-hidden="true">&times;</span></button>
      </div>
      <input type="hidden" name="geolocation" id="aa-geolocation">
    </div>
  </div>
  @if($address_primary)
  <div class="form-group row text-left">
    <div class="col-12">
      <div class="form-check">
        <input id="aa-primary" class="form-check-input" type="checkbox"
               name="primary" value="true">
        <label class="form-check-label" for="aa-primary">
          Jadikan alamat utama
        </label>
      </div>
    </div>
  </div>
  @endif
  <div class="form-group row text-left" id="aa-submit-message" style="display: none"></div>
  <button type="submit" class="btn btn-primary float-right">Simpan</button>
  <button type="button" class="btn btn-secondary float-right mr-2" onclick="showAddress('list')">Kembali</button>
</form>

<link href="{{ asset('css/autocomplete.css') }}" rel="stylesheet">
<script src="{{ asset('js/autocomplete.js') }}"></script>
<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<script>
    autocomplete(document.getElementById("aa-region"), JSON.parse('{!! $village !!}'));

    $(document).ready(function() {
        $("#aa-form").submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: '{{url('checkout/address')}}',
                type: 'post',
                data: $(this).serialize(),
                success: function(data) {
                    let aa = ELEMENT.checkout.address.add;
                    aa.submit_message.innerHTML = "";
                    aa.region_id.value = null;
                    aa.form.reset();
                    resetGeolocation();
                    setAddress(data.id, data.label, data.complete_region, data.address, data.primary);
                },
                error: function(data) {
                    let aa = ELEMENT.checkout.address.add;
                    let errors = data.responseJSON.errors;
                    let keys = Object.keys(errors);
                    let html = "<div class='col-12'>";
                    for(let i=0; i<keys.length; i++){
                        for(let a=0; a<errors[keys[i]].length; a++){
                            html += "<div class='alert alert-danger'>"+errors[keys[i]][a]+"</div>"
                        }
                    }
                    aa.submit_message.innerHTML = (html + "</div>");
                    aa.submit_message.style.display = 'block';
                },
            });
        });
    });

    function setMapHeight() {
        document.getElementById('address-geolocation-container').style.height = (window.innerHeight - 121) + 'px';
    }
</script>
<style>
  .btn-tandai-lokasi{
    background: white;
    border-radius: 3px;
    border: 1px solid grey;
    float: right;
    font-size: 14px;
    padding: 10px;
    color: grey;
    margin-top: 20px;
    margin-right: 20px;
    max-height: 38px;
    line-height: 15px;
  }
  .card-map{
    height: 75px;
    width: 100%;
    background: #f2f2f2 url('{{ asset('img/bg-map.png') }}') no-repeat center center;
    background-size: cover;
    border-radius: 5px;
    color: #606060;
    padding-left: 20px;
    line-height: 75px;
  }
</style>