@foreach($artikel_lainnya as $a)
  <a href="{{ url('healthy_info/read/'.$a['id']) }}">
    <table class="col-12 py-1 my-1 border-bottom" style="cursor: pointer">
      <tr>
        <td rowspan="2" style="width: 70px; padding-bottom: 5px; vertical-align: top">
          <img src="{{ $a['img'] }}" alt="Gambar Artikel" class="oa-img">
        </td>
        <td class="oa-title">
          <span>{{ $a['judul'] }}</span>
        </td>
      </tr>
      <tr>
        <td class="oa-body">
          <span>{{ $a['isi'] }}</span>
        </td>
      </tr>
    </table>
  </a>
@endforeach

<style>
  .oa-img{
    width: 70px;
    height: 70px;
    float: left;
  }
  .oa-title{
    font-size: 13px;
    font-weight: bold;
    text-align: left;
    vertical-align: top;
    padding-left: 10px;
    /*height: 30px;*/
  }
  .oa-body{
    font-size: 13px;
    text-align: left;
    vertical-align: top;
    padding-left: 10px;
    padding-bottom: 10px;
  }
  a:hover{
    color: black;
  }
</style>