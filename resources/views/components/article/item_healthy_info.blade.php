<div class="col-sm-12 col-md-6 col-lg-4 float-left mb-4">
  <div class="card">
    <a href="{{ route('healthy_info/read', ['id' => $d['id']]) }}">
      <div class="card-img" style="background-image: url('{{ $d['img'] }}'); height: 150px; background-size: cover; background-position-y: 20%;"></div>
    </a>
    <div class="card-body">
      <i class="text-secondary">{{ $d['tgl'] }}</i>
      <a href="{{ route('healthy_info/read', ['id' => $d['id']]) }}">
        <h2>{{ $d['judul'] }}</h2>
      </a>
      <div class="dropdown-divider" style="border-width: 3px; width: 100px"></div>
      <p class="card-text">{{ $d['isi'] }}</p>
    </div>
    <div class="card-footer">
      <a href="{{ route('healthy_info/read', ['id' => $d['id']]) }}">
        <button class="btn btn-primary float-right">Baca Artikel &nbsp; <i class="fas fa-arrow-right"></i></button>
      </a>
    </div>
  </div>
</div>