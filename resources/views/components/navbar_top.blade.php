<style>
  .nav-top-logo-horizontal{
    width: 120px;
    display: block;
    margin-right: 10px;
  }
  @media (max-width: 768px){
    .nav-top-logo-horizontal{
      display: none;
    }
  }
  .btn-user{
    color: #7f7f7f;
    width: 100%;
    height: 30px;
    text-align: left;
    line-height: 30px;
    border-radius: 5px;
    font-size: 14px;
    padding-left: 10px;
  }
  .btn-user:hover{
    background: #f5f5f5;
    cursor: pointer;
  }
  .img-user{
    width: 45px;
    height: 45px;
    background-size: cover;
    background-repeat: no-repeat;
    margin: auto;
    border-radius: 50%;
  }

  .btn-icon{
    font-size: 35px;
  }
  .btn-menunggu{
    width: 100%;
    font-size: 12px;
    background: white;
    border-radius: 5px;
    padding: 5px 10px;
  }
  .btn-menunggu:hover{
    background: #f5f5f5;
    cursor: pointer;
  }
  .font-11px{
    font-size: 11px;
  }
  .font-12px{
    font-size: 12px;
  }
  .font-14px{
    font-size: 14px;
  }
  .container-notif{
    width: 350px;
    position: fixed;
    box-sizing: border-box;
    height: auto;
    max-height: calc(120vh - 90px);
    background: white;
    box-shadow: 0 2px 5px 2px rgba(0,0,0,0.1);
    border-radius: 5px;
    margin-left: calc(50px - 350px);
  }
  .btn-notif{
    color: #7f7f7f;
    width: 37px;
    height: 37px;
    text-align: center;
    line-height: 37px;
    border-radius: 5px;
    margin-left: 5px;
  }
  .btn-notif:hover{
    background: #f5f5f5;
    cursor: pointer;
  }
  .chip-notif{
    background: #e60050;
    border-radius: 50%;
    width: 17px;
    height: 17px;
    line-height: 17px;
    position: absolute;
    color: white;
    text-align: center;
    font-size: 10px;
    font-weight: bold;
    display: none;
  }
  .chip-notif1{
    margin-left: 25px;
  }
  .chip-notif2{
    margin-left: 60px;
    margin-top: -10px;
  }


  a{
    color: #333;
  }
  .search-box{
    background: #f5f5f5;
    /*background: #4e555b;*/
    width: 100%;
    height: 37px;
    border-radius: 5px;
  }
  .search-box tr td{
    padding: 7px 0;
  }
  .search-input{
    border: none;
    background: transparent;
    color: gray;
    font-size: 14px;
    width: 100%;
  }
  .search-result-box{
    width: 100%;
    height: max-content;
    padding: 0 0 10px 0;
    overflow: hidden;
    border-radius: 5px;
    background: white;
    /*background: #4e555b;*/
    position: inherit;
    /*color: white;*/
    font-size: 11pt;
    /*display: block;*/
    box-shadow: 0 2px 5px 2px rgba(0,0,0,0.1);
  }
  .search-result-img{
    width: 35px;
    height: 35px;
    border-radius: 3px;
    background: white;
    background-size: cover;
    background-position-y: 20%;
    background-position-x: 50%;
  }
  .search-result-nama{
    line-height: 15px;
  }
  .search-result-harga{
    line-height: 10px;
    font-size: 10pt;
  }
  .get-search-result{
    width: 100%;
    cursor: pointer;
  }
  .get-search-result:hover{
    background: #f5f5f5;
    color: #333;
    /*background: #212529;*/
  }
  input:focus {
    outline: none;
  }
  ::placeholder {
    color: #999999;
  }
</style>
<nav class="navbar navbar-dark fixed-top navbar-shrink" id="mainNav" style="z-index: 98;
    -webkit-box-shadow: 0 2px 3px 0 rgba(0,0,0,0.1);
    -moz-box-shadow: 0 2px 3px 0 rgba(0,0,0,0.1);
    box-shadow: 0 2px 3px 0 rgba(0,0,0,0.1);
    height: 60px;
    display: inline;
    padding: 0;
">
  <div class="container-fluid" style="padding-left: 10px; padding-right: 10px; max-width: 1140px;">
    <table style="width: 100%; margin-top: 10px">
      <tr>
{{--        Button back--}}
        <td style="width: 1px">
{{--          <a href="{{ url()->previous() }}" class="pt-1 btn-back" style="font-size: 16pt; color: gray">--}}
{{--          <a href="{{ route('/') }}" class="pt-1 btn-back" style="font-size: 16pt; color: gray">--}}
          {{--<a onclick="window.history.back()" class="pt-1 btn-back" style="font-size: 16pt; color: gray">--}}
            {{--<i class="fas fa-arrow-left"></i>--}}
          {{--</a>--}}
          <a href="{{ route('/') }}">
            <img src="{{ asset('img/logo_horizontal.jpg') }}" height="35px" alt="logo_horizontal" class="nav-top-logo-horizontal">
          </a>
        </td>
{{--        Input search--}}
        <td>
          <table class="search-box">
            <tr>
              <td width="30px" style="width: 30px; padding: 0 5px 2px 10px; font-size: 11pt">
                <i class="fas fa-search float-left" style="margin: 3px 10px 0 5px; color: gray"></i>
              </td>
              <td>
                <input oninput="debounce(this.value, '{{ csrf_token() }}')"
                       onfocusout="showSearchResult(false)"
                       onfocusin="showSearchResult(this.value)"
                       onmousedown="debounce(this.value, '{{ csrf_token() }}')"
                       id="search-input"
                       type="text" class="search-input float-left"
                       placeholder="Cari sayuran dan buah segar disini">
              </td>
              <td width="30px" style="width: 30px; padding: 0 5px 2px 10px; font-size: 11pt">
                <i id="search-clear" class="fas fa-times"
                   style="margin: 3px 10px 0 5px; cursor: pointer; display: none; color: gray"
                   onclick="clearSearch()"></i>
              </td>
            </tr>
          </table>
        </td>
        @if(Auth::user())
{{--        Button notif--}}
        <td style="width: 50px" class="text-center">
          <div class="btn-notif" id="navbar-top-notif-btn" onclick="showNotifContainer()">
            <div class="chip-notif chip-notif1" id="navbar-top-notif-chip-total"></div>
            <i class="fas fa-bell"></i>
          </div>
        </td>
{{--        Button User--}}
        <td style="width: 40px" class="text-center">
          <div class="btn-notif ml-0" id="navbar-top-user-btn" onclick="showUserContainer()">
            <i class="fas fa-user"></i>
          </div>
        </td>
      </tr>
      @else
        <td style="width: 90px" class="text-center">
          <a href="{{ route('viewLogin') }}">
            <button class="btn btn-primary">Masuk</button>
          </a>
        </td>
        <td style="width: 80px">
          <a href="{{ route('viewRegister') }}">
            <button class="btn" style="border-color: #2ea7c1; color: #2ea7c1;">Daftar</button>
          </a>
        </td>
      @endif
      <tr>
        <td></td>
{{--        Container Search Result--}}
        <td class="pr-3">
          <div style="display: none;" class="search-result-box ml-3" id="search-result">
            <div class="col-12 px-0 border-bottom">
              <table class="get-search-result">
                <tr>
                  <td width="10px" class="py-4">
{{--                    <div class="search-result-img"></div>--}}
                  </td>
                  <td>
                    <div id="hasilPencarianUntuk"
                         onmousedown="redirectToSearchPage()"></div>
                  </td>
                  <td width="10px" class="pr-3">
                    <i class="fas fa-chevron-right"></i>
                  </td>
                </tr>
              </table>
            </div>
            <div class="pl-3 py-2 font-weight-bold float-left" style="color: gray">Rekomendasi produk</div>
            <div id="search-loading"
                 class="spinner-border spinner-border-sm float-left ml-2" role="status"
                 style="color: gray; border-width: 0.15em; display: none; margin-top: 10px"></div>
            <div id="search-recommended"></div>
          </div>
        </td>
{{--        Container Notifikasi--}}
        <td style="vertical-align: top">
          @include('components.navbar_top.container_notifikasi')
        </td>
{{--        Container User--}}
        <td style="vertical-align: top">
          @include('components.navbar_top.container_user')
        </td>
      </tr>
    </table>
  </div>
</nav>
<script>
  function showNotifContainer(show = true) {
    let ntf = ELEMENT.navbar_top.notif;
    ntf.container.style.display = !!show ? 'block' : 'none'
  }
  function showUserContainer(show = true) {
      let usr = ELEMENT.navbar_top.user;
      usr.container.style.display = !!show ? 'block' : 'none'
  }
  function setNotif(data) {
    let notif = ELEMENT.navbar_top.notif;
    if(data.total !== 0){
        notif.chip_total.innerText = data.total;
        notif.chip_total.style.display = 'block';
    }
    if(data.periksa !== 0){
        console.log(data.periksa !== 0)
        notif.chip_periksa.innerText = data.periksa;
        notif.chip_periksa.style.display = 'block';
    }
    if(data.proses !== 0){
        notif.chip_proses.innerText = data.proses;
        notif.chip_proses.style.display = 'block';
    }
    if(data.dikirim !== 0){
        notif.chip_dikirim.innerText = data.dikirim;
        notif.chip_dikirim.style.display = 'block';
    }
    if(data.selesai !== 0){
        notif.chip_selesai.innerText = data.selesai;
        notif.chip_selesai.style.display = 'block';
    }
  }
  function getNotif(){
      $(document).ready(function() {
          $.ajax({
              url: '{{url('notif')}}',
              type: 'get',
              data: {
                  _token: '{{ @csrf_token() }}'
              },
              success: function(data) {
                  setNotif(data)
              },
              error: function(data) {
                  console.log(data)
              },
          });
      });
  }
  document.addEventListener("click", function (e) {
      if(!(e.path.length >= 2 && (e.path[0].id === 'navbar-top-notif-btn' || e.path[1].id === 'navbar-top-notif-btn'))){
          showNotifContainer(false);
      }
      if(!(e.path.length >= 2 && (e.path[0].id === 'navbar-top-user-btn' || e.path[1].id === 'navbar-top-user-btn'))){
          showUserContainer(false);
      }
  });
  getNotif()
</script>
<script src="{{ asset('js/cariProduk.js') }}"></script>
@if(Route::current()->uri == 'product/search/{key}')
  <script>setCurrentKeySearch('{{ Route::current()->parameters()['key'] }}')</script>
@endif
