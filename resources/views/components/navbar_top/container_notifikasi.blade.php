<div class="container-notif" id="navbar-top-notif-container" style="display: none; margin-left: calc(95px - 350px);">
  <div class="container">
    <div class="row">
      <div class="col-12 font-weight-bold py-3 border-bottom">Notifikasi</div>
    </div>

    <div class="row" style="overflow: auto; overscroll-behavior-y: contain;">
      <div style="height: 150px; display: flex; flex-direction: column; max-height: calc(100vh - 140px); width: 100%">
        <div class="row" style="margin: 0">
          <div class="col-6 font-weight-bold pt-2 pb-4 font-14px">Pembelian</div>
          <a class="col-6 text-right pt-2 pb-4 font-12px text-primary" href="{{ url('order/list') }}">Lihat Semua</a>
          {{--                <div class="col-12 px-1 font-14px mb-3">--}}
          {{--                  <div class="btn-menunggu">Menunggu konfirmasi stok</div>--}}
          {{--                </div>--}}
          <a class="col-3 text-center px-0 mb-2" href="{{ url('order/list/status=1') }}">
            <div class="chip-notif chip-notif2" id="navbar-top-notif-chip-periksa"></div>
            <i class="fas fa-clock btn-icon text-primary"></i>
            <div class="font-11px pt-2" style="line-height: 14px">Pemeriksaan<br>Stok</div>
          </a>
          <a class="col-3 text-center px-0 mb-2" href="{{ url('order/list/status=2') }}">
            <div class="chip-notif chip-notif2" id="navbar-top-notif-chip-proses"></div>
            <i class="fas fa-sync-alt btn-icon text-primary"></i>
            <div class="font-11px pt-2" style="line-height: 14px">Pesanan<br>Diproses</div>
          </a>
          <a class="col-3 text-center px-0 mb-2" href="{{ url('order/list/status=7') }}">
            <div class="chip-notif chip-notif2" id="navbar-top-notif-chip-dikirim"></div>
            <i class="fas fa-shipping-fast btn-icon text-primary"></i>
            <div class="font-11px pt-2" style="line-height: 14px">Pesanan<br>Dikirim</div>
          </a>
          <a class="col-3 text-center px-0 mb-2" href="{{ url('order/list/status=4') }}">
            <div class="chip-notif chip-notif2" id="navbar-top-notif-chip-selesai"></div>
            <i class="fas fa-check-circle btn-icon text-primary"></i>
            <div class="font-11px pt-2" style="line-height: 14px">Pesanan<br>Selesai</div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>