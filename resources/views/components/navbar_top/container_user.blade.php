@if(Auth::user())
  <div class="container-notif" style="width: 200px; margin-left: calc(40px - 200px); display: none;" id="navbar-top-user-container">
    <div class="container">
      <div class="row">
        <div class="col-12 pt-3">
          <div class="img-user">
            <img src="{{ Auth::user()->foto ? asset('img_people/'.Auth::user()->foto) : (Auth::user()->photo ?: asset('img/default_user.jpg') )}}"
                 class="img-user" style="background-size: cover;" alt="people-photo" id="navbar-top-user-photo">
          </div>
        </div>
        <div class="col-12 font-weight-bold pb-3 border-bottom text-center">
          {{ Auth::user()->name }}
        </div>
      </div>

      <div class="row" style="overflow: auto; overscroll-behavior-y: contain;">
        <div class="p-2" style="height: 240px; display: flex; flex-direction: column; max-height: calc(100vh - 140px); width: 100%">
          @if(Auth::user()->role_user == '3')
            <a class="row" style="margin: 0" href="{{ url('admin/dashboard') }}">
              <div class="btn-user"><i class="fas fa-chart-line" style="width: 15px;"></i> &nbsp; Admin Panel</div>
            </a>
          @endif
          <a class="row" style="margin: 0" href="{{ url('order/list') }}">
            <div class="btn-user"><i class="fas fa-dollar-sign" style="width: 15px;"></i> &nbsp; Pembelian</div>
          </a>
          {{--<a class="row" style="margin: 0" href="{{ url('people') }}">--}}
            {{--<div class="btn-user"><i class="fas fa-map-marker-alt" style="width: 15px;"></i> &nbsp; Daftar Alamat</div>--}}
          {{--</a>--}}
          <a class="row" style="margin: 0" href="{{ url('people') }}">
            <div class="btn-user"><i class="fas fa-cog" style="width: 15px;"></i> &nbsp; Pengaturan</div>
          </a>
          <a class="row  mt-2 pt-2 border-top" style="margin: 0" href="{{ url('healthy_info') }}">
            <div class="btn-user"><i class="fas fa-newspaper" style="width: 15px;"></i> &nbsp; Healthy Info</div>
          </a>
          <a class="row" style="margin: 0" href="{{ url('fresh_edu') }}">
            <div class="btn-user"><i class="fas fa-school" style="width: 15px;"></i> &nbsp; Fresh Edu</div>
          </a>
          <a class="row" style="margin: 0" href="{{ url('about') }}">
            <div class="btn-user"><i class="fas fa-info-circle" style="width: 15px;"></i> &nbsp; Tentang FG</div>
          </a>
          <a class="row  mt-2 py-2 border-top" style="margin: 0" href="{{ route('logout') }}"
             onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <div class="btn-user"><i class="fas fa-sign-out-alt" style="width: 15px;"></i> &nbsp; Keluar</div>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </div>
    </div>
  </div>
@endif