@extends('layouts.shards')

@section('content')
  <script src="{{ asset('js/Chart.bundle.js') }}"></script>
  <input type="hidden" id="token" value="{{ csrf_token() }}">
  <input type="hidden" id="url" value="{{ url('') }}">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-3">
          <div class="col text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title">Dashboard</h3>
          </div>
        </div>
        <div class="row">
          @foreach($total_records as $tr)
            <div class="col-lg-3 col-md-6 col-sm-6 my-2 px-2">
              <div class="stats-small stats-small--1 card card-small">
                <div class="card-body p-0 d-flex">
                  <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                      <span class="stats-small__label text-uppercase">{{ $tr['name'] }}</span>
                      <h6 class="stats-small__value count my-3">{{ is_numeric($tr['total']) ? number_format($tr['total'], 0, ',', '.') : $tr['total'] }}</h6>
                    </div>
                  </div>
                  <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="row">
          <div class="col-12 mt-3">
            <span style="font-size: 16px;" class="d-block pt-3 text-muted border-top">
              <strong>Jumlah Transaksi</strong>
            </span>
          </div>
          <div class="col-12 mb-4 px-2 pt-3">
            <div class="card card-small h-100">
              <div class="card-header border-bottom">
                <div class="row">
                  <div class="col-9">
                    <select class="custom-select custom-select font-weight-bold" id="transaksi-berdasarkan" title="berdasarkan" onchange="getTransaksi()">
                      <option value="nominal" selected>Berdasarkan Nominal Transaksi</option>
                      <option value="jumlah">Berdasarkan Jumlah Transaksi</option>
                    </select>
                  </div>
                  <div class="col-3">
                    <select class="custom-select custom-select font-weight-bold" id="transaksi-tahun" title="tahun" onchange="getTransaksi()">
                      {{--@for($i=0; $i<count($item_tahun_pc); $i++)--}}
                        {{--<option value="{{ $item_tahun_pc[$i] }}" {{ $i == 0 ? 'selected' : '' }} >Tahun {{ $item_tahun_pc[$i] }}</option>--}}
                      {{--@endfor--}}
                      <option value="2020" selected >Tahun 2020</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="card-body d-flex p-3" id="chart-transaksi"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
      let colors = [
          '#81D4FA',
          '#9FA8DA',
          '#EF9A9A',
          '#A5D6A7',
          '#FFF59D',
          '#F48FB1',
          '#FFE082',
          '#CE93D8',
          '#80CBC4',
          '#90CAF9',
          '#FFAB91',
          '#B39DDB',
          '#FFCC80',
          '#80DEEA',
          '#C5E1A5',
          '#B0BEC5',
          '#E6EE9C',
          '#BCAAA4',
      ];
      let lineOptions = {
          legend: {
              display: true,
              position: 'top',
              labels: {
                  // boxWidth: 80,
                  // pointStyle:'circle',
                  // usePointStyle:true,
                  // fontColor: 'black'
              }
          }
      };
      let url = _('url').value;
      let token = _('token').value;
      let namaBulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

      function _(id){
          return document.getElementById(id);
      }
      function setChartTransaksi(transaksi_data, tahun, berdasarkan = 'nominal') {
          let data = {
              labels: namaBulan,
              datasets: [
                  {
                      borderColor: colors[Math.floor(Math.random() * (colors.length - 1))],
                      fill: false,
                      label: tahun,
                      data: transaksi_data
                  }
              ]
          };
          _('chart-transaksi').innerHTML = "<canvas id='chart-transaksi-"+berdasarkan+"-"+tahun+"' width='1000' height='450'/>";
          new Chart(_('chart-transaksi-'+berdasarkan+'-'+tahun), {
              type: 'line',
              data: data,
              options: lineOptions
          });
      }
      function getTransaksi() {
          let berdasarkan = _('transaksi-berdasarkan').value;
          let tahun = _('transaksi-tahun').value;
          $(document).ready(function() {
              $.ajax({
                  url: url+'/supplier/dashboard/transaksi/'+tahun+'/'+berdasarkan,
                  type: 'get',
                  data: {_token: token},
                  success: function(data) {
                      setChartTransaksi(JSON.parse(data), tahun, berdasarkan)
                  },
                  error: function(data) {
                      alert('Gagal mengambil data!')
                  },
              });
          });
      }
      setChartTransaksi(JSON.parse('{!! $data_transaksi !!}'), '2020');
  </script>
@endsection