@extends('layouts.shards')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-3">
          <div class="col text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title">Konfirmasi stok</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4 overflow-hidden">
              <div class="card-header border-bottom px-3">
                <div class="row">
                  <div class="col-sm-6 col-md-3 mb-2 mb-md-0" style="height: 35px">
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">search</i>
                        </span>
                      </span>
                      <input id="stock-confirmation-filter-search" type="text" class="form-control" placeholder="Cari data" oninput="filterKonfirmasiStok()">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3 mb-2 mb-md-0" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select id="stock-confirmation-filter-pemesan" class="form-control" onchange="filterKonfirmasiStok()">
                        <option selected disabled value="">Filter Pemesan</option>
                        @foreach($item_filter['pemesan'] as $p)
                          <option value="{{ $p }}">{{ $p }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select id="stock-confirmation-filter-tanggal" class="form-control" onchange="filterKonfirmasiStok()">
                        <option selected disabled value="">Filter Tanggal</option>
                        @foreach($item_filter['tanggal'] as $p)
                          <option value="{{ $p }}">{{ $p }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select id="stock-confirmation-filter-product" class="form-control" onchange="filterKonfirmasiStok()">
                        <option selected disabled value="">Filter Produk</option>
                        @foreach($item_filter['product'] as $p)
                          <option value="{{ $p }}">{{ $p }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row" style="display: none" id="stock-confirmation-filter-reset">
                  <div class="col pt-2">
                    <button type="button" class="btn btn-primary float-right" onclick="filterReset()">
                      Reset Filter
                    </button>
                  </div>
                </div>
              </div>
              <div class="card-body p-0 text-left overflow-auto" id="stock-confirmation-table">
                @include('components.supplier.stock_confirmation_table')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style>
    .overflow-x-auto > .row {
      overflow-x: auto;
      white-space: nowrap;
    }
  </style>
  <script>
    function filterReset(){
        let scf = ELEMENT.supplier.stock_confirmation.filter;
        scf.search.value = null;
        scf.pemesan.selectedIndex = 0;
        scf.tanggal.selectedIndex = 0;
        scf.product.selectedIndex = 0;
        filterKonfirmasiStok();
    }
    function filterKonfirmasiStok(){
      let sc  = ELEMENT.supplier.stock_confirmation;
      let scf = sc.filter;
      let search = scf.search.value;
      let pemesan = scf.pemesan.options[scf.pemesan.selectedIndex].value;
      let product = scf.product.options[scf.product.selectedIndex].value;
      let tanggal = scf.tanggal.options[scf.tanggal.selectedIndex].value;
      $(document).ready(function() {
        $.ajax({
          url: '{{url('supplier/stock-confirmation/filter')}}',
          type: 'post',
          data: {
            search: search,
            pemesan: pemesan,
            product: product,
            tanggal: tanggal,
            _token: '{{ @csrf_token() }}'
          },
          success: function(data) {
            sc.table.innerHTML = data;
            if(!search && !pemesan && !product && !tanggal)
              scf.reset.style.display = 'none';
            else scf.reset.style.display = 'block';
          },
          error: function(data) {
            console.log(data)
          },
        });
      });
    }
    function confirm(id){
        $(document).ready(function() {
            let sc  = ELEMENT.supplier.stock_confirmation;
            sc.btn_confirm(id).setAttribute('disabled','')
            $.ajax({
                url: '{{url('supplier/stock-confirmation/confirm')}}'+'/'+id,
                type: 'get',
                success: function(data) {
                    filterKonfirmasiStok();
                },
                error: function(data) {
                    sc.btn_confirm(id).removeAttribute('disabled')
                },
            });
        });
    }
    function reject(id){
        $(document).ready(function() {
            let sc  = ELEMENT.supplier.stock_confirmation;
            sc.btn_reject(id).setAttribute('disabled','')
            $.ajax({
                url: '{{url('supplier/stock-confirmation/reject')}}'+'/'+id,
                type: 'get',
                success: function(data) {
                    filterKonfirmasiStok();
                },
                error: function(data) {
                    sc.btn_reject(id).removeAttribute('disabled')
                },
            });
        });
    }
    function hoverTable(id, hover = true) {
      let bg = hover ? '#F1F3F4' : '#FFF';
      let td = (id1, id2 = null, child) => {
        if(id2 === null) return document.getElementById('td'+child+'-'+id1);
        else return document.getElementById('td'+child+'-'+id1+'-'+id2)
      }
      if(id.includes('-')){
        let ids = id.split('-');
        if(ids.length === 2){
          for(let i=1; i<=3; i++){
            td(ids[1], null, i).style.background = bg;

            let index = 0;
            do{
              for(let p=4; p<=7; p++)
                td(ids[1], index, p).style.background = bg;
              index += 1;
            }while(td(ids[1], index, 4) !== null)
          }
        }
        else if(ids.length === 3){
          for(let i=1; i<=4; i++) {
            if(i !== 4) td(ids[1], null, i).style.background = bg;
            td(ids[1], ids[2], i+3).style.background = bg;
          }
        }
      }
    }
  </script>
@endsection