@extends('layouts.shards')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title float-left">
              <a href="{{ url('supplier/admin') }}"><i class="material-icons">arrow_back</i></a>
              Tambah supplier</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <ul class="list-group list-group-flush">
                <li class="list-group-item px-3">
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ ucfirst($error) }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @elseif ($message = Session::get('success'))
                    <div class="alert alert-success"> {{ $message }} </div>
                  @endif
                  <form  method="post" action="{{ url('supplier/admin') }}">
                    @csrf
                    <div class="form-group row text-left">
                      <label for="nama" class="col-sm-3 col-form-label text-right">Nama supplier</label>
                      <div class="col-sm-9">
                        <input type="text" id="nama" class="form-control" placeholder="Nama supplier" name="nama_supplier"
                               value="{{ old('nama_supplier') }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="alamat" class="col-sm-3 col-form-label text-right">Alamat</label>
                      <div class="col-sm-9">
                        <input type="text" id="alamat" class="form-control" placeholder="Alamat" name="alamat"
                               value="{{ old('alamat') }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="kota" class="col-sm-3 col-form-label text-right">Kota</label>
                      <div class="col-sm-9">
                        <input type="text" id="kota" class="form-control" name="kota" placeholder="Kota"
                               value="{{ old('kota') }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="keterangan" class="col-sm-3 col-form-label text-right ">Keterangan</label>
                      <div class="col-sm-9">
                        <input type="text" id="keterangan" class="form-control" placeholder="Keterangan" name="keterangan"
                               value="{{ old('keterangan') }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="telp" class="col-sm-3 col-form-label text-right">Nomor telpon</label>
                      <div class="col-sm-9">
                        <input type="text" id="telp" class="form-control" name="telp" placeholder="Nomor telpon"
                               value="{{ old('telp') }}">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                    <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ url('supplier/admin') }}'">Kembali</button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection