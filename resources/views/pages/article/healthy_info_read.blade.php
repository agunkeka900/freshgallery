@extends('layouts.main')

@section('content')
  @if($artikel['pdf'] != null)
    <script src="{{ asset('js/pdfobject.min.js') }}"></script>
  @endif
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-body p-0">
              <div class="text-center font-weight-bold px-5 pt-5 pb-4">
                <h2>{{ $artikel['judul'] }}</h2>
                <p class="text-center text-secondary">{{ $artikel['tgl'] }}</p>
                <div class="dropdown-divider mx-auto" style="border-width: 5px; width: 100px"></div>
              </div>
              <div class="card-text px-5 py-0" id="isi">
                {{ $artikel['isi'] }}
              </div>
              @if($artikel['pdf'] != null)
                <div id="pdf_view" class="py-4"></div>
              @endif
              <div class="card-text pt-0 pb-5 px-5">
                Kategori:
                @foreach($artikel['tag'] as $k)
                  <div class="card p-1 mt-1" style="width: 150px">
                    <table >
                      <td style="width: 30px"><i class="fas fa-tags"></i></td>
                      <td>{{ $k  }}</td>
                    </table>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">Artikel lainnya</div>
            <div class="card-body">
              <div class="row px-2">
                @include('components.article.item_others')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <style>
    #isi a{
      color: blue;
    }

    .instagram-media{margin:15px auto !important;}
    /*.pdfobject-container { height: 500px;}*/
    /*.pdfobject { border: 1px solid #666; }*/
  </style>
  @if($artikel['pdf'] != null)
    <script type="text/javascript">
      $(document).ready(function(){
          let options = {
              height: "500px",
              pdfOpenParams: { view: 'FitV', page: '1' }
          }
          PDFObject.embed("{{ asset('pdf/'.$artikel['pdf']) }}", "#pdf_view", options);
      });
    </script>
  @endif
  <script type="text/javascript">
      const isi = document.getElementById('isi').innerText;
      document.getElementById('isi').innerHTML = isi;
  </script>
  <script async src="//www.instagram.com/embed.js"></script>
@endsection