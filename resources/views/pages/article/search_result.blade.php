@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row mb-3 pl-3">
        <h3 class="text-secondary">
          <b>Hasil pencarian "{{ $keywords }}"</b>
          <div class="dropdown-divider" style="border-width: 5px; width: 120px"></div>
        </h3>

      </div>
      <div class="row">
        @foreach($data as $d)
          @include('components.article.item_healthy_info')
        @endforeach
      </div>
    </div>
  </section>
@endsection