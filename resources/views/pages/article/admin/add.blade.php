@extends('layouts.shards')

@section('content')
  <section class="page-section py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title mb-0">{{ $data ? 'Edit' : 'Tambah' }}  artikel</h5>
            </div>
            <div class="card-body">
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @elseif ($message = Session::get('success'))
                <div class="alert alert-success"> {{ $message }} </div>
              @endif
              <form method="post" action="{{ url('article/admin') }}" id="form" enctype="multipart/form-data">
                @csrf
                @if($data)
                  @method('PUT')
                @endif
                <div class="form-group row text-left">
                  <div class="col-12">
                    <input type="text" class="form-control" placeholder="Judul artikel" name="judul" required id="judul"
                           value="{{ old('judul') ?: ($data ? $data['judul'] : '') }}">
                  </div>
                </div>
                <input type="hidden" id="isi_artikel" name="isi_artikel">
                <input type="hidden" id="kategori_hidden" name="kategori">
                <input type="hidden" id="thumbnail_hidden" name="thumbnail">
                <input type="file" id="pdf_hidden" name="pdf_file" style="display: none" onchange="setName(this.value)">
                @if($data)
                  <input type="hidden" name="id" value="{{ $data['id'] }}">
                @endif
              </form>

              <div class="toolbar">
                <button class="btn btn-dark" data-toggle="tooltip" title="Italic (Ctrl+I)" onclick="document.execCommand('italic', false, ''); fc()">
                  <i class="fa fa-italic"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Bold (Ctrl+B)" onclick="document.execCommand('bold', false, ''); fc()">
                  <i class="fa fa-bold"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Underline (Ctrl+U)" onclick="document.execCommand('underline', false, ''); fc()">
                  <i class="fa fa-underline"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Strike through" onclick="document.execCommand('strikeThrough',false, ''); fc()">
                  <i class="fas fa-strikethrough"></i>
                </button>

                <button class="btn btn-dark ml-3" data-toggle="tooltip" title="Align left" onclick="document.execCommand('justifyLeft',false, ''); fc()">
                  <i class="fas fa-align-left"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Align center" onclick="document.execCommand('justifyCenter',false, ''); fc()">
                  <i class="fas fa-align-center"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Align right" onclick="document.execCommand('justifyRight',false, ''); fc()">
                  <i class="fas fa-align-right"></i>
                </button>

                <button class="btn btn-dark ml-3" data-toggle="tooltip" title="Ordered list" onclick="document.execCommand('insertOrderedList',false, ''); fc()">
                  <i class="fas fa-list-ol"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Unordered list" onclick="document.execCommand('insertUnorderedList',false, ''); fc()">
                  <i class="fas fa-list-ul"></i>
                </button>

                <input type="file" accept="image/*" id="file" style="display: none;" onchange="getImage()">
                <button class="btn btn-dark ml-3" data-toggle="tooltip" title="Masukkan gambar" onclick="document.getElementById('file').click()">
                  <i class="fas fa-images"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Masukkan link" onclick="link()">
                  <i class="fa fa-link"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Ubah warna teks" onclick="changeColor()">
                  <i class="fas fa-paint-brush"></i>
                </button>

                <button class="btn btn-dark ml-3" data-toggle="tooltip" title="Undo (Ctrl+Z)" onclick="document.execCommand('undo',false, ''); fc()">
                  <i class="fas fa-undo"></i>
                </button>
                <button class="btn btn-dark " data-toggle="tooltip" title="Redo (Ctrl+Y)" onclick="document.execCommand('redo',false, ''); fc()">
                  <i class="fas fa-redo"></i>
                </button>

                <button class="btn btn-dark ml-3" data-toggle="tooltip" title="Copy (Ctrl+C)" onclick="copy()">
                  <i class="fas fa-copy"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Cut (Ctrl+X)" onclick="document.execCommand('cut',false, ''); fc()">
                  <i class="fas fa-cut"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Delete (backspace)" onclick="document.execCommand('delete',false, ''); fc()">
                  <i class="fas fa-trash"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Select all (Ctrl+A)" onclick="fc(); document.execCommand('selectAll',false, '')">
                  <i class="fab fa-scribd"></i>
                </button>
                <button class="btn btn-dark" data-toggle="tooltip" title="Sematkan instagram" onclick="embedInstagram()">
                  <i class="fab fa-instagram"></i>
                </button>
              </div>
                <div id="isi_content" class="editor mt-3 mb-3 p-2 form-control" contenteditable>{{ old('isi_artikel') ?: $data['isi']}}</div>
                <div class="form-group row text-left" >
                  <div style="width: 100%; height: auto;">
                    <div class="col-2">File PDF (Optional)</div>
                    <div class="col-10 pt-2 pb-2">
                      <button class="btn btn-primary" onclick="document.getElementById('pdf_hidden').click()">Pilih PDF</button>
                      <span id="pdf-name" style="font-weight: normal"></span>
                    </div>
                  </div>
                </div>
                <div id="pdf_view"></div>
                <div class="form-group row text-left">
                  <div class="col-12">
                    <input type="text" class="form-control" placeholder="Tag / kategori" id="kategori"
                           value="{{ old('kategori') ?: ($data ? $data['kategori'] : '') }}">
                  </div>
                </div>
                <div class="form-group row text-left">
                  <div class="col-12">
                    <input type="text" class="form-control" placeholder="Thumbnail photo url (optional)" id="thumbnail"
                           value="{{ old('thumbnail') ?: ($data ? $data['thumbnail'] : '') }}">
                  </div>
                </div>
                <button class="sai btn btn-primary float-right" onclick="simpan()">Simpan</button>
              <button class="btn btn-secondary float-right mr-3" onclick="location.href = '{{ url('article/admin') }}'">kembali</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script async src="//www.instagram.com/embed.js"></script>
  <script src="{{ asset('js/artikel_tambah.js') }}"></script>
  <script>
    function setName(path) {
        let sp = path.split('\\');
        document.getElementById('pdf-name').innerText = sp[sp.length-1];
    }

    // window.onload = function() {
    //     document.getElementById("inserter").onmousedown = function() {
    //         var editor = document.getElementById("isi_content");
    //         if (nodeContainsSelection(editor)) {
    //             insertHTML();
    //             return false;
    //         }
    //     };
    // };




  </script>
  @if($data)
    <script>
        const html = document.getElementById('isi_content').innerText;
        document.getElementById('isi_content').innerHTML = html;
    </script>
  @endif
  <style>
    .editor{
      width:100%;
      height: 70vh;
      border: 1px solid rgba(0, 0, 0, 0.125);
      border-radius: 5px;
      overflow-y: auto;
    }
    .instagram-media{margin:15px auto !important;}
  </style>
@endsection