@extends('layouts.shards')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-3">
          <div class="col-10 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title">Data jenis produk</h3>
          </div>
          <div class="col-2">
            <a href="{{ url('product-type/admin/add') }}">
              <button type="button" class="btn btn-primary float-right">
                Tambah Data
              </button>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4 overflow-hidden">
              <div class="card-header border-bottom px-3">
                <div class="row">
                  <div class="col" style="height: 35px">
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">search</i>
                        </span>
                      </span>
                      <input name="nama" type="text" class="form-control" placeholder="Cari data jenis produk" aria-label="Nama">
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body p-0 text-left overflow-auto">
                <table class="table table-sm mb-0 table-hover">
                  <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0 text-center" style="min-width: 35px; width: 35px">ID</th>
                    <th scope="col" class="border-0">Keterangan</th>
                    <th scope="col" class="text-right border-0" style="min-width: 70px; max-width: 80px; width: 80px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $d)
                    <tr>
                      <td class="border-right text-center">{{ $d['id'] }}</td>
                      <td class="border-right">{{ $d['keterangan'] }}</td>
                      <td class="text-right">
                        <form action="{{ url('product-type/admin/'.$d['id']) }}" method="POST">
                          @method('DELETE')
                          @csrf
                          <button type="submit" class="btn btn-sm  btn-outline-danger float-right" style="font-size: 16px; padding: 0 4px">
                            <i class="material-icons">delete</i>
                          </button>
                        </form>
                        <a href="{{ url('product-type/admin/edit/'.$d['id']) }}" class="float-right mr-2">
                          <button type="button" class="btn btn-sm  btn-outline-primary float-right" style="font-size: 16px;  padding: 0 4px">
                            <i class="material-icons">edit</i>
                          </button>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style>
    .overflow-x-auto > .row {
      overflow-x: auto;
      white-space: nowrap;
    }
  </style>
@endsection