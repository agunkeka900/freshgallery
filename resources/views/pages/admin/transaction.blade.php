@extends('layouts.shards')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-3">
          <div class="col-10 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title">Transaksi Pembelian</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4 overflow-hidden">
              <div class="card-header border-bottom px-3">
                <div class="row py-3">
                  <div class="col-4 py-0 px-0 border-right">
                    <div class="form-group text-left mb-0">
                      <label for="order-list-filter-status" class="col-12 pt-0 col-form-label text-left">Filter Status</label>
                      <div class="col-12 py-0">
                        <select class="form-control" id="order-list-filter-status" onchange="debounceFilterOrder('{{url('')}}')">
                          <option value="all" selected>Semua</option>
                          @foreach($statuses as $s)
                            <option value="{{ $s['status'] }}" {{ $status == $s['status'] ? 'selected' : '' }}>{{ $s['keterangan2'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-4 py-0 border-right">
                    <div class="form-group text-left row mb-0">
                      <label for="order-list-filter-start-date" class="col-6 pt-0 pr-1 col-form-label text-left">Filter Tanggal Awal</label>
                      <label for="order-list-filter-end-date" class="col-6 pt-0 pl-1 col-form-label text-left">Filter Tanggal Akhir</label>
                      <div class="col-6 pr-1 py-0">
                        <input type="date" class="form-control" id="order-list-filter-start-date" value="{{ $initial_date }}" oninput="debounceFilterOrder('{{url('')}}')">
                      </div>
                      <div class="col-6 pl-1 py-0">
                        <input type="date" class="form-control" id="order-list-filter-end-date" value="{{ date('Y-m-d') }}" oninput="debounceFilterOrder('{{url('')}}')">
                      </div>
                    </div>
                  </div>
                  <div class="col-4 py-0 px-0">
                    <div class="form-group text-left mb-0">
                      <label for="order-list-filter-search" class="col-12 pt-0 col-form-label text-left">Cari</label>
                      <div class="col-12 py-0">
                        <input type="text" class="form-control" id="order-list-filter-search" placeholder="Cari Nama Barang & No Pesanan" oninput="debounceFilterOrder('{{url('')}}')">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body pt-0" id="order-list-filter-loading" style="display: none;">
                  <div class="spinner-border spinner-border-sm mx-auto" role="status"
                       style="color: gray; border-width: 0.15em; display: block;  margin-top: 10px"></div>
                </div>
                <div class="card-body pt-0" id="order-list-filter-loading" style="display: none;">
                  <div class="spinner-border spinner-border-sm mx-auto" role="status"
                       style="color: gray; border-width: 0.15em; display: block;  margin-top: 10px"></div>
                </div>
                <div class="card-body pt-0 px-0" id="order-list-item-container">
                  @include('components.order.item_order_list')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('components.order.order_detail_modal')

  <script>
    let timeoutID = null;
  </script>
  <script src="{{ asset('js/transaction_order_list.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('css/transaction_order_list.css') }}">
  <style>
    .overflow-x-auto > .row {
      overflow-x: auto;
      white-space: nowrap;
    }
  </style>
@endsection