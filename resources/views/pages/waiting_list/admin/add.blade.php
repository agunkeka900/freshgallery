@extends('layouts.shards')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title float-left">
              <a href="{{ url('waiting-list/admin') }}"><i class="material-icons">arrow_back</i></a>
              Tambah Wating List</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <ul class="list-group list-group-flush">
                <li class="list-group-item px-3">
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ ucfirst($error) }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @elseif ($message = Session::get('success'))
                    <div class="alert alert-success"> {{ $message }} </div>
                  @endif

                    <form method="post" action="{{ url('waiting-list/admin') }}">
                      @csrf
                      <div class="form-group row text-left">
                        <label for="judul" class="col-sm-3 col-form-label text-right">Judul</label>
                        <div class="col-sm-9">
                          <input type="text" id="judul" class="form-control" placeholder="Judul"
                                 name="judul" value="{{ old('judul') }}">
                        </div>
                      </div>
                      <div class="form-group row text-left">
                        <label for="tanggal_pesanan" class="col-sm-3 col-form-label text-right">Tanggal pesanan</label>
                        <div class="col-sm-9">
                          <input type="date" id="tanggal_pesanan" class="form-control" placeholder="Tanggal pesanan"
                                 name="tanggal_pesanan" value="{{ old('tanggal_pesanan') }}">
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ url('waiting-list/admin') }}'">Kembali</button>
                    </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection