@extends('layouts.shards')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title float-left">
              <a href="{{ url('waiting-list/customer/'.$id_wl ) }}"><i class="material-icons">arrow_back</i></a>
              Edit Product Waiting List</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <ul class="list-group list-group-flush">
                <li class="list-group-item px-3">
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ ucfirst($error) }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @elseif ($message = Session::get('success'))
                    <div class="alert alert-success"> {{ $message }} </div>
                  @endif
                  <form method="post" action="{{ url('waiting-list/product') }}">
                    @csrf @method('PUT')
                    <input type="hidden" name="id" value="{{ $id }}">
                    <div class="form-group row text-left">
                      <label for="nama_produk" class="col-sm-3 col-form-label text-right">Nama produk</label>
                      <div class="col-sm-9">
                        <input type="text" id="nama_produk" class="form-control" placeholder="Nama produk"
                               name="nama_produk" value="{{ old('nama_produk') ?: $data['nama_produk'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="jumlah" class="col-sm-3 col-form-label text-right">Jumlah</label>
                      <div class="col-sm-9">
                        <input type="number" id="jumlah" class="form-control" placeholder="Jumlah"
                               name="jumlah" value="{{ old('jumlah') ?: $data['jumlah'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="satuan" class="col-sm-3 col-form-label text-right">Satuan</label>
                      <div class="col-sm-9">
                        <input type="text" id="satuan" class="form-control" placeholder="Satuan"
                               name="satuan" value="{{ old('satuan') ?: $data['satuan'] }}">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                    <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ url('waiting-list/customer/'.$data['id_waiting_list']) }}'">Kembali</button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection