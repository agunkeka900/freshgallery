@extends('layouts.shards')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title float-left">
              <a href="{{ url('waiting-list/customer/'.$data['id_waiting_list']) }}"><i class="material-icons">arrow_back</i></a>
              Edit Customer Waiting List</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <ul class="list-group list-group-flush">
                <li class="list-group-item px-3">
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ ucfirst($error) }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @elseif ($message = Session::get('success'))
                    <div class="alert alert-success"> {{ $message }} </div>
                  @endif
                  <form method="post" action="{{ url('waiting-list/customer') }}">
                    @csrf @method('PUT')
                    <input type="hidden" name="id" value="{{ $id }}">
                    <div class="form-group row text-left">
                      <label for="nama_pemesan" class="col-sm-3 col-form-label text-right">Nama pemesan</label>
                      <div class="col-sm-9">
                        <input type="text" id="nama_pemesan" class="form-control" placeholder="Nama pemesan"
                               name="nama_pemesan" value="{{ old('nama_pemesan') ?: $data['nama_pemesan'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="lokasi" class="col-sm-3 col-form-label text-right">Lokasi</label>
                      <div class="col-sm-9">
                        <input type="text" id="lokasi" class="form-control" placeholder="Lokasi"
                               name="lokasi" value="{{ old('lokasi') ?: $data['lokasi'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="keterangan" class="col-sm-3 col-form-label text-right">Keterangan</label>
                      <div class="col-sm-9">
                        <input type="text" id="keterangan" class="form-control" placeholder="Keterangan"
                               name="keterangan" value="{{ old('keterangan') ?: $data['keterangan'] }}">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                    <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ url('waiting-list/customer/'.$data['id_waiting_list']) }}'">Kembali</button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection