@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-6 mx-auto">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <img src="{{ asset('img/logo_vertikal.jpeg') }}" alt="logo" height="100px" class="mx-auto">
                <div class="col-12 text-center mt-3">Password baru telah tersimpan</div>
                <div class="col-12 text-center mt-3 text-secondary" style="font-size: 12px">Gunakanlah password <br>tersebut saat login</div>
                <div class="col-12">
                  <div style="width: fit-content" class="mx-auto mt-3">
                    @if(Auth::user())
                      <a href="{{ url('/') }}" class="btn btn-primary ">
                        Beranda
                      </a>
                    @else
                      <a href="{{ url('viewLogin') }}" class="btn btn-primary ">
                        Login
                      </a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection