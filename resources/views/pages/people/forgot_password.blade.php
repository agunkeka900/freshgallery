@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-6 mx-auto">
          <div class="card">
            <div class="card-header">Masukkan Email Anda</div>
            <div class="card-body p-3">
              <form class="row" action="{{ url('people/mail/forgot-password') }}" method="POST" id="people-form-forgot-password">
                @csrf
                <div class="col-12">
                  <input type="text" class="form-control" autocomplete="off" id="people-forgot-email"
                         placeholder="Contoh: example@gmail.com" name="email">
                </div>
              </form>
              <div class="row">
                <div class="col-12">
                  {{--@if (count($errors) > 0)--}}
                    {{--<div class="text-warning">{{ $error }}</div>--}}
                  {{--@endif--}}
                  <button class="btn btn-primary mt-3 float-right" type="submit" onclick="validasiEmailForgotPassword()">
                    <i class="fas fa-arrow-right"></i> &nbsp; Selanjutnya
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    function validasiEmailForgotPassword(){
        let e = ELEMENT.people;
        let email = e.forgot_email;

        if(!email) alert('Email harus diisi!');
        else e.form_forgot_password.submit();
    }
  </script>
@endsection