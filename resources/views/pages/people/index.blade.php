@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">Biodata Diri</div>
            <div class="card-body p-3">
              <div class="row">
                <div class="col-4">
                  <div class="card">
                    <div class="card-header p-3">
                      <img class="card-img" style="background-size: cover; height: 300px" src="{{ Auth::user()->foto ? asset('img_people/'.Auth::user()->foto) : asset('img/default_user.jpg') }}" id="people-foto" alt="people-photo">
                      <div class="text-secondary text-center py-2" style="font-size: 12px">Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</div>
                      <input type="file" name="photo_file" id="people-input-file-foto" onchange="onFileSelected(event)" style="display: none">
                      <progress class="mt-2" id="people-progress-foto" value="0" max="100" style="width: 100%; display: none"></progress>
                      <div class="text-center font-weight-bold mt-2" id="people-status-foto" style="display: none"></div>
                      <div class="btn btn-secondary mt-2" style="width: 100%" id="people-btn-pilih-foto" onclick="choosePhoto()">Pilih Foto</div>
                      <div class="btn btn-primary mt-2" style="width: 100%; display: none" id="people-btn-save-foto" onclick="uploadPhoto()">
                        <i class="fas fa-save"></i> &nbsp; Simpan Foto
                      </div>
                    </div>
                  </div>
                  <div class="btn btn-primary mt-2" style="width: 100%" id="people-btn-password" onclick="changePassword()">
                    <i class="fas fa-wrench"></i> &nbsp; Ubah Kata Sandi
                  </div>
                  <div class="col-12">
                    <div class="float-right text-center pt-3" id="people-msg-password-success"
                         style="display: none;"
                    >
                      Silahkan cek email anda, kami telah mengirimkan link ubah password ke email {{ Auth::user()->email }}
                    </div>
                    <div class="spinner-border spinner-border-sm mx-auto" role="status" id="people-btn-password-loading"
                         style="color: gray; border-width: 0.15em; display: none;  margin-top: 26px"></div>
                  </div>
                </div>
                <div class="col-8">
                  <div class="col-12 font-weight-bold">Ubah Biodata Diri</div>
                  <div class="col-12 pt-2">
                    <table class="font-dg table-biodata" style="font-size: 13px; width: 100%">
                      <tr>
                        <td style="width: 120px;">Nama</td>
                        <td style="width: 10px">:</td>
                        <td>
                          <input type="text" class="form-control" autocomplete="off" id="people-nama"
                                 placeholder="Nama" value="{{ $nama }}" oninput="showBtnBiodata()">
                        </td>
                      </tr>
                      <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>
                          <select class="form-control" id="people-gender" onchange="showBtnBiodata()">
                            <option disabled selected>Jenis kelamin</option>
                            @foreach($itemGender as $i)
                              <option {{ $data['id_gender'] == $i['id'] ? 'selected' : '' }}
                                      id="{{ 'gender'.$i['id'] }}" value="{{ $i['id'] }}">{{ $i['keterangan'] }}</option>
                            @endforeach
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td>
                          <input type="date" id="people-tgl-lahir" class="form-control" value="{{ $data['tgl_lahir'] }}"
                                 oninput="showBtnBiodata()">
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div class="col-12 mt-5 font-weight-bold">Ubah Kontak</div>
                  <div class="col-12 pt-2">
                    <table class="font-dg table-biodata" style="font-size: 13px; width: 100%">
                      <tr>
                        <td style="width: 120px;">Email</td>
                        <td style="width: 10px">:</td>
                        <td>{{ Auth::user()->email }}</td>
                      </tr>
                      <tr>
                        <td>Nomor telpon</td>
                        <td>:</td>
                        <td>
                          <input type="text" class="form-control" autocomplete="off" id="people-telp"
                                 placeholder="Nomor Telpon" value="{{ $data['telp'] }}" oninput="showBtnBiodata()">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                          <div class="text-primary float-right" id="people-msg-save-success" style="display: none;">Biodata Tersimpan</div>
                          <div class="spinner-border spinner-border-sm float-right" role="status" id="people-btn-save-loading"
                               style="color: gray; border-width: 0.15em; display: none;  margin-top: 17px"></div>
                          <button class="btn btn-primary mt-2 float-right" id="people-btn-save-biodata"
                                  style="visibility: hidden"
                                  onclick="saveBiodata()">
                            <i class="fas fa-save"></i> &nbsp; Simpan Biodata
                          </button>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <link rel="stylesheet" href="{{ asset('css/transaction_order_list.css') }}">
  <script>
    let photo_files;
    function showBtnBiodata(show = true){
        ELEMENT.people.btn_save_biodata.style.visibility = !!show ? 'visible' : 'hidden'
        ELEMENT.people.msg_save_success.style.display = 'none';
    }
    function saveBiodata(){
        $(document).ready(function() {
            let e = ELEMENT.people;
            e.btn_save_loading.style.display = 'block';
            e.btn_save_biodata.style.visibility = 'hidden';
            showBtnBiodata(false);
            $.ajax({
                url: '{{ url('people/update-biodata') }}',
                type: 'post',
                data: {
                    _token: TOKEN,
                    nama: e.nama.value,
                    tgl_lahir: e.tgl_lahir.value,
                    gender: e.gender.options[e.gender.selectedIndex].value,
                    telp: e.telp.value
                },
                success: function(data) {
                    e.btn_save_loading.style.display = 'none';
                    e.msg_save_success.style.display = 'block';
                },
                error: function(data) {
                    e.btn_save_loading.style.display = 'none';
                    showBtnBiodata();
                },
            });
        });
    }
    function changePassword(){
        $(document).ready(function() {
            let e = ELEMENT.people;
            e.btn_password_loading.style.display = 'block';
            e.btn_password.style.display = 'none';
            $.ajax({
                url: '{{ url('people/mail/change-password').'/'.Auth::user()->id }}',
                type: 'get',
                data: {
                    _token: TOKEN,
                },
                success: function(data) {
                    e.btn_password_loading.style.display = 'none';
                    e.msg_password_success.style.display = 'block';
                },
                error: function(data) {
                    e.btn_password_loading.style.display = 'none';
                    e.btn_password.style.display = 'block';
                },
            });
        });
    }
    function choosePhoto(){
        let e = ELEMENT.people;
        e.input_file_foto.click();
    }
    function onFileSelected (event) {
        let e = ELEMENT.people;
        try {
            const fr = new FileReader();
            photo_files = event.target.files[0];
            fr.readAsDataURL(photo_files);
            fr.onload = (event) => {
                e.foto.src = event.target.result;
                e.btn_save_foto.style.display = 'block';
                e.btn_pilih_foto.style.display = 'none';
                e.status_foto.style.display = 'none';
            };
        }
        catch (e) {
            console.log(e);
        }
    }
    function uploadPhoto() {
        let e = ELEMENT.people;
        let file = photo_files;

        let form_data = new FormData();
        form_data.append("photo_file", file);
        form_data.append("_token", "{{ csrf_token() }}");

        e.status_foto.style.display = 'none';

        let ajax = new XMLHttpRequest();
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.open("POST", "{{ url('people/photo-upload') }}");
        ajax.send(form_data);

        e.btn_save_foto.style.display = 'none';
        e.progress_foto.style.display = 'block';
    }

    function progressHandler(event) {
        let e = ELEMENT.people;
        let percent = (event.loaded / event.total) * 100;
        e.progress_foto.value = Math.round(percent);
    }

    function completeHandler(event) {
        let e = ELEMENT.people;
        let response = JSON.parse(event.target.responseText);
        let html = response[0] === 0 ? "<div class='text-danger'>"+ response[1] +"</div>" :
            "<div class='text-primary'><i class='fas fa-check'></i> &nbsp; "+ response[1] +"</div>";
        e.status_foto.innerHTML = html;
        e.status_foto.style.display = 'block';
        e.progress_foto.style.display = 'none';
        ELEMENT.navbar_top.user.photo.src = e.foto.src;
    }

    function errorHandler(event) {
        let e = ELEMENT.people;
        e.status_foto.innerHTML = "<div class='text-danger'>Gagal menyimpan foto</div>";
        e.status_foto.style.display = 'block';
        e.btn_pilih_foto.style.display = 'block';
        e.progress_foto.style.display = 'none';
    }
  </script>

  <style>
    .table-biodata tr td{
      padding-top: 5px;
      padding-bottom: 5px;
    }
  </style>
@endsection