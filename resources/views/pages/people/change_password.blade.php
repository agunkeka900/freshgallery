@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-6 mx-auto">
          <div class="card">
            <div class="card-header">Ubah Password</div>
            <div class="card-body p-3">
              <form class="row" action="{{ url('people/change-password') }}" method="POST" id="people-form-change-password">
                @csrf
                <input type="hidden" value="{{ $unique_string }}" name="unique_string">
                <div class="col-12">
                  Masukkan password baru
                </div>
                <div class="col-12">
                  <input type="password" class="form-control" autocomplete="off" id="people-password-baru"
                         placeholder="Password baru" name="password_baru">
                </div>
                <div class="col-12 mt-3">
                  Ulangi masukkan password baru
                </div>
                <div class="col-12">
                  <input type="password" class="form-control" autocomplete="off" id="people-password-baru2"
                         placeholder="Ulangi Password baru" name="password_baru2">
                </div>
              </form>
              <div class="row">
                <div class="col-12">
                  <button class="btn btn-primary mt-3 float-right" type="submit" onclick="validasiSavePassword()">
                    <i class="fas fa-save"></i> &nbsp; Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    function validasiSavePassword(){
        let e = ELEMENT.people;
        let pass1 = e.password_baru.value;
        let pass2 = e.password_baru2.value;

        if(pass1 !== pass2) alert('Kedua isian password harus sama!');
        else if(pass1.length < 6) alert('Password minimal 6 digit!');
        else e.form_change_password.submit();
    }
  </script>
@endsection