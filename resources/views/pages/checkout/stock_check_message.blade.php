@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row pt-3 mx-auto pb-2" style="width: 200px;">
        <img src="{{ asset('img/logo_vertikal.jpeg') }}" alt="logo" style="width: 200px; height: 200px">
      </div>
      <div class="text-center font-weight-bold">Terima kasih telah memesan</div>
      <div class="text-center pt-2">Stok sedang di cek</div>
      <div class="text-center text-secondary" style="font-size: 12px">kami akan segera informasikan <br> ketersediaan stok melalui email</div>
      <div class="row pt-3 mx-auto" style="width: 70px">
        <a href="{{ url('/') }}">
          <button class="btn btn-primary btn-lg">Oke</button>
        </a>
      </div>
    </div>
  </section>
@endsection