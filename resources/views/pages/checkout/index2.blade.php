@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title mb-0">Pembayaran dan Pengiriman</h5>
          </div>
          <div class="card-body">
            <form method="post" action="{{ route('checkout/savePembayaran') }}">
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              {{ csrf_field() }}
              <div class="form-group row text-left">
                <label for="metode" class="col-12 col-form-label text-left">Metode Pembayaran</label>
                <div class="col-12">
                  <select class="form-control" id="metode" name="metode">
                    <option disabled selected>Pilih metode pembayaran</option>
                    <option id="metode1" value="1">Cash on Delivery</option>
                  </select>
                  <script>document.getElementById('metode'+'{{ old('metode') }}').setAttribute('selected','')</script>
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="alamat" class="col-12 col-form-label text-left">Alamat Pengiriman</label>
                <div class="col-12">
                  <input type="text" id="alamat" class="form-control" placeholder="Alamat" name="alamat" value="{{ old('alamat') }}">
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="waktu" class="col-12 col-form-label text-left">Waktu Pengiriman</label>
                <div class="col-12">
                  <select class="form-control" id="waktu" name="waktu">
                    <option disabled selected>Pilih waktu pengiriman</option>
                    <option id="waktu1" value="1">Siang (11.00 - 14.59)</option>
                    <option id="waktu2" value="2">Sore (15.00 - 18.00)</option>
                  </select>
                  <script>document.getElementById('waktu'+'{{ old('waktu') }}').setAttribute('selected','')</script>
                </div>
              </div>
              <div class="form-group row text-left">
                <label for="catatan" class="col-12 col-form-label text-left">Catatan</label>
                <div class="col-12">
                  <textarea id="catatan" class="form-control" placeholder="Catatan" name="catatan">{{ old('catatan') }}</textarea>
                </div>
              </div>
              <input type="hidden" id="barang" name="barang" value="">
              <button type="submit" class="btn btn-primary float-right">Lanjut</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script src="{{ asset('js/beliProduk.js') }}"></script>
  <script>
    document.getElementById('barang').value = JSON.stringify(getArrCookieCart());
  </script>
@endsection