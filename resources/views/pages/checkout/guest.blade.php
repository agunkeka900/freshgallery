@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row pt-3 mx-auto" style="width: 200px;">
        <img src="{{ asset('img/logo_vertikal.jpeg') }}" alt="logo" style="width: 200px; height: 200px">
      </div>
      <div class="text-center">Belum punya akun di Fresh Gallery? Yuk daftar</div>
      <div class="text-center text-secondary" style="font-size: 12px">Login aja kalau udah punya</div>
      <div class="row pt-3 mx-auto" style="width: 320px">
        <a href="{{ route('viewRegister') }}">
          <button class="btn btn-outline-primary btn-lg btn-daftar mr-2" >Daftar</button>
        </a>
        <a href="{{ route('viewLogin', ['param'=>'redirect=checkout']) }}">
          <button class="btn btn-primary btn-lg btn-daftar">Login</button>
        </a>
      </div>
      {{--<div class="row pt-3 mx-auto" style="width: 320px">--}}
        {{--<a href="{{ route('checkout/dataDiri') }}">--}}
          {{--<button class="btn btn-primary btn-lg btn-lanjut mr-2" >LANJUT TANPA DAFTAR</button>--}}
        {{--</a>--}}
      {{--</div>--}}
    </div>
  </section>

  <style>
    .btn-daftar{
      width: 150px;
    }
    .btn-lanjut{
      width: 310px;
    }
  </style>
@endsection