@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
{{--        Ringkasan Belanja--}}
        <div class="col-md-6 col-sm-12">
          <div class="card card-produk mb-3">
            <div class="card-header">
              <h5 class="card-title mb-0">Ringkasan Belanja</h5>
            </div>
            <div class="card-body pt-0">
              <div class="row">
                <div id="checkout-item-ringkasan-belanja" style="width: 100%"></div>
                <div class="col-12 pt-3">
                  <span style="font-size: 14px;">Total harga</span>
                  <span id="cartHarga2" style="font-weight: 600; float: right"></span>
                </div>
              </div>
            </div>
          </div>
        </div>

{{--        Pembayaran & Pengiriman--}}
        <div class="col-md-6 col-sm-12">
          <div class="card card-produk">
            <div class="card-header">
              <h5 class="card-title mb-0">Pembayaran & Pengiriman</h5>
            </div>
            <div class="card-body">
              <form method="post" action="{{ url('checkout') }}">
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                {{ csrf_field() }}
  {{--              <div class="form-group row text-left">--}}
  {{--                <button id="modal" type="button" class="btn btn-primary" data-toggle="modal" data-target="#address">--}}
  {{--                  Launch demo modal--}}
  {{--                </button>--}}
  {{--              </div>--}}
                <div class="form-group row text-left">
                  <label for="metode" class="col-12 col-form-label text-left">Metode Pembayaran</label>
                  <div class="col-12">
                    <select class="form-control" id="metode" name="metode">
                      <option disabled>Pilih metode pembayaran</option>
                      <option id="metode1" value="1" selected>Cash on Delivery</option>
                    </select>
                    <script>document.getElementById('metode'+'{{ old('metode') }}').setAttribute('selected','')</script>
                  </div>
                </div>
                <div class="form-group row text-left">
                  <label for="alamat_id" class="col-12 col-form-label text-left">Alamat Pengiriman</label>
                  <div class="col-12">
                    <div style="width: 100%; border: 1px solid #ced4da; border-radius: 0.25em; display: {{$address_primary ? 'block' : 'none'}}" id="alamat-container">
                      <div class='col-12'>
                        <div class='card-body row'>
                          <div class='font-weight-bold col-12' id="alamat-label">{{ $address_primary ? $address_primary['label'] : '' }} <span class='text-primary'> (Utama)</span></div>
                          <div class='col-12' id="alamat-wilayah">{{ $address_primary ? $address_primary['complete_region'] : '' }}</div>
                          <div class='col-12'id="alamat-lengkap">{{ $address_primary ? $address_primary['address'] : '' }}</div>
                          <div class='col-12 pt-2'>
                            <button class='btn btn-outline-secondary' type="button" data-toggle="modal" data-target="#address-modal" onclick="showModalAddressList()">Pilih Alamat Lain</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button class='btn btn-outline-secondary' type="button" data-toggle="modal" data-target="#address-modal"
                            id="btn-masukkan-alamat"
                            style="display: {{$address_primary ? 'none' : 'block'}}"
                            onclick="showModalAddressList(); showAddress('add')">Masukkan alamat</button>

                    <input type="hidden" id="alamat-id" class="form-control" placeholder="Alamat" name="alamat" value="{{ old('alamat') ?: ($address_primary ? $address_primary['id'] : '') }}">
                  </div>
                </div>
                <div class="form-group row text-left">
                  <label for="waktu" class="col-12 col-form-label text-left">Waktu Pengiriman</label>
                  <div class="col-12">
                    <select class="form-control" id="waktu" name="waktu">
                      <option disabled selected>Pilih waktu pengiriman</option>
                      @foreach($waktu_kirim as $wk)
                        <option id="waktu{{ $wk->id }}" value="{{ $wk->id }}">{{ $wk->keterangan }}</option>
                      @endforeach
                    </select>
                    <script>document.getElementById('waktu'+'{{ old('waktu') }}').setAttribute('selected','')</script>
                  </div>
                </div>
                <div class="form-group row text-left">
                  <label for="catatan" class="col-12 col-form-label text-left">Catatan</label>
                  <div class="col-12">
                    <textarea id="catatan" class="form-control" placeholder="Catatan" name="catatan">{{ old('catatan') }}</textarea>
                  </div>
                </div>
                  <div style="height: 34px">
                    <button id="btn-checkout" type="submit" class="btn btn-primary float-right">Lanjut</button>
                    <div id="btn-checkout-loading"
                         class="spinner-border spinner-border-sm ml-2 float-right" role="status"
                         style="color: gray; border-width: 0.15em; display: none; margin-top: 10px; "></div>
                  </div>
              </form>
              <input type="hidden" id="cart" name="cart" value="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('components.checkout.address')
  @include('components.checkout.address_geolocation')

  <script src="{{ asset('js/beliProduk.js') }}"></script>
  {{--<script src="{{ asset('jquery/jquery.min.js') }}"></script>--}}
  <script>
    let address_lists = [];
    // let timeoutID = null;

    function showAddress(type) {
      if(type === null){
        // $('#address').modal('hide');
        $('#address-geolocation').modal('show');
      }
      else{
          let types = ['list','add','edit'];
          let titles = ['Pilih Alamat','Tambah Alamat','Ubah Alamat']
          for(let i=0; i<types.length; i++){
              document.getElementById('address-'+types[i]).style.display = (type === types[i] ? "block" : "none");
              if(type === types[i]){
                  ELEMENT.checkout.address.mode = type;
                  document.getElementById('address-modal-title').innerText = titles[i];
              }
          }
      }
    }
    function getAddressList(){
      $(document).ready(function() {
        $.ajax({
          url: '{{url('checkout/address')}}',
          type: 'get',
          data: $(this).serialize(),
          success: function(data) {
            setAddressList(data);
            address_lists = data;
          },
          error: function(data) {
              console.log(data)
          },
        });
      });
    }
    function setAddressList(data) {
        let h = "";
        for(let i=0; i<data.length; i++){
            let text_utama = data[i].primary === 0 ? "" : "<span class='text-primary'> (Utama)</span>";
            let text_lokasi = data[i].province +", "+ data[i].city +", "+ data[i].district +", "+ data[i].village;
            let text_jadikan = data[i].primary === 1 ? "" : "<button class='btn btn-secondary' onclick='setPrimaryAddress(`"+data[i].id+"`)'>Jadikan Alamat Utama</button>";
            h +="  <div class='col-12'>\n" +
                "    <div class='card mt-2'>\n" +
                "      <div class='card-body row'>\n" +
                "        <div class='font-weight-bold col-12'>"+data[i].label+text_utama+"</div>\n" +
                "        <div class='col-12'>"+text_lokasi+"</div>\n" +
                "        <div class='col-12'>"+data[i].address+"</div>\n" +
                "        <div class='col-12 pt-2'>\n" +
                "          <button class='btn btn-primary' onclick='setAddress(`"+data[i].id+"`,`"+data[i].label+"`,`"+text_lokasi+"`,`"+data[i].address+"`,`"+data[i].primary+"`)'>Pilih Alamat</button>\n" + text_jadikan +
                "          <button id='address-more-btn' type='button' class='btn btn-secondary' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>\n" +
                "            <i class='fas fa-ellipsis-v'></i>\n" +
                "          </button>\n" +
                "          <div class='dropdown-menu' aria-labelledby='address-more-btn'>\n" +
                "            <span class='dropdown-item' style='cursor: pointer' onclick='setAddressEdit(`"+data[i].id+"`)'>Ubah Alamat</span>\n" +
                "            <span class='dropdown-item' style='cursor: pointer' onclick='deleteAddress(`"+data[i].id+"`)'>Hapus Alamat</span>\n" +
                "          </div>" +
                "        </div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>"
        }
        document.getElementById('address-list-data').innerHTML = h;
    }
    function showModalAddressList(){
        getAddressList();
        showAddress('list');
    }
    function setAddress(id, label, region, address, primary, close_modal = true){
        let text_primary = primary == '1' ? "<span class='text-primary'> (Utama)</span>" : '';
        document.getElementById('alamat-id').value = id;
        document.getElementById('alamat-label').innerHTML = label + text_primary;
        document.getElementById('alamat-wilayah').innerText = region;
        document.getElementById('alamat-lengkap').innerText = address;
        document.getElementById('alamat-container').style.display = 'block';
        document.getElementById('btn-masukkan-alamat').style.display = 'none';
        if(close_modal !== false) document.getElementById('address-modal-close').click();
    }
    function setPrimaryAddress(id){
        $(document).ready(function() {
            $.ajax({
                url: '{{url('checkout/address/primary')}}'+'/'+id,
                type: 'get',
                data: $(this).serialize(),
                success: function(data) {
                    setAddressList(data);
                    address_lists = data;
                },
                error: function(data) {
                    console.log(data)
                },
            });
        });
    }
    function deleteAddress(id){
        $(document).ready(function() {
            $.ajax({
                url: '{{url('checkout/address/delete')}}'+'/'+id,
                type: 'get',
                data: $(this).serialize(),
                success: function(data) {
                    setAddressList(data);
                    address_lists = data;

                    let id_alamat = document.getElementById('alamat-id').value;
                    if(id_alamat == id){
                        setAddress(null, '', '', '', 0, false)
                    }
                },
                error: function(data) {
                    console.log(data)
                },
            });
        });
    }
    function searchAddress(keyword){
        let keys = [];
        let results = [];

        if(keyword && keyword.includes(' ')) keys = keyword.split(' ');
        else keys.push(keyword)

        for(let i=0; i<address_lists.length; i++){
            let ali = address_lists[i];
            let flag = [];
            for(let a=0; a<keys.length; a++){
                let key = keys[a].toLowerCase();
                if(ali.label.toLowerCase().includes(key)) flag.push(1);
                else if(ali.address.toLowerCase().includes(key)) flag.push(1);
                else if(ali.village.toLowerCase().includes(key)) flag.push(1);
                else if(ali.city.toLowerCase().includes(key)) flag.push(1);
                else if(ali.district.toLowerCase().includes(key)) flag.push(1);
                else if(ali.province.toLowerCase().includes(key)) flag.push(1);
                else flag.push(0);
            }
            if(!flag.includes(0)) results.push(ali);
        }

        setAddressList(results);
    }
    function debounceSearchAddress(keyword){
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function () {
            searchAddress(keyword)
        }, 500);
    }
    function setAddressEdit(id){
        let ae = ELEMENT.checkout.address.edit;
        for (let i=0; i<address_lists.length; i++){
            let ali = address_lists[i];
            if(ali.id == id){
                ae.id.value = ali.id;
                ae.label.value = ali.label;
                ae.region_name.value = ali.province +', '+ ali.city +', '+ ali.district +', '+ ali.village;
                ae.region_id.value = ali.id_village;
                ae.complete_address.value = ali.address;
                ae.geolocation_text.innerText = !!ali.latitude ? (ali.latitude +', '+ ali.longitude) : TEXT.tandai_lokasi;
                ae.geolocation.value = !!ali.latitude ? ali.latitude +', '+ ali.longitude : null;
                ae.geolocation_reset_btn.style.display = !!ali.latitude ? 'block' : 'none';
                ae.primary_container.style.visibility = ali.primary == 1 ? 'hidden' : 'visible';
                ae.primary.checked = ali.primary == 1;
                ae.submit_message.innerHTML = '';
                if(ali.latitude) marker.setLatLng([ali.latitude, ali.longitude]);
            }
        }
        showAddress('edit');
    }
    function resetGeolocation() {
        let state = ELEMENT.checkout.address.mode === 'add' ?
            ELEMENT.checkout.address.add : ELEMENT.checkout.address.edit;
        state.geolocation.value = null;
        state.geolocation_text.innerText = TEXT.tandai_lokasi;
        state.geolocation_reset_btn.style.display = 'none';
    }
    // setTimeout(function () {
    //     document.getElementById('btn-masukkan-alamat').click();
    //     showAddress('list');
    //     getAddressList()
    // }, 500)
    // document.getElementById('cart').value = JSON.stringify(getArrCookieCart());
    setTimeout(setInitialProduk, 1000);
  </script>
@endsection