@extends('layouts.main')

@section('content')

  <section class="page-section pt-5 pb-0">
    <div class="container-fluid p-0 mt-3">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('img/slide_1.jpg') }}" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/slide_2.jpg') }}" alt="Second slide">
          </div>
        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </section>
  <section class="page-section pt-0">
    <div class="container pt-0" style="max-width: 1140px">
      <div class="row mb-5 px-2">
        @foreach($kategori as $k)
          @include('components.card_kategori')
        @endforeach
      </div>
      @foreach($data as $d)
        <div class="row px-2">
          <h5>{{ $d['jenis'] }}</h5>
        </div>
        <div class="row mb-5 px-1">
          @foreach($d['data'] as $k)
            @include('components.item_produk')
          @endforeach
        </div>
      @endforeach
    </div>
  </section>
@endsection
