@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        {{--Breadcrumb--}}
{{--        <div class="col-12 pl-0 pb-0 m-0">--}}
{{--          <nav aria-label="breadcrumb">--}}
{{--            <ol class="breadcrumb bg-transparent" style="font-size: 10pt">--}}
{{--              <li class="breadcrumb-item text-primary"><a href="{{ route('/') }}">Beranda</a></li>--}}
{{--              <li class="breadcrumb-item text-primary"><a href="{{ route('produk/viewDataGroup') }}">Produk</a></li>--}}
{{--              <li class="breadcrumb-item text-primary"><a href="{{ route('produk/viewDataGroup') }}">{{ $data['jenis'] }}</a></li>--}}
{{--              <li class="breadcrumb-item active" aria-current="page">{{ $data['nama'] }}</li>--}}
{{--            </ol>--}}
{{--          </nav>--}}
{{--        </div>--}}

        {{--Foto--}}
        <div class="col-sm-12 col-md-6 col-lg-4 col-foto mb-3">
          <div class="img-zoom-container">

            <img id="product-detail-photo" src="{{ asset('img_produk/'.$foto[0]['foto']) }}" width="300" height="240">
{{--          <img src="{{ asset('img_produk/'.$foto[0]['foto']) }}" class="card-img img-custom" id="product-detail-photo" alt="gambar produk">--}}
          <div id="product-detail-photo-zoom" class="img-zoom-result"></div>
          </div>
          @if(count($foto) > 1)
            <div class="col-12 pt-2">
              <div style="width: max-content" class="mx-auto">
                @php $arr_id_foto = []; @endphp
                @foreach($foto as $f)
                  @php $arr_id_foto[] = $f['id']; @endphp
                  <img src="{{ asset('img_produk/'.$f['foto']) }}"
                       class="img-thumbnail p-0 border-primary"
                       id="product-detail-thumbnail-{{ $f['id'] }}"
                       style="border-width: 0px"
                       onclick="changePhoto({{ $f['id'] }})">
                @endforeach
                <input type="hidden" id="product-detail-arr-photo" value="{{ json_encode($arr_id_foto) }}">
              </div>
            </div>
          @endif
        </div>

        {{--Nama, Harga, Dilihat, Terjual, Min Beli, Deskripsi, Satuan--}}
        <div class="col-sm-12 col-md-6 col-lg-8">
          <div class="col-12 px-3 py-0">
            {{--Nama--}}
            <div class="row">
              <div class="text-nama">{{ $data['nama'] }}</div>
            </div>

            {{--Harga--}}
            <div class="row pb-3">
              <div class="text-primary text-nama" style="width: 100%">
                Rp {{ number_format($data['harga'], 0, '', '.') }}
                <button
                  onclick="tambahBeli({{ $data }})"
                  class="btn btn-primary float-right"
                  style="text-transform: none; margin-top: -7px"
                  id="btnBeli{{$data['id']}}">
                  <i class="fas fa-shopping-cart pr-2"></i>Beli
                </button>

                {{--Button Beli--}}
                <div class="row" id="row{{ $data['id'] }}" style="display: none; width: 160px; float: right; margin-top: -5px">
                  <div style="width: 40px; padding: 0" class="pr-0">
                    <button
                      onclick="kurangBeli({{ $data }})"
                      style="width: 100%; background: #ebebeb"
                      class="btn btn-add-cart">
                      <i class="fas fa-minus text-secondary"></i>
                    </button>
                  </div>
                  <div class="col px-1">
                    <input type="number" value="0" id="jumlah{{ $data['id'] }}"
                           onfocusout="showJumlah({{ $data }})"
                           class="jumlah-item-produk"
                           style="border: none; width: 100%; text-align: center; font-size: 14px; height: 27px"
                    >
                  </div>
                  <div style="width: 40px; padding: 0" class="pl-0 mr-3">
                    <button
                      onclick="tambahBeli({{ $data }})"
                      style="width: 100%; background: #ebebeb"
                      class="btn btn-add-cart">
                      <i class="fas fa-plus text-secondary"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            {{--Dilihat, Terjual, Min Beli, Satuan--}}
            <div class="row">
              <div class="card py-2" style="width: 100%">
                <table>
                  <tbody>
                  <tr>
                    <td class="pl-3 pr-2" style="width: 30px">
                      <i class="fas fa-eye text-secondary"></i>
                    </td>
                    <td class="text-secondary border-right pr-3" style="line-height: 18px">
                      <span style="font-size: 9pt">Dilihat</span><br>
                      <b style="font-size: 10pt">{{ $data['view'] }}</b>
                    </td>

                    <td class="pl-3 pr-2" style="width: 30px">
                      <i class="fas fa-truck text-secondary"></i>
                    </td>
                    <td class="text-secondary border-right pr-3" style="line-height: 18px">
                      <span style="font-size: 9pt">Terjual</span><br>
                      <b style="font-size: 10pt">{{ $data['terjual'] }}</b>
                    </td>

{{--                    <td class="pl-3 pr-2" style="width: 30px">--}}
{{--                      <i class="fas fa-tags text-secondary"></i>--}}
{{--                    </td>--}}
{{--                    <td class="text-secondary border-right pr-3" style="line-height: 18px">--}}
{{--                      <span style="font-size: 9pt">Min. Beli</span><br>--}}
{{--                      <b style="font-size: 10pt">{{ $data['min_beli'] }}</b>--}}
{{--                    </td>--}}

                    <td class="pl-3 pr-2" style="width: 30px">
                      <i class="fas fa-tag text-secondary"></i>
                    </td>
                    <td class="text-secondary pr-3" style="line-height: 18px">
                      <span style="font-size: 9pt">Satuan</span><br>
                      <b style="font-size: 10pt">{{ $data['satuan'] }}</b>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>

            {{--Deskripsi--}}
            <div class="row pt-3">
              <span>{{ $data['keterangan'] }}</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row border-top mt-3 pt-3 pl-3">
        <h5>Rekomendasi produk</h5>
      </div>
      <div class="row mb-5 px-2">
        @foreach($terkait as $k)
          @include('components.item_produk')
        @endforeach
      </div>
    </div>
  </section>

  <script>
      function imageZoom(imgID, resultID) {
          var img, lens, result, cx, cy;
          img = document.getElementById(imgID);
          result = document.getElementById(resultID);
          /*create lens:*/
          lens = document.createElement("DIV");
          lens.setAttribute("class", "img-zoom-lens");
          /*insert lens:*/
          img.parentElement.insertBefore(lens, img);
          /*calculate the ratio between result DIV and lens:*/
          cx = result.offsetWidth / lens.offsetWidth;
          cy = result.offsetHeight / lens.offsetHeight;
          /*set background properties for the result DIV:*/
          result.style.backgroundImage = "url('" + img.src + "')";
          result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
          /*execute a function when someone moves the cursor over the image, or the lens:*/
          lens.addEventListener("mousemove", moveLens);
          img.addEventListener("mousemove", moveLens);
          /*and also for touch screens:*/
          lens.addEventListener("touchmove", moveLens);
          img.addEventListener("touchmove", moveLens);
          function moveLens(e) {
              var pos, x, y;
              /*prevent any other actions that may occur when moving over the image:*/
              e.preventDefault();
              /*get the cursor's x and y positions:*/
              pos = getCursorPos(e);
              /*calculate the position of the lens:*/
              x = pos.x - (lens.offsetWidth / 2);
              y = pos.y - (lens.offsetHeight / 2);
              /*prevent the lens from being positioned outside the image:*/
              if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
              if (x < 0) {x = 0;}
              if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
              if (y < 0) {y = 0;}
              /*set the position of the lens:*/
              lens.style.left = x + "px";
              lens.style.top = y + "px";
              /*display what the lens "sees":*/
              result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
          }
          function getCursorPos(e) {
              var a, x = 0, y = 0;
              e = e || window.event;
              /*get the x and y positions of the image:*/
              a = img.getBoundingClientRect();
              /*calculate the cursor's x and y coordinates, relative to the image:*/
              x = e.pageX - a.left;
              y = e.pageY - a.top;
              /*consider any page scrolling:*/
              x = x - window.pageXOffset;
              y = y - window.pageYOffset;
              return {x : x, y : y};
          }
      }
    function changePhoto(id){
        let e = ELEMENT.product.detail;
        let arr_id = JSON.parse(e.arr_photo.value);

        e.photo.src = e.thumbnail(id).src;
        for(let i=0; i<arr_id.length; i++){
            // console.log(arr_id[i] == id)
            // e.thumbnail(id).style.border = arr_id[i] == id ? 'solid blue 2px' : 'solid green 2px';
            if(arr_id[i] == id) e.thumbnail(arr_id[i]).style.border = 'solid blue 2px';
            else e.thumbnail(arr_id[i]).style.border = 'none';
        }
    }

      imageZoom("product-detail-photo", "product-detail-photo-zoom");
  </script>

  <style>
    .img-zoom-container {
      position: relative;
    }
    .img-zoom-lens {
      position: absolute;
      border: 1px solid #d4d4d4;
      /*set the size of the lens:*/
      width: 40px;
      height: 40px;
    }

    .img-zoom-result {
      border: 1px solid #d4d4d4;
      /*set the size of the result div:*/
      width: 300px;
      height: 300px;
    }

    .img-thumbnail{
      object-fit: cover;
      width: 50px;
      height: 50px;
      background-size: cover;
      float: left;
      margin-right: 5px;
      margin-left: 5px;
      cursor: pointer;
    }
    .img-custom{
      object-fit: cover;
      height: 350px;
      background-size: cover;
      background-position-y: 20%;
      background-position-x: 50%;
      border-radius: 0;
    }
    .col-foto{
      padding-left: 15px;
    }
    .text-nama{
      font-size: 20pt;
      font-weight: bold;
    }
    .page-section{
      padding-top: 100px;
    }
    /*.btn-add-cart{*/
    /*  margin-top: 5px;*/
    /*}*/
    .jumlah-item-produk{
      /*padding-left: 15px;*/
    }
    @media (max-width: 767px){
      .page-section{
        padding-top: 60px;
      }
      .text-nama{
        font-size: 14pt;
      }
      .container{
        width: 100%;
        max-width: 100%;
      }
      .col-foto{
        width: 100%;
        padding: 0;
      }
      .img-custom{
        height: 500px;
      }
      /*.btn-add-cart{*/
      /*  margin-top: 0;*/
      /*}*/
      .jumlah-item-produk{
        /*padding-left: 0;*/
      }
    }
    @media (max-width: 576px) {
      .img-custom{
        height: 350px;
      }
    }
    /*.*/
  </style>
@endsection