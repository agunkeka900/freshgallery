@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container">
      <div class="row">
        <div class="col text-center text-secondary">
          <p>Semua Kategori</p>
        </div>
      </div>
      <div class="row">
        @foreach($data as $k)
          @include('components.card_kategori')
        @endforeach
      </div>
    </div>
  </section>
  <style>
    .card-hover:hover{
      background: rgba(0, 0, 0, 0.03);
      cursor: pointer;
    }
  </style>
@endsection