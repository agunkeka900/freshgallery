@extends('layouts.shards')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-3">
          <div class="col-10 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title">Data produk</h3>
          </div>
          <div class="col-2">
            <a href="{{ url('product/admin/add') }}">
              <button type="button" class="btn btn-primary float-right">
                Tambah Data
              </button>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4 overflow-hidden">
              <div class="card-header border-bottom px-3">
                <div class="row">
                  <div class="col-sm-6 col-md-3 mb-2 mb-md-0" style="height: 35px">
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">search</i>
                        </span>
                      </span>
                      <input name="nama" type="text" class="form-control" placeholder="Cari data produk" aria-label="Nama">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3 mb-2 mb-md-0" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select name="jenis_kelamin" class="form-control">
                        <option selected disabled>Filter Supplier</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select name="jenis_kelamin" class="form-control">
                        <option selected disabled>Filter Jenis</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3" style="height: 35px">
                    <div class="input-group mb-3">
                      <span class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">person_outline</i>
                        </span>
                      </span>
                      <select name="jenis_kelamin" class="form-control">
                        <option selected disabled>Filter Tempat</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body p-0 text-left overflow-auto">
                <table class="table table-sm mb-0 table-hover">
                  <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0 text-center" style="min-width: 35px">ID</th>
                    <th scope="col" class="border-0">Nama</th>
                    <th scope="col" class="border-0">Supplier</th>
                    <th scope="col" class="border-0">Jenis</th>
                    <th scope="col" class="border-0">Satuan</th>
                    <th scope="col" class="border-0">Tempat</th>
                    <th scope="col" class="border-0 text-right">Harga</th>
                    <th scope="col" class="text-right border-0" style="min-width: 70px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $d)
                    <tr>
                      <td class="border-right text-center">{{ $d['id'] }}</td>
                      <td class="border-right">{{ $d['nama'] }}</td>
                      <td class="border-right">{{ $d['supplier'] }}</td>
                      <td class="border-right">{{ $d['jenis'] }}</td>
                      <td class="border-right">{{ $d['satuan'] }}</td>
                      <td class="border-right">{{ $d['tempat'] }}</td>
                      <td class="border-right text-right">{{ $d['harga'] }}</td>
                      <td class="text-right">
                        <form action="{{ url('product/admin/'.$d['id']) }}" method="POST">
                          @method('DELETE')
                          @csrf
                          <button type="submit" class="btn btn-sm  btn-outline-danger float-right" style="font-size: 16px; padding: 0 4px">
                            <i class="material-icons">delete</i>
                          </button>
                        </form>
                        <a href="{{ url('product/admin/edit/'.$d['id']) }}" class="float-right mr-1">
                          <button type="button" class="btn btn-sm  btn-outline-primary float-right" style="font-size: 16px;  padding: 0 4px">
                            <i class="material-icons">edit</i>
                          </button>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style>
    .overflow-x-auto > .row {
      overflow-x: auto;
      white-space: nowrap;
    }
  </style>
@endsection