@extends('layouts.shards')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle"></span>
            <h3 class="page-title float-left">
              <a href="{{ url('product/admin') }}"><i class="material-icons">arrow_back</i></a>
              Edit produk</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <ul class="list-group list-group-flush">
                <li class="list-group-item px-3">
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ ucfirst($error) }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @elseif ($message = Session::get('success'))
                    <div class="alert alert-success"> {{ $message }} </div>
                  @endif
                  <form method="post" action="{{ url('product/admin') }}" enctype="multipart/form-data">
                    @csrf @method('PUT')
                    <input name="id" type="hidden" value="{{ $id }}">
                    @if(count($foto))
                      <div class="form-group row text-left">
                        <div class="col"></div>
                        <div class="col-sm-9">
                          @for($i=1; $i<=5; $i++)
                            <div class="container-photo">

                              <div class="btn-delete-photo text-primary"
                                   id="btn-delete-photo{{ $i }}"
                                   style="{{ !isset($foto[$i-1]) ? 'display: none' : '' }}"
                                   onclick="deletePhoto('{{ isset($foto[$i-1]) ? $foto[$i-1]['id'] : '' }}', '{{ $i }}')">
                                <i class="fas fa-times-circle"></i>
                              </div>
                              <img {{ isset($foto[$i-1]) ? "src=".asset('img_produk/'.$foto[$i-1]['foto']) : "" }}
                                   alt="foto_produk{{ $i }}"
                                   style="max-width: 300px; max-height: 250px; object-fit: cover; {{ isset($foto[$i-1]) ? "" : "display: none" }}"
                                   id="foto{{ $i }}">
                            </div>
                          @endfor
                        </div>
                      </div>
                    @endif

                    <input type="hidden" name="deleted_photo" id="deleted-photo" value="[]">

                    <div class="form-group row text-left">
                      <div class="col-sm-3"></div>
                      <div class="col-sm-9">
                        <div class="btn-insert-photo text-primary border-primary" onclick="insertPhoto()" id="btn-insert-photo">
                          <div style="width: 100%; height: 50px; text-align: center; line-height: 60px; font-size: 30px">
                            <i class="material-icons">insert_photo</i>
                          </div>
                          <div style="width: 100%; font-size: 12px" class="text-primary font-weight-bold text-center">Masukkan<br>Foto</div>
                        </div>
                      </div>
                    </div>
                    {{--<div class="form-group row text-left">--}}
                      {{--<label for="nama_produk" class="col-sm-3 col-form-label text-right">Foto</label>--}}
                      {{--<div class="col-sm-9">--}}
                        <input type="file" name="foto1" id="file-foto1" style="display: none" onchange="onFileSelected(event)">
                        <input type="file" name="foto2" id="file-foto2" style="display: none" onchange="onFileSelected(event)">
                        <input type="file" name="foto3" id="file-foto3" style="display: none" onchange="onFileSelected(event)">
                        <input type="file" name="foto4" id="file-foto4" style="display: none" onchange="onFileSelected(event)">
                        <input type="file" name="foto5" id="file-foto5" style="display: none" onchange="onFileSelected(event)">
                      {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row text-left">
                      <label for="nama_produk" class="col-sm-3 col-form-label text-right">Nama produk</label>
                      <div class="col-sm-9">
                        <input value="{{ old('nama_produk') ?: $produk['nama'] }}" type="text" id="nama_produk"
                               class="form-control" placeholder="Nama produk" name="nama_produk">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="id_supplier" class="col-sm-3 col-form-label text-right">Supplier</label>
                      <div class="col-sm-9" style="padding-left: 20px">
                        @for($i=0; $i<count($itemSupp); $i++)
                          <div class="col-sm-5 float-left">
                            <input class="form-check-input" type="checkbox" id="{{ 'cb'.$itemSupp[$i]['id'] }}"
                                   name="supplier[]" value="{{ $itemSupp[$i]['id'] }}"
                              {{  old('supplier')
                                  ? (in_array($itemSupp[$i]['id'], old('supplier')) ? 'checked' : '')
                                  : ($itemSupp[$i]['check'] ? 'checked' : '') }}>
                            <label class="form-check-label" for="{{ 'cb'.$itemSupp[$i]['id'] }}">
                              {{ $itemSupp[$i]['nama'] }}
                            </label>
                            <br>
                          </div>
                        @endfor
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="jenis" class="col-sm-3 col-form-label text-right">Jenis produk</label>
                      <div class="col-sm-9">
                        <select class="form-control" id="jenis" name="jenis" required>
                          <option disabled {{ $produk['jenis'] == null ? 'selected' : '' }}>Jenis produk</option>
                          @foreach($itemJenis as $i)
                            <option {{ old('jenis')
                                  ? (old('jenis') == $i['id'] ? 'selected' : '')
                                  : ($i['check'] ? 'selected' : '') }}
                                    value="{{ $i['id'] }}">{{ $i['keterangan'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="keterangan" class="col-sm-3 col-form-label text-right">Keterangan</label>
                      <div class="col-sm-9">
                        <textarea name="keterangan" id="keterangan" class="form-control" cols="30"
                                  rows="5">{{ old('keterangan') ?: $produk['keterangan'] }}</textarea>
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="stock" class="col-sm-3 col-form-label text-right">Stock</label>
                      <div class="col-sm-9">
                        <input type="number" id="stock" class="form-control" placeholder="Stock" name="stock"
                               value="{{ old('stock') ?: $produk['stock'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga</label>
                      <div class="col-sm-6">
                        <input type="number" id="harga" class="form-control" placeholder="Harga" name="harga"
                               value="{{ old('harga') ?: $produk['harga'] }}">
                      </div>
                      <div class="col-3 pt-2">
                        <input class="form-check-input" id="harga-grosir-check" type="checkbox" name="harga_grosir_check" onchange="hargaGrosirCheck()" {{ count($itemGrosir) ? 'checked' : '' }}>
                        <label class="form-check-label" for="harga-grosir-check">
                          Harga grosir
                        </label>
                      </div>
                    </div>
                    <div class="form-group row text-left" style="display: {{ (count($itemGrosir) ? 'flex' : 'none') }}" id="grosir-1">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga Grosir 1</label>
                      <div class="col-sm-5">
                        <input type="number" id="qty-gosir-1" class="form-control" placeholder="Masukkan jumlah minimal barang" name="qty_grosir_1" value="{{ old('qty_grosir_1') ?: (isset($itemGrosir[0]) ? $itemGrosir[0]['qty'] : null) }}">
                      </div>
                      <div class="col-4">
                        <input type="number" id="harga-gosir-1" class="form-control" placeholder="Masukkan harga per satuan" name="harga_grosir_1" value="{{ old('harga_grosir_1') ?: (isset($itemGrosir[0]) ? $itemGrosir[0]['harga'] : null) }}">
                      </div>
                    </div>
                    <div class="form-group row text-left" style="display: {{ (count($itemGrosir) ? 'flex' : 'none') }}" id="grosir-2">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga Grosir 2</label>
                      <div class="col-sm-5">
                        <input type="number" id="qty-gosir-2" class="form-control" placeholder="Masukkan jumlah minimal barang" name="qty_grosir_2" value="{{ old('qty_grosir_2') ?: (isset($itemGrosir[1]) ? $itemGrosir[1]['qty'] : null) }}">
                      </div>
                      <div class="col-4">
                        <input type="number" id="harga-gosir-2" class="form-control" placeholder="Masukkan harga per satuan" name="harga_grosir_2" value="{{ old('harga_grosir_2') ?: (isset($itemGrosir[1]) ? $itemGrosir[1]['harga'] : null) }}">
                      </div>
                    </div>
                    <div class="form-group row text-left" style="display: {{ (count($itemGrosir) ? 'flex' : 'none') }}" id="grosir-3">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga Grosir 3</label>
                      <div class="col-sm-5">
                        <input type="number" id="qty-gosir-3" class="form-control" placeholder="Masukkan jumlah minimal barang" name="qty_grosir_3" value="{{ old('qty_grosir_3') ?: (isset($itemGrosir[2]) ? $itemGrosir[2]['qty'] : null) }}">
                      </div>
                      <div class="col-4">
                        <input type="number" id="harga-gosir-3" class="form-control" placeholder="Masukkan harga per satuan" name="harga_grosir_3" value="{{ old('harga_grosir_3') ?: (isset($itemGrosir[2]) ? $itemGrosir[2]['harga'] : null) }}">
                      </div>
                    </div>
                    <div class="form-group row text-left" style="display: {{ (count($itemGrosir) ? 'flex' : 'none') }}" id="grosir-4">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga Grosir 4</label>
                      <div class="col-sm-5">
                        <input type="number" id="qty-gosir-4" class="form-control" placeholder="Masukkan jumlah minimal barang" name="qty_grosir_4" value="{{ old('qty_grosir_4') ?: (isset($itemGrosir[3]) ? $itemGrosir[3]['qty'] : null) }}">
                      </div>
                      <div class="col-4">
                        <input type="number" id="harga-gosir-4" class="form-control" placeholder="Masukkan harga per satuan" name="harga_grosir_4" value="{{ old('harga_grosir_4') ?: (isset($itemGrosir[3]) ? $itemGrosir[3]['harga'] : null) }}">
                      </div>
                    </div>
                    <div class="form-group row text-left" style="display: {{ (count($itemGrosir) ? 'flex' : 'none') }}" id="grosir-5">
                      <label for="harga" class="col-sm-3 col-form-label text-right">Harga Grosir 5</label>
                      <div class="col-sm-5">
                        <input type="number" id="qty-gosir-5" class="form-control" placeholder="Masukkan jumlah minimal barang" name="qty_grosir_5" value="{{ old('qty_grosir_5') ?: (isset($itemGrosir[4]) ? $itemGrosir[4]['qty'] : null) }}">
                      </div>
                      <div class="col-4">
                        <input type="number" id="harga-gosir-5" class="form-control" placeholder="Masukkan harga per satuan" name="harga_grosir_5" value="{{ old('harga_grosir_5') ?: (isset($itemGrosir[4]) ? $itemGrosir[4]['harga'] : null) }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="satuan" class="col-sm-3 col-form-label text-right">Satuan</label>
                      <div class="col-sm-9">
                        <input type="text" id="satuan" class="form-control" placeholder="Satuan" name="satuan"
                               value="{{ old('satuan') ?: $produk['satuan'] }}">
                      </div>
                    </div>
                    <div class="form-group row text-left">
                      <label for="satuan" class="col-sm-3 col-form-label text-right">Tempat</label>
                      <div class="col-sm-9">
                        <input type="text" id="tempat" class="form-control" placeholder="Tempat" name="tempat"
                               value="{{ old('tempat') ?: $produk['tempat'] }}">
                      </div>
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="simpan">
                    <button type="button" class="btn btn-secondary float-right mr-2" onclick="location.href='{{ url('product/admin') }}'">Kembali</button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('js/add_foto_produk.js') }}"></script>
  <script>
      function hargaGrosirCheck() {
          let value = document.getElementById('harga-grosir-check').checked;
          for(let i=1; i<=5; i++){
              document.getElementById('grosir-'+i).style.display = value ? 'flex' : 'none';
          }
      }
  </script>

  <style>
    .btn-delete-photo{
      position: absolute;
      width: 25px;
      height: 25px;
      background: white;
      border-radius: 50%;
      margin-top: 10px;
      margin-left: 10px;
      text-align: center;
      line-height: 25px;
      font-size: 20px;
      cursor: pointer;
    }
    .btn-insert-photo{
      width: 100px;
      height: 100px;
      border-width: 1px;
      border-style: solid;
      border-radius: 5px;
    }
    .btn-insert-photo:hover{
      cursor: pointer;
    }
    .container-photo{
      width: max-content;
      height: 250px;
      margin: 5px;
      float: left;
    }
  </style>
@endsection