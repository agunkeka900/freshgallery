@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container" style="max-width: 1140px">
      <div class="row">
        <div class="col text-center text-secondary">
          <p>Kategori Pilihan</p>
        </div>
      </div>
      @foreach($data as $d)
        <div class="row px-2">
          <h5>{{ $d['jenis'] }}</h5>
        </div>
        <div class="row mb-5 px-1">
          @foreach($d['data'] as $k)
            @include('components.item_produk')
          @endforeach
        </div>
      @endforeach
    </div>
  </section>
@endsection