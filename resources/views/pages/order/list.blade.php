@extends('layouts.main')

@section('content')
  <section class="page-section">
    <div class="container-fluid" style="max-width: 1140px;">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header transaksi-header">Daftar Transaksi</div>
            <div class="card-body py-0 mb-4 border-bottom filter-container">
              <div class="row py-3">
                <div class="col-4 py-0 px-0 border-right">
                  <div class="form-group text-left mb-0">
                    <label for="order-list-filter-status" class="col-12 pt-0 col-form-label text-left">Filter Status</label>
                    <div class="col-12 py-0">
                      <select class="form-control" id="order-list-filter-status" onchange="debounceFilterOrder('{{url('')}}')">
                        <option value="all" selected>Semua</option>
                        @foreach($statuses as $s)
                          <option value="{{ $s['status'] }}" {{ $status == $s['status'] ? 'selected' : '' }}>{{ $s['keterangan2'] }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-4 py-0 border-right">
                  <div class="form-group text-left row mb-0">
                    <label for="order-list-filter-start-date" class="col-6 pt-0 pr-1 col-form-label text-left">Filter Tanggal Awal</label>
                    <label for="order-list-filter-end-date" class="col-6 pt-0 pl-1 col-form-label text-left">Filter Tanggal Akhir</label>
                    <div class="col-6 pr-1 py-0">
                      <input type="date" class="form-control" id="order-list-filter-start-date" value="{{ $initial_date }}" oninput="debounceFilterOrder('{{url('')}}')">
                    </div>
                    <div class="col-6 pl-1 py-0">
                      <input type="date" class="form-control" id="order-list-filter-end-date" value="{{ date('Y-m-d') }}" oninput="debounceFilterOrder('{{url('')}}')">
                    </div>
                  </div>
                </div>
                <div class="col-4 py-0 px-0">
                  <div class="form-group text-left mb-0">
                    <label for="order-list-filter-search" class="col-12 pt-0 col-form-label text-left">Cari</label>
                    <div class="col-12 py-0">
                      <input type="text" class="form-control" id="order-list-filter-search" placeholder="Cari Nama Barang & No Pesanan" oninput="debounceFilterOrder('{{url('')}}')">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body pt-0" id="order-list-filter-loading" style="display: none;">
              <div class="spinner-border spinner-border-sm mx-auto" role="status"
                   style="color: gray; border-width: 0.15em; display: block;  margin-top: 10px"></div>
            </div>
            <div class="card-body pt-0" id="order-list-item-container">
              @include('components.order.item_order_list')
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="float-container">
    <button class="btn btn-white btn-filter" data-toggle="modal" data-target="#order-filter-modal">
      <i class="fa fa-filter my-float"></i> &nbsp; Filter
    </button>

  </div>

  @include('components.order.order_detail_modal')
  @include('components.order.order_filter')

  <style>
    .filter-container{
      display: block;
    }
    .float-container{
      position:fixed;
      width:100%;
      padding-top: 10px;
      height:50px;
      bottom:20px;
      /*background-color:#0C9;*/
      text-align:center;
      display: none;
    }
    .btn-filter{
      background: white;
      border-radius: 50px;
      box-shadow: 0 0 10px #999;
      padding: 10px 20px;
    }
    @media (max-width: 1000px){
      .float-container{
        display: block;
      }
      .filter-container{
        display: none
      }
      .transaksi-header{
        background-color: rgba(0,0,0,0);
        border-bottom: none;
        font-weight: bold;
      }
      .card{
        border: none;
      }
      .container-fluid{
        padding: 0;
      }
      .page-section{
        padding: 60px 0;
      }
    }
  </style>

  <script src="{{ asset('js/transaction_order_list.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('css/transaction_order_list.css') }}">
@endsection