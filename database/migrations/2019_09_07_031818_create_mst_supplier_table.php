<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('nama', 255);
            $table->string('alamat', 255)->nullable;
            $table->tinyInteger('status')->default(1);
            $table->text('deskripsi')->nullable();
            $table->string('telp', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_supplier');
    }
}
