<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable(false);
            $table->string('nama', 255)->nullable(false);
            $table->tinyInteger('gender');
            $table->string('alamat', 255);
            $table->date('tgl_lahir');
            $table->string('telp', 15);
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_pelanggan');
    }
}
