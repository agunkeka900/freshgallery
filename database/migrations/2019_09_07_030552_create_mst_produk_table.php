<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->integer('id_supplier');
            $table->tinyInteger('jenis');
            $table->text('keterangan')->nullable();
            $table->integer('stock')->default(0);
            $table->integer('harga')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_produk');
    }
}
