<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTerjualAndMinimalBeliInMstProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_produk', function (Blueprint $table) {
          $table->integer('terjual')->after('view')->default('0');
          $table->string('min_beli')->after('terjual')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_produk', function (Blueprint $table) {
            //
        });
    }
}
