<?php

use App\Transaksi;
use App\TransaksiDetail;
use Illuminate\Database\Seeder;
use App\Http\Controllers\SeqController;
use App\MstProduk;
use App\MstPelanggan;
use App\MstAddress;
use App\RefWaktuKirim;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $limit_trn = 50;
      $limit_detail = 3;
      $id_supplier = '3';

      $faker = Faker\Factory::create();
      $pelanggans = MstPelanggan::pluck('id');
      $waktu_kirims = RefWaktuKirim::pluck('id');
      $addresses = MstAddress::pluck('id');
      $products = MstProduk::leftJoin('rel_produk_supplier','rel_produk_supplier.id_produk','=','mst_produk.id')
        ->leftJoin('mst_supplier','mst_supplier.id','=','rel_produk_supplier.id_supplier')
        ->where('mst_supplier.id',$id_supplier)
        ->select('mst_produk.id','mst_produk.harga','mst_supplier.id as id_supplier')
        ->inRandomOrder()
        ->limit($limit_detail)
        ->get();

      for ($i=0; $i<$limit_trn; $i++) {
        $id_pesanan = SeqController::getCheckoutIdPesanan();
        $total_harga = 0;
        $created_at = '2020-'.$faker->date('m-d').' '.date('H:i:s');
        for($a=0; $a<count($products); $a++){
          $products[$a]['jumlah'] = $faker->randomNumber('1');
          $total_harga += $products[$a]['harga'] * $products[$a]['jumlah'];
        }

        $new_trn = new Transaksi();
        $new_trn->no_pesanan = $id_pesanan;
        $new_trn->id_pelanggan  = $faker->randomElement($pelanggans);
        $new_trn->total_harga   = $total_harga;
        $new_trn->id_alamat_kirim  = $faker->randomElement($addresses);
        $new_trn->id_waktu_kirim   = $faker->randomElement($waktu_kirims);
        $new_trn->metode_bayar  = 'COD';
        $new_trn->catatan       = 'Packing yang baik';
        $new_trn->status        = '4';
        $new_trn->created_at    = $created_at;
        $new_trn->updated_at    = $created_at;
        $new_trn->save();

        foreach ($products as $product){
          $new_trnd = new TransaksiDetail();
          $new_trnd->id_transaksi = $new_trn->id;
          $new_trnd->id_barang    = $product['id'];
          $new_trnd->id_supplier  = $product['id_supplier'];
          $new_trnd->harga        = $product['harga'];
          $new_trnd->qty          = $product['jumlah'];
          $new_trnd->total_harga  = $product['harga'] * $product['jumlah'];
          $new_trnd->status       = '4';
          $new_trnd->created_at   = $created_at;
          $new_trnd->updated_at   = $created_at;
          $new_trnd->save();
        }
      }
    }
}
