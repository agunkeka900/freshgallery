<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProdukController@index')->name('/');

Auth::routes();

Route::get('auth/redirect/{driver}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('auth/{driver}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.callback');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', function (){
  return view('index');
})->name('about');
Route::get('/index', 'ProdukController@index');

//LOGIN & REGISTER
Route::get('viewLogin/{param?}', function (){return view('login');})->name('viewLogin');
Route::get('viewRegister', function (){return view('register');})->name('viewRegister');

//USER
Route::get('/user/getProfile', 'UserController@getProfile')->name('user/getProfile');
Route::get('/user/getEditProfile', 'UserController@getEditProfile')->name('user/getEditProfile');
Route::post('/user/updateProfile', 'UserController@updateProfile')->name('user/updateProfile');

//PRODUK
Route::prefix('product')->group(function (){
  Route::get('/', 'ProdukController@index');
  Route::get('detail/{slug}', 'ProdukController@detail');
  Route::get('category', 'ProdukController@categoryList');
  Route::get('category/{slug}', 'ProdukController@categorySelected');
  Route::get('search/{key}', 'ProdukController@search');
  Route::get('search-recommended/{key}', 'ProdukController@searchRecommended');
});
//Route::get('produk/viewDetailAdmin/{id}', 'ProdukController@viewDetailAdmin')->name('produk/viewDetailAdmin');

//HEALTHY INFO
Route::get('healthy_info', 'ArtikelController@healthyInfo')->name('healthy_info');
Route::get('healthy_info/read/{id}', 'ArtikelController@healthyInfoRead')->name('healthy_info/read');
Route::get('healthy_info/search/{keywords}', 'ArtikelController@searchHealthyInfo');

//FRESH EDU
Route::get('fresh_edu', 'ArtikelController@freshEdu');
Route::get('fresh_edu/search/{keywords}', 'ArtikelController@searchFreshEdu');
Route::get('fresh_edu/read/{id}', 'ArtikelController@freshEduRead');

//ORDER
Route::get('order/direct/continue/{id}/{direct}', 'OrderController@continueOrder');
Route::get('order/direct/cancel/{id}/{direct}', 'OrderController@cancelOrder');

//CHECKOUT
Route::prefix('checkout')->group(function (){
  Route::get('/', 'CheckoutController@index');
  Route::post('/', 'CheckoutController@store');
  Route::get('guest', 'CheckoutController@guest');
  Route::get('dataDiri', 'CheckoutController@dataDiri')->name('checkout/dataDiri');
  Route::post('saveDataDiri', 'CheckoutController@saveDataDiri')->name('checkout/saveDataDiri');
  Route::get('invoice/{id}', 'CheckoutController@invoice')->name('checkout/invoice');

  Route::post('address', 'CheckoutAddressCtr@store');
  Route::get('address', 'CheckoutAddressCtr@getAll');
  Route::get('address/primary/{id}', 'CheckoutAddressCtr@updatePrimary');
  Route::post('address/update', 'CheckoutAddressCtr@update');
  Route::get('address/delete/{id}', 'CheckoutAddressCtr@delete');
});

//PEOPLE
Route::prefix('people')->group(function (){
  Route::get('view/forgot-password', 'PeopleController@viewForgotPassword');
  Route::get('view/change-password/{unique_string}', 'PeopleController@viewChangePassword');
  Route::post('mail/forgot-password', 'PeopleController@mailForgotPassword');
  Route::post('change-password', 'PeopleController@changePassword');
});

Route::middleware(['auth'])->group(function () {
  Route::middleware(['admin'])->group(function (){
    Route::get('admin/dashboard', 'AdminController@showDashboard');
    Route::get('admin/dashboard/transaksi/{tahun}/{berdasarkan}', 'AdminController@getChartTransaksi');
    Route::get('transaction/admin', 'AdminController@getAllTransaction');

    //PRODUK ADMIN
    Route::prefix('product')->group(function (){
      Route::get('admin', 'ProdukAdmCtr@all');
      Route::get('admin/add', 'ProdukAdmCtr@viewAdd');
      Route::get('admin/edit/{id}', 'ProdukAdmCtr@viewEdit');
      Route::put('admin', 'ProdukAdmCtr@update');
      Route::post('admin', 'ProdukAdmCtr@store');
      Route::delete('admin/{id}', 'ProdukAdmCtr@delete');
    });

    //SUPPLIER ADMIN
    Route::prefix('supplier')->group(function (){
      Route::get('admin', 'SupplierAdmCtr@all');
      Route::get('admin/edit/{id}', 'SupplierAdmCtr@viewEdit');
      Route::view('admin/add', 'pages.supplier.admin.add');
      Route::put('admin', 'SupplierAdmCtr@update');
      Route::post('admin', 'SupplierAdmCtr@store');
      Route::delete('admin/{id}', 'SupplierAdmCtr@delete');
    });

    //JENIS PRODUK ADMIN
    Route::prefix('product-type')->group(function (){
      Route::get('admin', 'JenisProdukController@all');
      Route::get('admin/edit/{id}', 'JenisProdukController@viewEdit');
      Route::view('admin/add', 'pages.product_type.admin.add');
      Route::put('admin', 'JenisProdukController@update');
      Route::post('admin', 'JenisProdukController@store');
      Route::delete('admin/{id}', 'JenisProdukController@delete');
    });

    //ARTIKEL ADMIN
    Route::prefix('article')->group(function (){
      Route::get('admin', 'ArtikelController@all');
      Route::get('admin/add', 'ArtikelController@viewAdd');
      Route::get('admin/edit/{id}', 'ArtikelController@viewEdit');
      Route::put('admin', 'ArtikelController@update');
      Route::post('admin', 'ArtikelController@store');
      Route::delete('admin/{id}', 'ArtikelController@delete');
    });

    //WAITING LIST
    Route::prefix('waiting-list')->group(function (){
      Route::get('admin', 'WaitingListController@all');
      Route::get('admin/edit/{id}', 'WaitingListController@viewEdit');
      Route::view('admin/add', 'pages.waiting_list.admin.add');
      Route::put('admin', 'WaitingListController@update');
      Route::post('admin', 'WaitingListController@store');
      Route::delete('admin/{id}', 'WaitingListController@delete');

      Route::get('customer/{id_wl}', 'WaitingListController@allCustomer');
      Route::get('customer/add/{id_wl}', 'WaitingListController@viewAddCustomer');
      Route::get('customer/edit/{id}', 'WaitingListController@viewEditCustomer');
      Route::put('customer', 'WaitingListController@updateCustomer');
      Route::post('customer', 'WaitingListController@storeCustomer');
      Route::delete('customer/{id}', 'WaitingListController@deleteCustomer');

      Route::get('product/add/{id_customer}', 'WaitingListController@viewAddProduct');
      Route::get('product/edit/{id}', 'WaitingListController@viewEditProduct');
      Route::put('product', 'WaitingListController@updateProduct');
      Route::post('product', 'WaitingListController@storeProduct');
      Route::delete('product/{id}', 'WaitingListController@deleteProduct');
    });
  });

  //CART
  Route::prefix('cart')->group(function (){
    Route::get('/', 'CartController@getCart');
    Route::post('/', 'CartController@insertCart');
  });

  //SUPPLIER
  Route::prefix('supplier')->group(function(){
    Route::get('dashboard', 'SupplierController@showDashboard');
    Route::get('dashboard/transaksi/{tahun}/{berdasarkan}', 'SupplierController@getChartTransaksi');

    Route::prefix('stock-confirmation')->group(function (){
      Route::get('/', 'SupplierController@showConfirmationList');
      Route::post('filter', 'SupplierController@filterStockConfirmation');
      Route::get('confirm/{trnd_id}', 'SupplierController@confirmStockConfirmation');
      Route::get('reject/{trnd_id}', 'SupplierController@rejectStockConfirmation');
    });
  });

  Route::get('notif', 'NotificationController@getNotif');

  //ORDER
  Route::prefix('order')->group(function(){
    Route::get('list/{param?}', 'OrderController@getOrderList')->name('order.list');
    Route::get('detail/{trn_id}', 'OrderController@detail');
    Route::post('list/filter','OrderController@filterOrder');
    Route::get('completed/{id}', 'OrderController@completeOrder');
    Route::get('continue/{id}', 'OrderController@continueOrder');
    Route::get('cancel/{id}', 'OrderController@cancelOrder');
  });

  //PEOPLE
  Route::prefix('people')->group(function (){
    Route::get('/', 'PeopleController@index');
    Route::post('update-biodata', 'PeopleController@updateBiodata');
    Route::get('mail/change-password/{id}', 'PeopleController@mailChangePassword');
    Route::post('photo-upload', 'PeopleController@photoUpload');
  });
});

//DEVELOPMENT
//Route::get('produk/getProdukTerkait', 'ProdukController@getProdukTerkait');
//Route::get('distance/{lat1}/{lon1}/{lat2}/{lon2}/{unit}', 'CheckoutController@getDistance');
//Route::get('createSlug', 'ProdukController@createSlug');
Route::get('getVillage', 'LocationController@getVillage');
Route::get('viewMessageCheckStock', function (){
  return view('pages.checkout.stock_check_message');
});
Route::get('getCheckoutIdPesanan','SeqController@getCheckoutIdPesanan');
Route::get('email/stock_available/{trn_id}','EmailController@stockAvailable');
Route::get('generateInvoice/{trn_id}', 'InvoiceController@generateInvoice');
//Route::get('generateMonthName', 'HelperController@generateMonthName');
Route::get('viewChangePassword', function (){
  return view('pages.people.change_password_success');
});
Route::get('some-stock-not-available', 'SupplierController@testSomeStockNotAvailable');
Route::get('estimasiPengiriman','OrderController@getEstimasiPengiriman');
