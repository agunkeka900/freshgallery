<?php

namespace Illuminate\Foundation\Auth;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath($request = null)
    {
        if($request == null) return '/home';
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($request);
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }
}
