// 1
function tambahBeli(data) {
    if(!!ELEMENT.jumlah(data.id)){
        let jumlah = ELEMENT.jumlah(data.id).value;
        ELEMENT.jumlah(data.id).value = parseFloat(jumlah) + 1;
    }
    else{
        let jumlah = ELEMENT.jumlah_cart(data.id).value;
        ELEMENT.jumlah_cart(data.id).value = parseFloat(jumlah) + 1;
    }
    setJumlahPerItemCart(data);
}
function kurangBeli(data) {
    if(!!ELEMENT.jumlah(data.id)) {
        let jumlah = ELEMENT.jumlah(data.id).value;
        if (!jumlah) jumlah = 0;
        ELEMENT.jumlah(data.id).value = parseFloat(jumlah) - 1;
    }
    else{
        let jumlah = ELEMENT.jumlah_cart(data.id).value;
        ELEMENT.jumlah_cart(data.id).value = parseFloat(jumlah) - 1;
    }
    setJumlahPerItemCart(data);
}
function hapusBeli(data) {
    if(!!ELEMENT.jumlah(data.id)) ELEMENT.jumlah(data.id).value = 0;
    else ELEMENT.jumlah_cart(data.id).value = 0;
    setJumlahPerItemCart(data);
}

// 2
function setJumlahPerItemCart(data, type) {
    let jumlah;
    if(!type || type === 'item')
        jumlah = !!ELEMENT.jumlah(data.id) ? parseFloat(ELEMENT.jumlah(data.id).value) :
            (!!ELEMENT.jumlah_cart(data.id) ? parseFloat(ELEMENT.jumlah_cart(data.id).value) : 0);
    else
        jumlah = !!ELEMENT.jumlah_cart(data.id) ? parseFloat(ELEMENT.jumlah_cart(data.id).value) : 0;

    if(jumlah < 0){
        if(!!ELEMENT.jumlah(data.id)) ELEMENT.jumlah(data.id).value = 0;
        else ELEMENT.jumlah_cart(data.id).value = 0;
        alert('Jumlah tidak valid!');
    }
    else if(!!ELEMENT.cart_subtotal_harga(data.id)){
        ELEMENT.cart_subtotal_harga(data.id).innerText = rupiah(jumlah * data.harga);
    }

    if((jumlah === 0 || !jumlah) && !!ELEMENT.jumlah(data.id)){
        ELEMENT.jumlah(data.id).value = 0;
        ELEMENT.btn_beli(data.id).style.display = 'inline';
        ELEMENT.row(data.id).style.display = 'none';
    }
    else if(!!ELEMENT.jumlah(data.id)) {
        ELEMENT.btn_beli(data.id).style.display = 'none';
        ELEMENT.row(data.id).style.display = 'flex';
    }

    setCookieCart(data, jumlah);
}

// 3
function setCookieCart(data, jumlah){
    let arr = getArrCookieCart();
    let path = '/';
    data.jumlah = jumlah;
    if(!arr.length){
        arr.push(data);
        document.cookie = "cartFG="+JSON.stringify(arr)+"endCartFG; path="+path;
        setJumlahCart(arr);
    }
    else{
        let exist = false;
        for(let i=0; i<arr.length; i++){
            if(data.id === arr[i].id && jumlah !== 0){
                arr[i] = data;
                exist = true;
            }
            else if(data.id === arr[i].id && jumlah === 0){
                arr.splice(i, 1);
                exist = true;
            }
        }
        if(!exist){
            arr.push(data);
        }
        document.cookie = "cartFG="+JSON.stringify(arr)+"endCartFG; expires="+setExpiredCookie()+"; path="+path;
        setJumlahCart(arr);
    }
    if(AUTH) debounceCart();
}
function getArrCookieCart() {
    const cookie = document.cookie;
    if(cookie.includes('cartFG')) {
        let text = cookie.split('cartFG=')[1].split('endCartFG');
        return JSON.parse(text[0]);
    }
    return [];
}
function setExpiredCookie() {
    let now = new Date();
    let time = now.getTime();
    let expireTime = time + 1000*86400*30;
    now.setTime(expireTime);
    return now.toGMTString();
}

// 4
function setJumlahCart(arr) {
    let jumlah = 0;
    let harga = 0;
    for(let i=0; i<arr.length; i++){
        if(arr[i].jumlah > 0){
            let harga_pcs = arr[i].harga;
            if(arr[i].harga_grosir.length >= 1){
                for(let a=0; a<arr[i].harga_grosir.length; a++){
                    if(arr[i].jumlah >= arr[i].harga_grosir[a].qty){
                        harga_pcs = arr[i].harga_grosir[a].harga;
                    }
                }
            }
            jumlah += arr[i].jumlah;
            harga += (arr[i].jumlah * harga_pcs);
        }
    }

    if(ELEMENT.cart_jumlah) ELEMENT.cart_jumlah.innerText = jumlah.toString();
    if(ELEMENT.cart_harga)  ELEMENT.cart_harga.innerText = rupiah(harga);
    if(ELEMENT.cart_jumlah2) ELEMENT.cart_jumlah2.innerText = jumlah.toString();
    if(ELEMENT.cart_harga2)  ELEMENT.cart_harga2.innerText = rupiah(harga);
    if(ELEMENT.navbar_bottom){
        if(jumlah === 0) ELEMENT.navbar_bottom.style.display = 'none';
        else ELEMENT.navbar_bottom.style.display = 'flex';
    }
    if(CURRENT_URI !== 'checkout') setItemCart(arr);
    else setItemRingkasanBelanja(arr);
}

// 5
function setItemCart(arr) {
    let text = "";
    for(let i=0; i<arr.length; i++){
        let bg_image_url = ENV_IP+ENV_PATH +"img_produk/"+ arr[i].foto;
        let harga_pcs = arr[i].harga;
        if(arr[i].harga_grosir.length >= 1){
            for(let a=0; a<arr[i].harga_grosir.length; a++){
                if(arr[i].jumlah >= arr[i].harga_grosir[a].qty){
                    harga_pcs = arr[i].harga_grosir[a].harga;
                }
            }
        }
        text += "<div class=\"row border-bottom ml-0 mr-2\">" +
            "      <div style=\"display: flex\" class=\"mx-auto\">" +
            "        <div class=\"py-2 px-0\">" +
            "          <a href='"+ ENV_PATH +"product/detail/"+ arr[i].slug +"'>" +
            "            <div class=\"card-img img-cart\" style=\"background-image: url("+ bg_image_url +")\"></div>" +
            "          </a>" +
            "        </div>" +
            "        <div class=\"col\" style=\"max-width: 186px\">" +
            "          <div class=\"row\">" +
            "            <a href='"+ ENV_PATH +"product/detail/"+ arr[i].slug +"'>" +
            "              <div class=\"col-12 mt-1 ml-2 px-0 box-nama\" style='min-height: 24px'>"+ arr[i].nama +"</div>" +
            "            </a>" +
            "          </div>" +
            "          <div class=\"row\">" +
            "            <div class=\"col-12 pl-2 text-secondary\" style=\"font-size: 12px\">"+ rupiah(harga_pcs) +" / "+ arr[i].satuan +"</div>" +
            "          </div>" +
            "          <div class=\"row pl-2 pt-1\">" +
            "            <div class=\"p-0\" style=\"width: 35px\">" +
            "              <button" +
            "                onclick='kurangBeli("+ JSON.stringify(arr[i]) +")'" +
            "                class=\"btn px-2 btn-add-cart\">" +
            "                <i class=\"fas fa-minus text-secondary\"></i>" +
            "              </button>" +
            "            </div>" +
            "            <div class=\"px-1\" style=\"width: 65px\">" +
            "              <table style=\"width: 100%\">" +
            "                <tr>" +
            "                  <td height=\"30px\">" +
            "                    <input type=\"number\" value='"+ arr[i].jumlah +"'" +
            "                           onfocusout='setJumlahPerItemCart("+ JSON.stringify(arr[i]) +",`cart`)'" +
            "                           id='jumlahCart"+ arr[i].id +"'" +
            "                           class='input-jumlah-cart'>" +
            "                  </td>" +
            "                </tr>" +
            "              </table>" +
            "            </div>" +
            "            <div class=\"p-0\" style=\"width: 35px\">" +
            "              <button" +
            "                onclick='tambahBeli("+ JSON.stringify(arr[i]) +")'" +
            "                class=\"btn px-2 btn-add-cart\">" +
            "                <i class=\"fas fa-plus text-secondary\"></i>" +
            "              </button>" +
            "            </div>";

        const jumlah = parseFloat(arr[i].jumlah);
        arr[i].jumlah = 0;

        text += "        <div class=\"p-0 ml-1\" style=\"width: 35px\">" +
            "              <button " +
            "                onclick='hapusBeli("+ JSON.stringify(arr[i]) +")'" +
            "                class=\"btn px-2 btn-add-cart\">" +
            "                <i class=\"far fa-trash-alt\"></i>" +
            "              </button>" +
            "            </div>" +
            "          </div>" +
            "          <div class=\"row pb-1\">" +
            "            <div class=\"col-12 pr-1 text-primary font-harga\" style=\"text-align: right\">"+ rupiah(jumlah * harga_pcs) +"</div>" +
            "          </div>" +
            "        </div>" +
            "      </div>" +
            "    </div>";
    }
    document.getElementById('cartIsi').innerHTML = text;
}
function setItemRingkasanBelanja(arr) {
    let text = "";
    for(let i=0; i<arr.length; i++){
        let bg_image_url = ENV_IP+ENV_PATH +"img_produk/"+ arr[i].foto;
        text += "<div class='col-12 p-3 border-bottom'>" +
            "      <table style='width: 100%;'>" +
            "        <tr>" +
            "          <td style='width: 70px; height: 70px; vertical-align: top;'>" +
            "            <div class='card-img img-cart' style='background-image: url("+ bg_image_url +")'></div>" +
            "          </td>" +
            "          <td class='pl-2'>" +
            "            <div class='box-nama'>"+ arr[i].nama +"</div>" +
            "            <div class='text-secondary' style='font-size: 12px'>"+ rupiah(arr[i].harga) +" / "+ arr[i].satuan +"</div>" +
            "            <div style='padding-top: 5px'>" +
            "              <div class='p-0' style='width: 35px; float: left'>" +
            "                <button" +
            "                  onclick='kurangBeli("+ JSON.stringify(arr[i]) +")'" +
            "                  class='btn px-2 btn-add-cart'>" +
            "                  <i class='fas fa-minus text-secondary'></i>" +
            "                </button>" +
            "              </div>" +
            "              <div style='width: 60px; float: left; padding-top: 2px; margin-left: 5px; margin-right: 5px'>" +
            "                <input" +
            "                  onfocusout='setJumlahPerItemCart("+ JSON.stringify(arr[i]) +", `cart`)'" +
            "                  type='number' value='"+ arr[i].jumlah +"'" +
            "                  id='jumlahCart"+ arr[i].id +"'" +
            "                  class='input-jumlah-cart'>" +
            "              </div>" +
            "              <div style='width: 35px; float: left'>" +
            "                <button" +
            "                  onclick='tambahBeli("+ JSON.stringify(arr[i]) +")'" +
            "                  class='btn px-2 btn-add-cart'>" +
            "                  <i class='fas fa-plus text-secondary'></i>" +
            "                </button>" +
            "              </div>";

        const jumlah = parseFloat(arr[i].jumlah);
        arr[i].jumlah = 0;

        text +=
            "              <div style='min-width: 100px; float: right; text-align: right; line-height: 29px' class='text-primary font-harga' id='cartSubTotalHarga"+arr[i].id+"'>" +
                             rupiah(jumlah * arr[i].harga) +
            "              </div>" +
            "            </div>" +
            "          </td>" +
            "        </tr>" +
            "      </table>" +
            "    </div>";
    }
    ELEMENT.checkout.item_ringkasan_belanja.innerHTML = text;
}

// Others
function rupiah(angka){
    if (!angka) angka = 0;
    let rupiah = '';
    let angkarev = angka.toString().split('').reverse().join('');
    for (let i = 0; i < angkarev.length; i++) {
        if (i % 3 === 0) rupiah += angkarev.substr(i, 3) + '.';
    }
    return 'Rp ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}
function setInitialProduk() {
    const prev_url = document.referrer;

    if(prev_url !== null && prev_url.includes('redirect=checkout')){
        domInitialProduk(getArrCookieCart());
        insertCart();
        setItemRingkasanBelanja(getArrCookieCart())
    }
    else if(!AUTH){
        domInitialProduk(getArrCookieCart());
    }
    else getCart();
}
function domInitialProduk(arr){
    for (let i=0; i<arr.length; i++){
        if(!!ELEMENT.jumlah(arr[i].id)){
            ELEMENT.jumlah(arr[i].id).value = arr[i].jumlah;
            ELEMENT.btn_beli(arr[i].id).style.display = 'none';
            ELEMENT.row(arr[i].id).style.display = 'flex';
        }
    }
    setJumlahCart(arr);
}

let timeoutIdCart = null;
function debounceCart(){
    if(!!ELEMENT.btn_checkout) ELEMENT.btn_checkout.style.display = 'none';
    if(!!ELEMENT.btn_checkout_loading) ELEMENT.btn_checkout_loading.style.display = 'inline-block';
    clearTimeout(timeoutIdCart);
    timeoutIdCart = setTimeout(insertCart, 500);
}

let xhr = new XMLHttpRequest();
function insertCart() {
    xhr.abort();
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            if(!!ELEMENT.btn_checkout) ELEMENT.btn_checkout.style.display = 'inline-block';
            if(!!ELEMENT.btn_checkout_loading) ELEMENT.btn_checkout_loading.style.display = 'none';
        }
    };
    xhr.open("POST", ENV_PATH+"cart", true);
    xhr.setRequestHeader("X-CSRF-Token", TOKEN);
    xhr.send();
}
function getCart() {
    xhr.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let arr = JSON.parse(this.responseText);
            document.cookie = "cartFG="+JSON.stringify(arr)+"endCartFG; expires="+setExpiredCookie()+"; path=/";
            domInitialProduk(arr);
        }
    };
    xhr.open("GET", ENV_PATH+"cart", true);
    xhr.setRequestHeader("X-CSRF-Token", TOKEN);
    xhr.send();
}