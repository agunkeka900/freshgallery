let key = null;
let timeoutID = null;
let token = null;

function debounce(text, tkn){
    key = text;
    token = tkn;
    if(!key){
        document.getElementById('search-result').style.display = 'none';
        document.getElementById('search-clear').style.display = 'none';
    }
    else{
        document.getElementById('hasilPencarianUntuk').innerHTML = 'Lihat Hasil pencarian <b>'+key+'<b>';
        document.getElementById('search-clear').style.display = 'block';
    }
    clearTimeout(timeoutID);
    timeoutID = setTimeout(search, 500);
}

function search() {
    if(key){
        document.getElementById('search-result').style.display = 'block';
        document.getElementById('search-loading').style.display = 'block';
        document.getElementById('search-recommended').innerHTML = '';
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                setSearchRecommended(this.responseText);
            }
        };
        xhr.open("GET", ENV_PATH+"product/search-recommended/"+encodeURI(key), true);
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.send();

        // xhr.open("POST", ENV_PATH+"produk/searchRekomendasi", true);
        // xhr.setRequestHeader("X-CSRF-Token", token);
        // xhr.send("searchValue="+key);
    }
}
function showSearchResult(v){
    if(!v){
        setTimeout(function () {
            document.getElementById('search-result').style.display = 'none';
        }, 200)
    }
    else    document.getElementById('search-result').style.display = 'block';
}
function clearSearch() {
    document.getElementById('search-input').value = null;
    document.getElementById('search-clear').style.display = 'none';
}
function setSearchRecommended(data) {
    let produk = JSON.parse(data);
    let html = '';
    for (let i=0; i<produk.length; i++){
        html += "<div class='col-12 px-0'>\n" +
            "      <a href='"+ ENV_PATH +"product/detail/"+ produk[i].slug +"'>\n" +
            "        <table class='get-search-result'>\n" +
            "          <tr>\n" +
            "            <td width='65px' class='pl-3 py-1'>\n" +
            "              <div class='search-result-img'" +
            "                   style=\"background-image: url('"+ ENV_IP+ENV_PATH +"img_produk/"+ produk[i].foto +"')\"></div>\n" +
            "            </td>\n" +
            "            <td>" + produk[i].nama + "</td>" +
            "            <td width='10px' class='pr-3'>\n" +
            "              <i class='fas fa-chevron-right'></i>\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </a>\n" +
            "    </div>"
    }
    document.getElementById('search-recommended').innerHTML = html;
    document.getElementById('search-loading').style.display = 'none';
}

function setEventInput(){
    let input = document.getElementById("search-input");

    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            redirectToSearchPage();
        }
    });
}

function setCurrentKeySearch(text) {
    document.getElementById('search-input').value = text;
}

function redirectToSearchPage() {
    window.location = ENV_PATH+'product/search/'+encodeURI(key);
}

setEventInput();