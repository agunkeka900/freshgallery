let route_url;

function debounceFilterOrder(r) {
    clearTimeout(timeoutID);
    route_url = r;
    ELEMENT.order.list.filter.loading.style.display = 'block';
    timeoutID = setTimeout(filterOrder, 1000);
}
function filterOrder() {
    $(document).ready(function() {
        let l = ELEMENT.order.list;
        let f = l.filter;
        $.ajax({
            url: route_url+'/order/list/filter',
            type: 'post',
            data: {
                status: f.status.options[f.status.selectedIndex].value,
                start_date: f.start_date.value,
                end_date: f.end_date.value,
                search: f.search.value,
                _token: TOKEN
            },
            success: function(data) {
                l.item_container.innerHTML = data;
                f.loading.style.display = 'none';
            },
            error: function(data) {
                console.log(data)
            },
        });
    });
}
function hideBtn(trn_id, hide = true){
    let l = ELEMENT.order.list;

    if(hide){
        l.btn_detail(trn_id).style.display = 'none';
        if(l.btn_continue(trn_id)) l.btn_continue(trn_id).style.display = 'none';
        if(l.btn_cancel(trn_id)) l.btn_cancel(trn_id).style.display = 'none';
        l.loading_detail(trn_id).style.display = 'block';
    }

    else{
        l.btn_detail(trn_id).style.display = 'block';
        if(l.btn_continue(trn_id)) l.btn_continue(trn_id).style.display = 'block';
        if(l.btn_cancel(trn_id)) l.btn_cancel(trn_id).style.display = 'block';
        l.loading_detail(trn_id).style.display = 'none';
    }
}
function showOrderDetail(trn_id, route){
    $(document).ready(function() {
        let d = ELEMENT.order.detail;
        hideBtn(trn_id);

        $.ajax({
            url: route+'/'+trn_id,
            type: 'get',
            data: {
                _token: TOKEN
            },
            success: function(data) {
                d.modal_content.innerHTML = data;
                hideBtn(trn_id, false);
                $('#order-detail-modal').modal('show');
            },
            error: function(data) {
                hideBtn(trn_id, false);
                console.log(data)
            },
        });
    });
}
function orderCompleted(id, url){
    $(document).ready(function() {
        route_url = url;
        let d = ELEMENT.order.detail;
        $.ajax({
            url: url+'/order/completed'+'/'+id,
            type: 'get',
            data: {
                _token: TOKEN
            },
            success: function(data) {
                d.modal_close.click();
                filterOrder();
            },
            error: function(data) {
                console.log(data)
            },
        });
    });
}
function continueOrder(id, url){
    hideBtn(id);
    $(document).ready(function() {
        route_url = url;
        $.ajax({
            url: url+'/order/continue'+'/'+id,
            type: 'get',
            data: {
                _token: TOKEN
            },
            success: function(data) {
                filterOrder();
            },
            error: function(data) {
                hideBtn(id, false);
                console.log(data)
            },
        });
    });
}
function cancelOrder(id, url){
    hideBtn(id);
    $(document).ready(function() {
        route_url = url;
        $.ajax({
            url: url+'/order/cancel'+'/'+id,
            type: 'get',
            data: {
                _token: TOKEN
            },
            success: function(data) {
                filterOrder();
            },
            error: function(data) {
                hideBtn(id, false);
                console.log(data)
            },
        });
    });
}