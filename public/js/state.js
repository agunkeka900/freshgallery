function get(id){
 return document.getElementById(id);
}

let ELEMENT = {
    cart_jumlah: get('cartJumlah'),
    cart_harga: get('cartHarga'),
    cart_jumlah2: get('cartJumlah2'),
    cart_harga2: get('cartHarga2'),
    navbar_bottom: get('navbarBottom'),
    btn_checkout: get('btn-checkout'),
    btn_checkout_loading: get('btn-checkout-loading'),
    btn_beli: id => get('btnBeli'+id),
    jumlah: id => get('jumlah'+id),
    jumlah_cart: id => get('jumlahCart'+id),
    row: id => get('row'+id),
    cart_subtotal_harga: id => get('cartSubTotalHarga'+id),

    navbar_top: {
        notif: {
            chip_total: get('navbar-top-notif-chip-total'),
            chip_periksa: get('navbar-top-notif-chip-periksa'),
            chip_proses: get('navbar-top-notif-chip-proses'),
            chip_dikirim: get('navbar-top-notif-chip-dikirim'),
            chip_selesai: get('navbar-top-notif-chip-selesai'),
            container: get('navbar-top-notif-container'),
        },
        user: {
            container: get('navbar-top-user-container'),
            photo: get('navbar-top-user-photo'),
        }
    },

    checkout: {
        item_ringkasan_belanja: get('checkout-item-ringkasan-belanja'),
        address: {
            mode: null,
            modal: get('address-modal'),
            modal_title: get('address-modal-title'),
            add: {
                form: get('aa-form'),
                label: get('aa-label'),
                region_name: get('aa-region'),
                region_id: get('aa-region_id'),
                complete_address: get('aa-complete-address'),
                geolocation: get('aa-geolocation'),
                geolocation_text: get('aa-geolocation-text'),
                geolocation_reset_btn: get('aa-geolocation-reset-btn'),
                primary: get('aa-primary'),
                submit_message: get('aa-submit-message'),
            },
            edit: {
                form: get('ae-form'),
                label: get('ae-label'),
                region_name: get('ae-region'),
                region_id: get('ae-region_id'),
                complete_address: get('ae-complete-address'),
                geolocation: get('ae-geolocation'),
                geolocation_text: get('ae-geolocation-text'),
                geolocation_reset_btn: get('ae-geolocation-reset-btn'),
                primary: get('ae-primary'),
                primary_container: get('ae-primary-container'),
                submit_message: get('ae-submit-message'),
                id: get('ae-id'),
            }
        }
    },
    supplier: {
        stock_confirmation: {
            filter: {
                search: get('stock-confirmation-filter-search'),
                pemesan: get('stock-confirmation-filter-pemesan'),
                tanggal: get('stock-confirmation-filter-tanggal'),
                product: get('stock-confirmation-filter-product'),
                reset: get('stock-confirmation-filter-reset'),
            },
            table: get('stock-confirmation-table'),
            btn_confirm: id => get('stock-confirmation-btn-confirm-'+id),
            btn_reject: id => get('stock-confirmation-btn-reject-'+id)
        }
    },
    order:{
        list: {
            filter: {
                status: get('order-list-filter-status'),
                start_date: get('order-list-filter-start-date'),
                end_date: get('order-list-filter-end-date'),
                search: get('order-list-filter-search'),
                loading: get('order-list-filter-loading'),
            },
            item_container: get('order-list-item-container'),
            btn_detail: id => get('order-list-btn-detail-'+id),
            btn_continue: id => get('order-list-btn-continue-'+id),
            btn_cancel: id => get('order-list-btn-cancel-'+id),
            loading_detail: id => get('order-list-loading-detail-'+id)
        },
        detail: {
            modal: get('order-detail-modal'),
            modal_title: get('order-detail-modal-title'),
            modal_close: get('order-detail-modal-close'),
            modal_content: get('order-detail-modal-content'),
        },
        filter: {
            modal: get('order-filter-modal'),
            modal_title: get('order-filter-modal-title'),
            modal_close: get('order-filter-modal-close'),
        }
    },
    people: {
        btn_save_biodata: get('people-btn-save-biodata'),
        btn_save_loading: get('people-btn-save-loading'),
        msg_save_success: get('people-msg-save-success'),
        btn_password: get('people-btn-password'),
        btn_password_loading: get('people-btn-password-loading'),
        msg_password_success: get('people-msg-password-success'),
        nama: get('people-nama'),
        gender: get('people-gender'),
        tgl_lahir: get('people-tgl-lahir'),
        telp: get('people-telp'),
        form_change_password: get('people-form-change-password'),
        password_baru: get('people-password-baru'),
        password_baru2: get('people-password-baru2'),
        form_forgot_password: get('people-form-forgot-password'),
        forgot_email: get('people-forgot-email'),
        foto: get('people-foto'),
        input_file_foto: get('people-input-file-foto'),
        btn_pilih_foto: get('people-btn-pilih-foto'),
        btn_save_foto: get('people-btn-save-foto'),
        progress_foto: get('people-progress-foto'),
        form_foto: get('people-form-foto'),
        status_foto: get('people-status-foto'),
    },
    product: {
        detail: {
            photo: get('product-detail-photo'),
            photo_zoom: get('product-detail-photo-zoom'),
            thumbnail: id => get('product-detail-thumbnail-'+id),
            arr_photo: get('product-detail-arr-photo'),
        }
    }
}