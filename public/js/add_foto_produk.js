let index_foto;

function _(el){
    return document.getElementById(el);
}
function insertPhoto(){
    for(let i=1; i<=5; i++){
        if(!_('foto'+i).src){
            index_foto = i;
            return _('file-foto'+i).click();
        }
    }
}
function disableInsertPhoto(){
    let disable = true;
    for(let i=1; i<=5; i++){
        if(_('foto'+i).src == "") disable = false;
    }
    if(disable) _('btn-insert-photo').style.display = 'none';
    else _('btn-insert-photo').style.display = 'block';
}
function onFileSelected (event) {
    try {
        const fr = new FileReader();
        fr.readAsDataURL(event.target.files[0]);
        fr.onload = (event) => {
            _('foto'+index_foto).src = event.target.result;
            _('foto'+index_foto).style.display = 'block';
            _('btn-delete-photo'+index_foto).style.display = 'block';
        };
        setTimeout(disableInsertPhoto, 100);
    }
    catch (e) {
        console.log(e);
    }
}
function deletePhoto(id, index){
    _('foto'+index).removeAttribute('src');
    _('foto'+index).style.display = 'none';
    _('btn-delete-photo'+index).style.display = 'none';
    _('file-foto'+index).value = "";
    setTimeout(disableInsertPhoto, 100);

    if(id){
        let arr_id_delete = JSON.parse(_('deleted-photo').value);
        if(!arr_id_delete.includes(id)){
            arr_id_delete.push(id);
            _('deleted-photo').value = JSON.stringify(arr_id_delete);
        }
    }
}