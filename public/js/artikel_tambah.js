const btn = document.querySelector(".sai");
const getText = document.querySelector(".getText");
// const content = document.querySelector(".getcontent");
const editorContent = document.querySelector(".editor");

function fc(){
    editorContent.focus()
}

btn.addEventListener("click", function() {
    const s = editorContent.innerHTML;
    console.log(s);
});

function link() {
    const url = prompt("Masukkan URL");
    if(url !== null) document.execCommand("createLink", false, url);
}

function copy() {
    document.execCommand("copy", false, "");
}

function changeColor() {
    const color = prompt("Masukkan nama warna contoh: red, green, blue, black");
    document.execCommand("foreColor", false, color);
}


function embedInstagram() {
    const embed = prompt("Masukkan sematan");
    var editor = document.getElementById("isi_content");
    if (nodeContainsSelection(editor)) {
        insertHTML(embed);
        return false;
    }
    // insertHTML(embed)
}
function isOrIsAncestorOf(ancestor, descendant) {
    var n = descendant;
    while (n) {
        if (n === ancestor) {
            return true;
        } else {
            n = n.parentNode;
        }
    }
    return false;
}

function nodeContainsSelection(node) {
    var sel, range;
    if (window.getSelection && (sel = window.getSelection()).rangeCount) {
        range = sel.getRangeAt(0);
        return isOrIsAncestorOf(node, range.commonAncestorContainer);
    }
    return false;
}

function insertHTML(embed) {
    var sel, range;
    if (window.getSelection && (sel = window.getSelection()).rangeCount) {
        range = sel.getRangeAt(0);
        range.collapse(true);

        var html = document.createElement("div");
        // html.style.width = '100%';
        html.innerHTML = embed;
        // html.appendChild( document.createTextNode("hi") );
        range.insertNode(html);

        // Move the caret immediately after the inserted span
        range.setStartAfter(html);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }
}

function getImage() {
    var file = document.getElementById('file').files[0];
    // var file = document.querySelector("input[type=file]").files[2];

    var reader = new FileReader();

    let dataURI;

    reader.addEventListener(
        "load",
        function() {
            dataURI = reader.result;

            const img = document.createElement("img");
            img.src = dataURI;
            img.onload = function(){
                if(img.width > 800){
                    img.src = downscaleImage(dataURI, 800)
                }
            };
            editorContent.appendChild(img);
        },
        false
    );

    if (file) {
        console.log("s");
        reader.readAsDataURL(file);
    }
}

function downscaleImage(dataUrl, newWidth, imageType, imageArguments) {
    "use strict";
    var image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl;

    // Provide default values
    imageType = imageType || "image/jpeg";
    imageArguments = imageArguments || 0.7;

    // Create a temporary image so that we can compute the height of the downscaled image.
    image = new Image();
    image.src = dataUrl;
    oldWidth = image.width;
    oldHeight = image.height;
    newHeight = Math.floor(oldHeight / oldWidth * newWidth)

    // Create a temporary canvas to draw the downscaled image on.
    canvas = document.createElement("canvas");
    canvas.width = newWidth;
    canvas.height = newHeight;

    // Draw the downscaled image on the canvas and return the new data URL.
    ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, 0, newWidth, newHeight);
    newDataUrl = canvas.toDataURL(imageType, imageArguments);
    return newDataUrl;
}

function simpan() {
    const isi = editorContent.innerHTML;
    const judul = document.getElementById('judul').value;
    const kategori = document.getElementById('kategori').value;
    const thumbnail = document.getElementById('thumbnail').value;

    if(!judul) return alert('Judul belum diisi!');
    if(!isi) return alert('Isi belum diisi!');
    document.getElementById('isi_artikel').value = isi;
    document.getElementById('kategori_hidden').value = kategori;
    document.getElementById('thumbnail_hidden').value = thumbnail;
    document.getElementById('form').submit();
}
