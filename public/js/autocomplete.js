function autocomplete(inp, arr) {
    var currentFocus;
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        let id = inp.getAttribute('id');
        document.getElementById(id+'_id').value = null;
        let match = 0;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].text.toUpperCase().includes(val.toUpperCase())) {
                match++;
                b = document.createElement("DIV");
                b.innerHTML = strongText(capitalizeEachWord(arr[i].text), val);
                b.innerHTML += "<input type='hidden' value='" + capitalizeEachWord(arr[i].text) + "'>";
                b.innerHTML += "<input type='hidden' value='" + arr[i].id + "'>";
                inp.getAttribute('id')
                b.addEventListener("click", function(e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    inp.setAttribute('alt', '1')
                    document.getElementById(id+'_id').value = this.getElementsByTagName("input")[1].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
        if(!match){
            b = document.createElement("DIV");
            b.innerHTML = "Tidak tersedia!";
            a.appendChild(b);
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) { //down
            currentFocus++;
            addActive(x);
        }
        else if (e.keyCode == 38) { //up
            currentFocus--;
            addActive(x);
        }
        else if (e.keyCode == 13) { //enter
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    function strongText(textArr, val){
        let pos = textArr.toUpperCase().indexOf(val.toUpperCase());
        let strong = "<strong>"+ textArr.substring(pos, pos + val.length) +"</strong>";
        return textArr.substring(0, pos) + strong + textArr.substring(pos + val.length);
    }
    function capitalizeEachWord(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}