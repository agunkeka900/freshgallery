<?php

namespace App\Http\Controllers;

use App\Transaksi;
use App\TransaksiDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
  public function getOrderList($status = 'all')
  {
    return view('pages.order.list')
      ->with('data', self::getDataOrderList($status))
      ->with('initial_date', self::getInitialDateOrderList())
      ->with('status', $status)
      ->with('statuses', StatusController::getStatus('beli','1'));
  }
  
  public static function getEstimasiPengiriman($tgl)
  {
    $time_tgl = strtotime($tgl);
    $day_now = (int)date('w', $time_tgl);
    $seconds_day = 86400;
//    if($day_now != 0){
      $difference = (6 - $day_now) * $seconds_day;
      $saturday = date('d-m-Y', (int)$time_tgl + $difference);
      $sunday = date('d-m-Y', (int)$time_tgl + $difference + $seconds_day);
//    }
//    else{
//      $saturday = date('Y-m-d', (int)$time_tgl - $seconds_day);
//      $sunday = $tgl;
//    }
    return "Sabtu ($saturday) / Minggu ($sunday)";
  }
  
  public static function getDataOrderList($status)
  {
    $id_user    = Auth::user()->id;
    $role_user  = Auth::user()->role_user;

    if(strpos($status, 'status=') !== false){
      $status = explode('status=', $status)[1];
    }

    if($status == 'all'){
      if($role_user == '3')
        $trn = Transaksi::orderBy('id','desc')->get();
      else
        $trn = Transaksi::where('id_pelanggan', $id_user)->orderBy('id','desc')->get();
    }
    else{
      if($role_user == '3')
        $trn = Transaksi::where('status', $status)->orderBy('id','desc')->get();
      else{
        $trn = Transaksi::where('id_pelanggan', $id_user)
          ->where('status', $status)
          ->orderBy('id','desc')
          ->get();
      }
    }
    $data = [];

    for($i=0; $i<count($trn); $i++){
      $data[] = TransaksiController::getTransaksi($trn[$i]['id']);
    }

    return $data;
  }

  public static function getInitialDateOrderList()
  {
    $timestamp_now = strtotime(date('Y-m-d'));
    $thirty_days   = 2592000;
    return date('Y-m-d', $timestamp_now - $thirty_days);
  }

  public function filterOrder(Request $req)
  {
    $status = $req->status;
    $start_date = date('Y-m-d H:i:s', strtotime($req->start_date));
    $end_date = date('Y-m-d H:i:s', strtotime($req->end_date) + 86399);
    $search = $req->search;
    $id_user = Auth::user()->id;
    $role_user = Auth::user()->role_user;
    $data = [];

    $trn = Transaksi::leftJoin('transaksi_detail','transaksi_detail.id_transaksi','=','transaksi.id')
      ->leftJoin('mst_produk','mst_produk.id','=','transaksi_detail.id_barang');

    if($search != null){
      if(is_numeric($search) ||
        strpos($search ,'/') !== false ||
        strpos(strtoupper($search), 'TRN') !== false)
      {
        $trn = $trn->where('transaksi.no_pesanan','like','%'.$search.'%');
      }
      else {
        $trn = $trn->where('mst_produk.nama','like','%'.$search.'%');
      }
    }
    if($status != 'all'){
      $trn = $trn->where('transaksi.status', $status);
    }

    if($role_user != '3'){
      $trn = $trn->where('id_pelanggan', $id_user);
    }
    $trn = $trn->where('transaksi.created_at','>=', $start_date)
      ->where('transaksi.created_at','<=', $end_date)
      ->select('transaksi.id')
      ->groupBy('transaksi.id')
      ->orderBy('transaksi.id','desc')
      ->get();

    for($i=0; $i<count($trn); $i++){
      $data[] = TransaksiController::getTransaksi($trn[$i]['id']);
    }

    return view('components.order.item_order_list')
      ->with('data', $data);
  }

  public function detail($trn_id)
  {
    $data = TransaksiController::getTransaksi($trn_id);
    return view('components/order/order_detail')
      ->with('trn', $data['trn'])
      ->with('trnd', $data['trnd']);
  }

  public function completeOrder($id)
  {
    Transaksi::where('id', $id)->update([
      'status' => '4'
    ]);
    TransaksiDetail::where('id_transaksi', $id)->update([
      'status' => '4'
    ]);
    EmailController::orderCompleted($id);

    return 'Transaksi selesai';
  }

  public function continueOrder($id, $direct = false)
  {
    $trnd = TransaksiDetail::where('id_transaksi', $id)->get();
    $total_harga = 0;

    for($i=0; $i<count($trnd); $i++){
      if($trnd[$i]['status'] == '5'){
        TransaksiDetail::where('id', $trnd[$i]['id'])->delete();
      }
      else{
        $total_harga += $trnd[$i]['total_harga'];
      }
    }

    Transaksi::where('id', $id)->update([
      'status' => '2',
      'total_harga' => $total_harga
    ]);
    
    if($direct) return redirect()->route('order.list');
  }

  public function cancelOrder($id, $direct = false)
  {
    Transaksi::where('id', $id)->update([
      'status' => '0'
    ]);
  
    if($direct) return redirect()->route('order.list');
  }
}
