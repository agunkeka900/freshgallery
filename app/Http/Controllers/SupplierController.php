<?php

namespace App\Http\Controllers;

use App\MstPelanggan;
use App\MstProduk;
use App\MstSupplier;
use App\RelProdukSupplier;
use App\Transaksi;
use App\TransaksiDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{

  public function showDashboard()
  {
//    return $this->getChartTransaksi();
    return view('pages.supplier.dashboard')
      ->with('total_records', $this->getTotalRecords())
      ->with('data_transaksi', $this->getChartTransaksi());
  }

  private function getTotalRecords()
  {
    $id_supplier = User::leftJoin('mst_supplier','users.id','=','mst_supplier.id_user')
      ->where('users.id', Auth::user()->id)
      ->select('mst_supplier.id')
      ->first()['id'];

    $mst_produk = MstProduk::leftJoin('rel_produk_supplier','rel_produk_supplier.id_produk','=','mst_produk.id')
      ->leftJoin('mst_supplier','mst_supplier.id','=','rel_produk_supplier.id_supplier')
      ->where('mst_supplier.id', $id_supplier);

    $total_produk = $mst_produk
      ->select('mst_produk.id')
      ->count('mst_produk.id');

    $jumlah_transaksi = TransaksiDetail::where('id_supplier', $id_supplier)
      ->where('status','4')
      ->select('id_transaksi')
      ->groupBy('id_transaksi')
      ->get()
      ->count();

    $jumlah_nominal_transaksi = TransaksiDetail::where('id_supplier', $id_supplier)
      ->where('status','4')
      ->selectRaw('SUM(total_harga) as total')
      ->first()['total'];

    $produk_terlaris = TransaksiDetail::leftJoin('mst_produk','mst_produk.id','=','transaksi_detail.id_barang')
      ->where('transaksi_detail.id_supplier', $id_supplier)
      ->where('transaksi_detail.status','4')
      ->selectRaw('SUM(transaksi_detail.qty) as qty, mst_produk.nama')
      ->orderBy('transaksi_detail.qty','DESC')
      ->groupBy('mst_produk.nama')
      ->first()['nama'];

//    dd($produk_terlaris);

    return [
      ['name' => 'Total Produk', 'total' => $total_produk],
      ['name' => 'Jumlah Transaksi', 'total' => $jumlah_transaksi],
      ['name' => 'Jumlah Nominal Transaksi', 'total' => $jumlah_nominal_transaksi],
      ['name' => 'Produk Terlaris', 'total' => $produk_terlaris],
    ];
  }

  public function getChartTransaksi($tahun = '2020', $berdasarkan = 'nominal')
  {
    $id_supplier = User::leftJoin('mst_supplier','users.id','=','mst_supplier.id_user')
      ->where('users.id', Auth::user()->id)
      ->select('mst_supplier.id')
      ->first()['id'];

    $data = [];

    if($berdasarkan == 'nominal'){
      for($bln=1; $bln<=12; $bln++){
        $data[] = TransaksiDetail::where('id_supplier', $id_supplier)
          ->where('created_at','like', '2020-'.(strlen($bln) == 1 ? '0'.$bln : $bln).'%')
          ->selectRaw('SUM(total_harga) as total')
          ->first()['total'];
      }
    }
    else{
      for($bln=1; $bln<=12; $bln++){
        $data[] = TransaksiDetail::where('id_supplier', $id_supplier)
          ->where('created_at','like', '2020-'.(strlen($bln) == 1 ? '0'.$bln : $bln).'%')
          ->select('id_transaksi')
          ->groupBy('id_transaksi')
          ->get()
          ->count();
      }
    }


    return json_encode($data);
  }

  public function showConfirmationList()
  {
    $confirmation_lists = $this->getConfirmationList();
    $item_filter = $this->getItemFilterConfirmationList($confirmation_lists);

    return view('pages.supplier.confirmation_list')
      ->with('confirmation_lists', $confirmation_lists)
      ->with('item_filter', $item_filter)
      ->with('id', Auth::user()->id);
  }

  public function filterStockConfirmation(Request $req)
  {
    $confirmation_lists = $this->getConfirmationList();
    $arr_return = [];
    $index = ['search','pemesan','tanggal','product'];

    if(empty($req->search) && empty($req->pemesan) && empty($req->tanggal) && empty($req->product))
      view('components.supplier.stock_confirmation_table')
        ->with('confirmation_lists', $confirmation_lists);

    for($i=0; $i<count($confirmation_lists); $i++){
      $cli = $confirmation_lists[$i];
      $flag = [
        'search' => false,
        'pemesan' => false,
        'tanggal' => false,
        'product' => false,
      ];

      if($this->searchStockConfirmation($req->search, $cli))
        $flag['search'] = true;

      if($cli['nama_pemesan'] == $req->pemesan)
        $flag['pemesan'] = true;

      if($cli['tanggal'] == $req->tanggal)
        $flag['tanggal'] = true;

      if(!empty($req->product)) {
        $arr_product = $this->filterProductStockConfirmation($req->product, $cli['product']);
        if(count($arr_product))
          $flag['product'] = true;
      }

      $flag_push = true;
      for($a=0; $a<count($index); $a++){
        if($req[$index[$a]] != null && !$flag[$index[$a]])
          $flag_push = false;
      }
      if($flag_push){
        $arr_return[] = $cli;
        if($flag['product']){
          $arr_return[count($arr_return)-1]['product'] = $arr_product;
        }
      }
    }

    return view('components.supplier.stock_confirmation_table')
      ->with('confirmation_lists', $arr_return);
  }

  private function filterProductStockConfirmation($key, $product)
  {
    if(empty($key)) return [];
    $arr = [];
    for($i=0; $i<count($product); $i++){
      if($product[$i]['nama'] == $key)
        $arr[] = $product[$i];
    }
    return $arr;
  }

  private function searchStockConfirmation($key, $confirmation_list)
  {
    if(empty($key)) return false;
    $cl = $confirmation_list;
    $key = strtoupper($key);

    if(strpos(strtoupper($cl['no_pesanan']), $key) !== false) return true;
    if(strpos(strtoupper($cl['nama_pemesan']), $key) !== false) return true;
    if(strpos(strtoupper($cl['tanggal']), $key) !== false) return true;
    foreach($cl['product'] as $b){
      if(strpos(strtoupper($b['qty']), $key) !== false) return true;
      if(strpos(strtoupper($b['nama']), $key) !== false) return true;
      if(strpos(strtoupper($b['satuan']), $key) !== false) return true;
    }
    return false;
  }

  private function getConfirmationList()
  {
    $id_supplier = self::getIdSupplierByIdUser();
    $trns = Transaksi::whereIn('status', ['1','6'])->get();
    $arr = [];

    foreach($trns as $trn){
      $trnds = TransaksiDetail::leftJoin('mst_produk','mst_produk.id','=','transaksi_detail.id_barang')
        ->where('transaksi_detail.id_transaksi', $trn['id']);

      if(Auth::user()->role_user != '3')
        $trnds = $trnds->where('transaksi_detail.id_supplier', $id_supplier);

        $trnds = $trnds->where('transaksi_detail.status','1')
        ->select(
          'transaksi_detail.id',
          'transaksi_detail.id_barang',
          'transaksi_detail.qty',
          'mst_produk.nama',
          'mst_produk.satuan')
        ->get();

      $arr[] = [
        'id' => $trn['id'],
        'no_pesanan' => $trn['no_pesanan'],
        'nama_pemesan' => User::where('id', $trn['id_pelanggan'])->first()['name'],
        'tanggal' => explode(' ', $trn['created_at'])[0],
        'product' => $trnds
      ];
    }

    return $arr;
  }

  private function getItemFilterConfirmationList($confirmation_lists)
  {
    $arr = [
      'pemesan' => [],
      'tanggal' => [],
      'product'  => []
    ];

    foreach($confirmation_lists as $lk){
      $pemesan = $lk['nama_pemesan'];
      $tanggal = $lk['tanggal'];
      $product  = $lk['product'];

      if(!count($arr['pemesan']) || !in_array($pemesan, $arr['pemesan']))
        $arr['pemesan'][] = $pemesan;
      if(!count($arr['tanggal']) || !in_array($tanggal, $arr['tanggal']))
        $arr['tanggal'][] = $tanggal;

      foreach($product as $b){
        $nama_product = $b['nama'];
        if(!count($arr['product']) || !in_array($nama_product, $arr['product']))
          $arr['product'][] = $nama_product;
      }
    }
    return $arr;
  }

  public static function getSupplier($id_product)
  {
    return RelProdukSupplier::where('id_produk', $id_product)->first();
  }

  public static function getIdSupplierByIdUser($id_user = null)
  {
    $id  = $id_user == null ? Auth::user()->id : $id_user;
    $get = MstSupplier::where('id_user', $id)->first();
    return $get ? $get['id'] : null;
  }

  public function confirmStockConfirmation($trnd_id)
  {
    $upd_trnd = TransaksiDetail::where('id', $trnd_id)->first();
    $upd_trnd->status = 2;
    $upd_trnd->save();

    $trnds = TransaksiDetail::where('id_transaksi', $upd_trnd['id_transaksi'])
      ->whereIn('status',['1'])
      ->get();

    $trnds2 = TransaksiDetail::where('id_transaksi', $upd_trnd['id_transaksi'])
      ->whereIn('status',['5'])
      ->get();

    if(count($trnds) == 0){
      if(count($trnds2) == 0){
        Transaksi::where('id', $upd_trnd['id_transaksi'])->update([
          'status' => 2
        ]);

        EmailController::stockAvailable($upd_trnd['id_transaksi']);
      }
      else{
        Transaksi::where('id', $upd_trnd['id_transaksi'])->update([
          'status' => 6
        ]);

        EmailController::someStockNotAvailable($upd_trnd['id_transaksi']);
      }
    }
  }

  public function rejectStockConfirmation($trnd_id)
  {
    $upd_trnd = TransaksiDetail::where('id', $trnd_id)->first();
    $upd_trnd->status = 5;
    $upd_trnd->save();

    $trnds = TransaksiDetail::where('id_transaksi', $upd_trnd['id_transaksi'])
      ->whereIn('status',['1'])
      ->get();

    $trnds2 = TransaksiDetail::where('id_transaksi', $upd_trnd['id_transaksi'])
      ->where('status','!=','5')
      ->get();

    if(count($trnds) == 0) {
      if (count($trnds2) == 0) {
        Transaksi::where('id', $upd_trnd['id_transaksi'])->update([
          'status' => 5
        ]);

        EmailController::stockNotAvailable($upd_trnd['id_transaksi']);
      }
      else {
        Transaksi::where('id', $upd_trnd['id_transaksi'])->update([
          'status' => 6
        ]);

        EmailController::someStockNotAvailable($upd_trnd['id_transaksi']);
      }
    }
  }

  public function testSomeStockNotAvailable()
  {
    $id = 262;
    $trn = TransaksiController::getTransaksi($id);

    return view('email.some_stock_not_available')
      ->with($trn);
  }
}
