<?php

namespace App\Http\Controllers;

use App\WaitingList;
use App\WaitingListCustomer;
use App\WaitingListProduct;
use Illuminate\Http\Request;

class WaitingListController extends Controller
{
  public function all()
  {
    $get = WaitingList::all();
    return view('pages.waiting_list.admin.all')
      ->with('data', $get);
  }

  public function viewEdit($id)
  {
    $get = WaitingList::where('id', $id)->first();

    return view('pages.waiting_list.admin.edit')
      ->with('id', $id)
      ->with('data', $get);
  }

  public function store(Request $req)
  {
    $this->validateWaitingList();

    $new = new WaitingList();
    $new->judul = $req->judul;
    $new->tanggal_pesanan = $req->tanggal_pesanan;
    $new->save();

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function update(Request $req)
  {
    $this->validateWaitingList();

    WaitingList::where('id', $req->id)->update([
      'judul' => $req->judul,
      'tanggal_pesanan' => $req->tanggal_pesanan,
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  private function validateWaitingList()
  {
    return request()->validate([
      'judul'=> 'required|max:180',
      'tanggal_pesanan'=> 'required|date',
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }

  public function delete($id)
  {
    WaitingList::where('id', $id)->delete();

    $wl_customer = WaitingListCustomer::where('id_waiting_list', $id)->get();

    foreach($wl_customer as $w){
      WaitingListProduct::where('id_wl_customer', $w['id'])->delete();
    }

    WaitingListCustomer::where('id_waiting_list', $id)->delete();

    return back()->with('success', 'Data berhasil dihapus');
  }

  public function allCustomer($id)
  {
    $get = WaitingListCustomer::where('id_waiting_list', $id)->get();
    $judul = WaitingList::where('id', $id)->first()['judul'];

    for($i=0; $i<count($get); $i++){
      $get[$i]['product'] = WaitingListProduct::where('id_wl_customer', $get[$i]['id'])->get();
    }

    return view('pages.waiting_list.admin.customer_all')
      ->with('data', $get)
      ->with('id_wl', $id)
      ->with('judul', $judul);
  }

  public function viewAddCustomer($id_wl)
  {
    return view('pages.waiting_list.admin.customer_add')
      ->with('id_wl', $id_wl);
  }

  public function viewEditCustomer($id)
  {
    $get = WaitingListCustomer::where('id', $id)->first();

    return view('pages.waiting_list.admin.customer_edit')
      ->with('id', $id)
      ->with('data', $get);
  }

  public function storeCustomer(Request $req)
  {
    $this->validateWaitingListCustomer();

    $new = new WaitingListCustomer();
    $new->id_waiting_list = $req->id_waiting_list;
    $new->nama_pemesan = $req->nama_pemesan;
    $new->lokasi = $req->lokasi;
    $new->keterangan = $req->keterangan;
    $new->save();

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function updateCustomer(Request $req)
  {
    $this->validateWaitingListCustomer();

    WaitingListCustomer::where('id', $req->id)->update([
      'nama_pemesan' => $req->nama_pemesan,
      'lokasi' => $req->lokasi,
      'keterangan' => $req->keterangan,
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  private function validateWaitingListCustomer()
  {
    return request()->validate([
      'nama_pemesan'=> 'required|max:180',
      'lokasi'=> 'required|max:180',
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }

  public function deleteCustomer($id)
  {
    WaitingListCustomer::where('id', $id)->delete();
    WaitingListProduct::where('id_wl_customer', $id)->delete();
    return back()->with('success', 'Data berhasil dihapus');
  }

  public function viewAddProduct($id_customer)
  {
    $id_wl = WaitingListCustomer::where('id', $id_customer)->first()['id_waiting_list'];
    return view('pages.waiting_list.admin.product_add')
      ->with('id_wl', $id_wl)
      ->with('id_customer', $id_customer);
  }

  public function viewEditProduct($id)
  {
    $get = WaitingListProduct::where('id', $id)->first();
    $id_wl = WaitingListCustomer::where('id', $get['id_wl_customer'])->first()['id_waiting_list'];

    return view('pages.waiting_list.admin.product_edit')
      ->with('id', $id)
      ->with('id_wl', $id_wl)
      ->with('data', $get);
  }

  public function storeProduct(Request $req)
  {
    $this->validateWaitingListProduct();

    $new = new WaitingListProduct();
    $new->id_wl_customer = $req->id_wl_customer;
    $new->nama_produk = $req->nama_produk;
    $new->jumlah = $req->jumlah;
    $new->satuan = $req->satuan;
    $new->save();

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function updateProduct(Request $req)
  {
    $this->validateWaitingListProduct();

    WaitingListProduct::where('id', $req->id)->update([
      'nama_produk' => $req->nama_produk,
      'jumlah' => $req->jumlah,
      'satuan' => $req->satuan,
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  private function validateWaitingListProduct()
  {
    return request()->validate([
      'nama_produk'=> 'required|max:180',
      'jumlah'=> 'required|numeric',
      'satuan'=> 'required|max:180',
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }

  public function deleteProduct($id)
  {
    WaitingListProduct::where('id', $id)->delete();
    return back()->with('success', 'Data berhasil dihapus');
  }
}
