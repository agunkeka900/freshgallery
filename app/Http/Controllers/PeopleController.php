<?php

namespace App\Http\Controllers;

use App\MstPelanggan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeopleController extends Controller
{
  public function index()
  {
    $user_ctr   = new UserController();
    $gender_ctr = new GenderController();

    $profile      = $user_ctr->getProfile(false);
    $item_gender  = $gender_ctr->getGender();

    return view('pages.people.index')
      ->with('data', $profile['mst'])
      ->with('nama', $profile['nama'])
      ->with('itemGender', $item_gender);
  }

  public function updateBiodata(Request $req)
  {
    $auth = Auth::user();
    $mst = MstPelanggan::where('id', $auth->id)->first();
    if($mst){
      $user = User::find($auth->id);
      $user->name = $req->nama;
      $user->save();

      $mst->nama = $req->nama;
      $mst->gender = $req->gender;
      $mst->tgl_lahir = $req->tgl_lahir;
      $mst->telp = $req->telp;
      $mst->save();
    }
    else{
      $new = new MstPelanggan();
      $new->id = $auth->id;
      $new->nama = $req->nama;
      $new->gender = $req->gender;
      $new->tgl_lahir = $req->tgl_lahir;
      $new->telp = $req->telp;
      $new->save();
    }
  }

  public function mailChangePassword($id)
  {
//    sleep(1);
    EmailController::changePassword($id);
  }

  public function viewChangePassword($unique_string)
  {
    return view('pages.people.change_password')
      ->with('unique_string', $unique_string);
  }

  public function changePassword(Request $req)
  {
    $id = substr($req->unique_string, 31);

    User::where('id', $id)->update([
      'password'  => bcrypt($req->password_baru)
    ]);

    return view('pages.people.change_password_success');
  }

  public function viewForgotPassword()
  {
    return view('pages.people.forgot_password');
  }

  public function mailForgotPassword(Request $req)
  {
//    if(!filter_var($req->email, FILTER_VALIDATE_EMAIL)){
//      return back()->with('error', 'Email tidak valid!');
//    }
    EmailController::forgotPassword($req->email);
    return view('pages.people.forgot_password_check_email');
  }

  public function photoUpload(Request $req)
  {
    if($req->photo_file) {
      $file = $req->photo_file;
      $ext = $file->getClientOriginalExtension();
      $namaFoto = md5(time()) . '.' . $ext;
      $format = ['png', 'jpeg', 'jpg'];
      if (!in_array($ext, $format)) return [0, 'Foto tidak valid'];
      $file->move(public_path() . '/img_people', $namaFoto);

      User::where('id', Auth::user()->id)->update([
        'foto' => $namaFoto
      ]);
      return [1, 'Foto telah tersimpan'];
    }
    return [0, 'Terjadi kesalahan sistem!'];
  }
}
