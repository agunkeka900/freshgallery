<?php

namespace App\Http\Controllers;

use App\MstPelanggan;
use App\RefWaktuKirim;
use App\Transaksi;
use App\TransaksiDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class CheckoutController extends Controller
{
  private $errMsg = [
    'required'  => ':attribute harus diisi!',
    'max'       => ':attribute maksimal :max karakter!',
    'email'     => 'Email tidak valid!',
    'numeric'   => ':attribute harus berupa angka!'
  ];

  public function dataDiri()
  {
    $gender = new GenderController();
    $itemGender = $gender->getGender();
    return view('co_data_diri')
      ->with('itemGender', $itemGender);
  }

  public function guest()
  {
    if(!Auth::user()){
      return view('pages.checkout.guest');
    }
    else{
      return redirect('checkout');
    }
  }

  public function saveDataDiri(Request $req)
  {
    $this->validate($req, [
      'nama'  => 'required|max:50',
      'email'  => 'required|email|max:100',
      'telp'  => 'required|numeric',
      'gender'  => 'required',
      'tgl_lahir'  => 'required',
      'alamat'  => 'required',
    ], $this->errMsg);

    $cek = MstPelanggan::where('email', $req->email)->first();

    if($cek){
      $cek->nama      = $req->nama;
      $cek->gender    = $req->gender;
      $cek->alamat    = $req->alamat;
      $cek->tgl_lahir = $req->tgl_lahir;
      $cek->telp      = $req->telp;
      $cek->save();

      Cookie::queue('idPelanggan', $cek['id'], 600);

      $upd = User::where('email', $req->email)->first();
      $upd->name      = $req->nama;
      $upd->save();
    }
    else{
      $new2 = new User();
      $new2->name     = $req->nama;
      $new2->email    = $req->email;
      $new2->role_user= 2;
      $new2->save();

      $id = User::orderBy('id', 'DESC')->first()['id'];
      Cookie::queue('idPelanggan', $id, 600);

      $new = new MstPelanggan();
      $new->id        = $id;
      $new->nama      = $req->nama;
      $new->gender    = $req->gender;
      $new->alamat    = $req->alamat;
      $new->tgl_lahir = $req->tgl_lahir;
      $new->telp      = $req->telp;
      $new->email     = $req->email;
      $new->save();
    }

    return redirect('checkout/pembayaran');
  }

  public function store(Request $req)
  {
    $this->validate($req, [
      'metode'  => 'required',
      'alamat'  => 'required',
      'waktu'   => 'required',
    ], $this->errMsg);

    $cart_ctr = new CartController();
    $carts = $cart_ctr->getCart();

    $new_trn = new Transaksi();
    $new_trn->no_pesanan = SeqController::getCheckoutIdPesanan();
    $new_trn->id_pelanggan  = !Auth::user() ? Cookie::get('idPelanggan') : Auth::user()->id;
    $new_trn->total_harga   = $this->countTotalHarga($carts);
    $new_trn->id_alamat_kirim  = $req->alamat;
    $new_trn->id_waktu_kirim   = $req->waktu;
    $new_trn->metode_bayar  = $req->metode == '1' ? 'COD' : null;
    $new_trn->catatan       = $req->catatan;
    $new_trn->estimasi_pengiriman = OrderController::getEstimasiPengiriman(date('Y-m-d'));
    $new_trn->save();

    foreach ($carts as $c){
      if($c->jumlah > 0){
        $new_trnd = new TransaksiDetail();
        $new_trnd->id_transaksi = $new_trn->id;
        $new_trnd->id_barang    = $c->id;
        $new_trnd->id_supplier  = SupplierController::getSupplier($c->id)['id_supplier'];
        $new_trnd->harga        = $c->harga ?: 0;
        $new_trnd->qty          = $c->jumlah ?: 0;
        $new_trnd->total_harga  = $c->harga * $c->jumlah;
        $new_trnd->save();
      }
    }

    CartController::deleteByIdUser(Auth::user()->id);

    return view('pages.checkout.stock_check_message');
  }

  private function countTotalHarga($arr){
    $total = 0;
    foreach($arr as $a){
      $total += ($a->jumlah * $a->harga);
    }
    return $total;
  }

  public function invoice($id)
  {
    $trn = Transaksi::leftJoin('mst_pelanggan', 'mst_pelanggan.id', '=', 'transaksi.id_pelanggan')
      ->select(
        'transaksi.total_harga',
        'transaksi.alamat_kirim',
        'transaksi.waktu_kirim',
        'transaksi.metode_bayar',
        'transaksi.catatan',
        'mst_pelanggan.nama',
        'mst_pelanggan.email',
        'mst_pelanggan.telp'
      )
      ->first();

    $trn['total_harga'] = 'Rp '.number_format($trn['total_harga'], 0, '', '.');

    $trnd = TransaksiDetail::leftJoin('mst_produk', 'mst_produk.id', '=', 'transaksi_detail.id_barang')
      ->where('transaksi_detail.id_transaksi', $id)
      ->select(
        'transaksi_detail.qty',
        'transaksi_detail.harga',
        'transaksi_detail.total_harga',
        'mst_produk.nama'
      )
      ->get();

    for($i=0; $i<count($trnd); $i++){
      $trnd[$i]['harga'] = 'Rp '.number_format($trnd[$i]['harga'], 0, '', '.');
      $trnd[$i]['total_harga'] = 'Rp '.number_format($trnd[$i]['total_harga'], 0, '', '.');
    }

//    return $trnd;

    return view('invoice')
      ->with('trn', $trn)
      ->with('trnd', $trnd)
      ->with('no', 1);
  }

  public function index()
  {
    if(!Auth::user()) return redirect('checkout/guest');
    $cart_ctr = new CartController();
    return view('pages.checkout.index')
      ->with('carts', $cart_ctr->getCart())
      ->with('waktu_kirim', RefWaktuKirim::all())
      ->with('village', LocationController::getVillage(51))
      ->with('address_primary', CheckoutAddressCtr::getAddress(true));
  }

  public function getDistance($lat1, $lon1, $lat2, $lon2, $unit = "M")
  {
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
      return 0;
    }
    else {
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      }
      else if($unit == "M"){
        return ($miles * 1609.344);
      }
      else if ($unit == "N") {
        return ($miles * 0.8684);
      }
      else {
        return $miles;
      }
    }
  }
}

