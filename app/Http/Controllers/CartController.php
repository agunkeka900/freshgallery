<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
  public function insertCart(Request $req)
  {
    $user = Auth::user()->id;
    $cart = $this->getArrCart($req->header('cookie'));
    $get = Cart::where('id_user', $user)->get();

    $old = $this->getArrIdProduk($get);
    $new = $this->getArrIdProduk($cart, true);

    for($i=0; $i<count($new); $i++){
      $jumlah = $cart[$i]['jumlah'] > 0 ? $cart[$i]['jumlah'] : 0;
      if(!in_array($new[$i], $old)){
        $insert = new Cart();
        $insert->id_user   = $user;
        $insert->id_produk = $new[$i];
        $insert->jumlah    = $jumlah;
        $insert->save();
      }
      else{
        Cart::where('id_user', $user)->where('id_produk', $new[$i])->update([
          'jumlah'  => $jumlah
        ]);
      }
    }
    for($a=0; $a<count($old); $a++){
      if(!in_array($old[$a], $new)){
        Cart::where('id_user', $user)->where('id_produk', $old[$a])->delete();
      }
    }
    return 'okay';
  }
  private function getArrCart($cookie)
  {
    $exp = explode('endCartFG', explode('cartFG=', $cookie)[1])[0];
    return json_decode($exp, true);
  }
  private function getArrIdProduk($arr, $id = false)
  {
    $ret = [];
    for($i=0; $i<count($arr); $i++){
      $ret[] = $arr[$i][$id ? 'id' : 'id_produk'];
    }
    return $ret;
  }

  public function getCart()
  {
    $pc = new ProdukController();
    $get = Cart::where('id_user', Auth::user()->id)->get();

    if(count($get)){
      $arrId = $this->getArrIdProduk($get);
      $produk = $pc->getProduk(null, $arrId);

      for($i=0; $i<count($produk); $i++){
        for($a=0; $a<count($get); $a++){
          if($produk[$i]['id'] == $get[$a]['id_produk']){
            $produk[$i]['jumlah'] = $get[$a]['jumlah'];
            break;
          }
        }
      }

      return $produk;
    }
    else return [];
  }

  public static function deleteByIdUser($id_user)
  {
    Cart::where('id_user', $id_user)->delete();
  }
}
