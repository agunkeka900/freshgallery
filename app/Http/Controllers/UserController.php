<?php

namespace App\Http\Controllers;

use App\MstPelanggan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  public function getProfile($returnView = true)
  {
    $auth = Auth::user()->id;
    $user = User::where('id', $auth)->first();
    $mst = MstPelanggan::leftJoin('ref_gender', 'ref_gender.id', '=', 'mst_pelanggan.gender')
      ->select(
        'mst_pelanggan.nama',
        'ref_gender.keterangan as gender',
        'mst_pelanggan.gender as id_gender',
        'mst_pelanggan.alamat',
        'mst_pelanggan.tgl_lahir',
        'mst_pelanggan.telp'
      )
      ->where('mst_pelanggan.id', $auth)
      ->first();

    $data = [
      ['Jenis kelamin', $mst['gender']],
      ['Alamat', $mst['alamat']],
      ['Tanggal lahir', $mst['tgl_lahir']],
      ['Telpon', $mst['telp']],
      ['Email', $user['email']],
    ];

    if($returnView) return view('profile')
      ->with('data', $data)
      ->with('nama', $user['name']);
    else return [
      'mst' => $mst,
      'nama' => $user['name']
    ];
  }

  public function getEditProfile()
  {
    $get = $this->getProfile(false);
    $gender = new GenderController();
    $itemGender = $gender->getGender();
//    return $get;
    return view('profile_edit')
      ->with('data', $get['mst'])
      ->with('nama', $get['nama'])
      ->with('itemGender', $itemGender);
  }

  public function updateProfile(Request $req)
  {

    $auth = Auth::user();
    $mst = MstPelanggan::where('id', $auth->id)->first();
    if($mst){
      $user = User::find($auth->id);
      $user->name = $req->nama;
      $user->save();

      $mst->nama = $req->nama;
      $mst->gender = $req->gender;
      $mst->tgl_lahir = $req->tgl_lahir;
      $mst->tgl_lahir = $req->tgl_lahir;
      $mst->telp = $req->telp;
      $mst->alamat = $req->alamat;
      $mst->save();
    }
    else{
      $new = new MstPelanggan();
      $new->id = $auth->id;
      $new->nama = $req->nama;
      $new->gender = $req->gender;
      $new->tgl_lahir = $req->tgl_lahir;
      $new->telp = $req->telp;
      $new->alamat = $req->alamat;
      $new->save();
    }
    return $this->getProfile();
  }
}
