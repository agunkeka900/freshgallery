<?php

namespace App\Http\Controllers;

use App\MstSupplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierAdmCtr extends Controller
{
  public function all()
  {
    $get = MstSupplier::orderBy('id','DESC')->get();
    return view('pages.supplier.admin.all')
      ->with('data', $get);
  }

  public function store(Request $req)
  {
    $this->validateSupplier();

    $new = new MstSupplier();
    $new->id_user = Auth::user()->id;
    $new->nama = $req->nama_supplier;
    $new->alamat = $req->alamat;
    $new->kota = $req->kota;
    $new->deskripsi = $req->keterangan;
    $new->telp = $req->telp;
    $new->save();

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function viewEdit($id)
  {
    return view('pages.supplier.admin.edit')
      ->with('data', MstSupplier::find($id))
      ->with('city', LocationController::getCity(51));
  }

  public function update(Request $req)
  {
    $this->validateSupplier();
//    dd($req);

    MstSupplier::where('id', $req->id)->update([
      'nama'      => $req->nama_supplier,
      'alamat'    => $req->alamat,
      'deskripsi' => $req->keterangan,
      'telp'      => $req->telp,
      'kota'      => $req->kt,
      'id_kota'   => $req->id_kota
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function delete($id)
  {
    MstSupplier::where('id', $id)->delete();
    return back()->with('success', 'Data berhasil dihapus');
  }

  private function validateSupplier()
  {
    return request()->validate([
      'nama_supplier'=> 'required|max:255',
      'alamat'=> 'required|max:255',
      'kota'=> 'max:255',
      'id_kota' => 'required',
      'telp'=> 'max:15',
    ], [
      'id_kota.required' => 'Kota tidak valid!',
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }
}
