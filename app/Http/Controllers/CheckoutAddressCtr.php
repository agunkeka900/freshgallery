<?php

namespace App\Http\Controllers;

use App\MstAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutAddressCtr extends Controller
{
  public function getAll()
  {
    $get = MstAddress::where('id_user', Auth::user()->id)
      ->leftJoin('ref_village','ref_village.id','=','mst_address.village_id')
      ->leftJoin('ref_district','ref_district.id','=','mst_address.district_id')
      ->leftJoin('ref_city','ref_city.id','=','mst_address.city_id')
      ->leftJoin('ref_province','ref_province.id','mst_address.province_id')
      ->select(
        'ref_village.name as village',
        'ref_district.name as district',
        'ref_city.name as city',
        'ref_province.name as province',
        'mst_address.village_id as id_village',
        'mst_address.label',
        'mst_address.latitude',
        'mst_address.longitude',
        'mst_address.address',
        'mst_address.primary',
        'mst_address.id'
      )
      ->get();

    for($i=0; $i<count($get); $i++){
      $get[$i]['village'] = ucwords(strtolower($get[$i]['village']));
      $get[$i]['district'] = ucwords(strtolower($get[$i]['district']));
      $get[$i]['city'] = ucwords(strtolower($get[$i]['city']));
      $get[$i]['province'] = ucwords(strtolower($get[$i]['province']));
    }

    return $get;
  }
  public function store(Request $req)
  {
    $this->validateAddress();

    $lokasi_id = LocationController::getIdDistrictCityProvince($req->village_id);
    $geo_locations = $req->geolocation ? explode(', ', $req->geolocation) : [null, null];
    $address_first = MstAddress::where('id_user', Auth::user()->id)->count() >= 1 ? false : true;

    if($req->primary){
      MstAddress::where('id_user', Auth::user()->id)
        ->where('primary','1')
        ->update([
          'primary' => 0
        ]);
    }

    $new_address = new MstAddress();
    $new_address->province_id = $lokasi_id['province_id'];
    $new_address->city_id = $lokasi_id['city_id'];
    $new_address->district_id = $lokasi_id['district_id'];
    $new_address->village_id = $req->village_id;
    $new_address->label = $req->label;
    $new_address->address = $req->alamat;
    $new_address->latitude = $geo_locations[0];
    $new_address->longitude = $geo_locations[1];
    $new_address->primary = $address_first ? 1 : ($req->primary ? 1 : 0);
    $new_address->id_user = Auth::user()->id;
    $new_address->save();

    return CheckoutAddressCtr::getAddress(false, $new_address->id);
  }
  public function update(Request $req){
    $this->validateAddress();

    $lokasi_id = LocationController::getIdDistrictCityProvince($req->village_id);
    $geo_locations = $req->geolocation ? explode(', ', $req->geolocation) : [null, null];

    if($req->primary){
      MstAddress::where('id_user', Auth::user()->id)
        ->where('primary','1')
        ->update([
          'primary' => 0
        ]);
    }

    $update_address =  MstAddress::where('id', $req->id)->first();
    $update_address->province_id = $lokasi_id['province_id'];
    $update_address->city_id = $lokasi_id['city_id'];
    $update_address->district_id = $lokasi_id['district_id'];
    $update_address->village_id = $req->village_id;
    $update_address->label = $req->label;
    $update_address->address = $req->alamat;
    $update_address->latitude = $geo_locations[0];
    $update_address->longitude = $geo_locations[1];
    if($req->primary) $update_address->primary = 1;
    $update_address->save();

    return self::getAddress(false, $req->id);
  }
  private function validateAddress()
  {
    return request()->validate([
      'label'=> 'required|max:100',
      'village_id'=> 'required',
      'alamat'=> 'required|max:255',
    ], [
      'label.required' => 'Label Alamat harus diisi!',
      'village_id.required' => 'Kota/Kabupaten, Kecamatan, Desa/Kelurahan harus diisi!',
      'alamat.required' => 'Alamat Lengkap harus diisi!',
      'label.max' => 'Label Alamat maksimal :max karakter!',
      'alamat.max' => 'Alamat Lengkap maksimal :max karakter!',
    ]);
  }

  public function viewAdd()
  {
    return view('components.checkout.address_add');
  }

  public static function getAddress(bool $primary, $id = null)
  {
    $address = MstAddress::where('mst_address.id_user', Auth::user()->id);
      if($primary){
        $address = $address->where('mst_address.primary','1');
      }
      if($id != null){
        $address = $address->where('mst_address.id', $id);
      }
      $address = $address->leftJoin('ref_village','ref_village.id','=','mst_address.village_id')
      ->leftJoin('ref_district','ref_district.id','=','mst_address.district_id')
      ->leftJoin('ref_city','ref_city.id','=','mst_address.city_id')
      ->leftJoin('ref_province','ref_province.id','mst_address.province_id')
      ->select(
        'ref_village.name as village',
        'ref_district.name as district',
        'ref_city.name as city',
        'ref_province.name as province',
        'mst_address.label',
        'mst_address.address',
        'mst_address.primary',
        'mst_address.id'
      )
      ->first();

    if($address){
      $address['village'] = ucwords(strtolower($address['village']));
      $address['district'] = ucwords(strtolower($address['district']));
      $address['city'] = ucwords(strtolower($address['city']));
      $address['province'] = ucwords(strtolower($address['province']));
      $address['complete_region'] = $address['province'] .", ". $address['city'] .", ". $address['district'] .", ". $address['village'];
    }

    return $address ?: null;
  }

  public function updatePrimary($id)
  {
    MstAddress::where('id_user', Auth::user()->id)
      ->where('primary', '1')
      ->update([
        'primary' => 0
      ]);

    MstAddress::where('id', $id)->update([
      'primary' => 1
    ]);

    return $this->getAll();
  }

  public function delete($id)
  {
    $check = MstAddress::where('id', $id)->first();

    if($check->primary == 1){
      $change_primary = MstAddress::where('id_user', Auth::user()->id)->first();
      $change_primary->primary = 1;
      $change_primary->save();
    }

    MstAddress::where('id', $id)->delete();
    return $this->getAll();
  }
}
