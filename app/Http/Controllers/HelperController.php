<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelperController extends Controller
{
  public static function generateMonthName($date, $delimiter = '-')
  {
    $months = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    if(strpos($date, ' ') !== false){
      $arr_time = explode(' ', $date);
      $arr_date = explode($delimiter, $arr_time[0]);
    }
    else{
      $arr_date = explode($delimiter, $date);
    }

    $month_id = $arr_date[1];
    $month_name = $months[(int)$month_id-1];

    return $arr_date[2].' '.$month_name.' '.$arr_date[0];
  }
}
