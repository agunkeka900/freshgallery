<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\MstProduk;
use App\MstSupplier;
use App\RefJenisProduk;
use App\TransaksiDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
  public function showDashboard()
  {
    return view('pages.admin.dashboard')
      ->with('total_records', $this->getTotalRecords())
      ->with('data_transaksi', $this->getChartTransaksi())
      ->with('data_jumlah_produk', $this->getJumlahProduk());
  }

  private function getTotalRecords()
  {
    $total_supplier = MstSupplier::count('id');

    $total_produk = MstProduk::count('id');

    $total_jenis = RefJenisProduk::count();

    $total_healthy_info = Artikel::where('status','1')
      ->where('pdf', null)
      ->count();

    $total_fresh_edu = Artikel::where('status','1')
      ->where('pdf', '!=', null)
      ->count();

    $total_view_hi = Artikel::where('status','1')
      ->where('pdf', null)
      ->selectRaw('SUM(hits) as total')
      ->first()['total'];

    $total_view_fe = Artikel::where('status','1')
      ->where('pdf', '!=', null)
      ->selectRaw('SUM(hits) as total')
      ->first()['total'];

    $jumlah_transaksi = TransaksiDetail::where('status','4')
      ->select('id_transaksi')
      ->groupBy('id_transaksi')
      ->get()
      ->count();

    $jumlah_nominal_transaksi = TransaksiDetail::where('status','4')
      ->selectRaw('SUM(total_harga) as total')
      ->first()['total'];

    $produk_terlaris = TransaksiDetail::leftJoin('mst_produk','mst_produk.id','=','transaksi_detail.id_barang')
      ->where('transaksi_detail.status','4')
      ->selectRaw('SUM(transaksi_detail.qty) as qty, mst_produk.nama')
      ->orderBy('transaksi_detail.qty','DESC')
      ->groupBy('mst_produk.nama','qty')
      ->first()['nama'];

//    dd($produk_terlaris);

    return [
      ['name' => 'Total Produk', 'total' => $total_produk],
      ['name' => 'Total Jenis Produk', 'total' => $total_jenis],
      ['name' => 'Produk Terlaris', 'total' => $produk_terlaris],
      ['name' => 'Total Supplier', 'total' => $total_supplier],
      ['name' => 'Jumlah Transaksi', 'total' => $jumlah_transaksi],
      ['name' => 'Jumlah Nominal Transaksi', 'total' => $jumlah_nominal_transaksi],
      ['name' => 'Total Healthy Info', 'total' => $total_healthy_info],
      ['name' => 'Total Fresh Edu', 'total' => $total_fresh_edu],
      ['name' => 'Total View Healthy Info', 'total' => $total_view_hi],
      ['name' => 'Total View Fresh Edu', 'total' => $total_view_fe],
    ];
  }

  public function getChartTransaksi($tahun = '2020', $berdasarkan = 'nominal')
  {
    $data = [];

    if($berdasarkan == 'nominal'){
      for($bln=1; $bln<=12; $bln++){
        $data[] = TransaksiDetail::where('created_at','like', '2020-'.(strlen($bln) == 1 ? '0'.$bln : $bln).'%')
          ->selectRaw('SUM(total_harga) as total')
          ->first()['total'];
      }
    }
    else{
      for($bln=1; $bln<=12; $bln++){
        $data[] = TransaksiDetail::where('created_at','like', '2020-'.(strlen($bln) == 1 ? '0'.$bln : $bln).'%')
          ->select('id_transaksi')
          ->groupBy('id_transaksi')
          ->get()
          ->count();
      }
    }
    
    return json_encode($data);
  }

  public function getJumlahProduk($berdasarkan = 'jenis', $with_view = true)
  {
//    if($berdasarkan = 'jenis'){
      $ref_jenis = RefJenisProduk::leftJoin('mst_produk','mst_produk.jenis','=','ref_jenis_produk.id')
        ->selectRaw('ref_jenis_produk.id, ref_jenis_produk.keterangan, count(ref_jenis_produk.id) as jumlah')
        ->groupBy('ref_jenis_produk.id', 'ref_jenis_produk.keterangan');

      $data = [ 'id'      => $ref_jenis->pluck('id'),
        'nama'    => $ref_jenis->pluck('keterangan'),
        'jumlah'  => $ref_jenis->pluck('jumlah'),
        'total_seluruhnya'  => MstProduk::count('id')
      ];
//    }
    return $data;
  }

  public function getAllTransaction()
  {
    $status = 'all';
    return view('pages.admin.transaction')
      ->with('data', OrderController::getDataOrderList($status))
      ->with('initial_date', OrderController::getInitialDateOrderList())
      ->with('status', $status)
      ->with('statuses', StatusController::getStatus('beli','1'));
  }
}
