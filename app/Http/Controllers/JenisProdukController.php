<?php

namespace App\Http\Controllers;

use App\RefJenisProduk;
use Illuminate\Http\Request;

class JenisProdukController extends Controller
{
  public function all()
  {
    $get = RefJenisProduk::all();
    return view('pages.product_type.admin.all')
      ->with('data', $get);
  }

  public function viewEdit($id)
  {
    $get = RefJenisProduk::where('id', $id)->first();

    return view('pages.product_type.admin.edit')
      ->with('id', $id)
      ->with('data', $get);
  }

  public function store(Request $req)
  {
    $this->validateJenisProduk();

    $new = new RefJenisProduk();
    $new->keterangan = $req->keterangan;
    $new->save();

    return back()->with('success', 'Data berhasil disimpan');
  }

  private function validateJenisProduk()
  {
    return request()->validate([
      'keterangan'=> 'required|max:255'
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }

  public function update(Request $req)
  {
    $this->validateJenisProduk();

    RefJenisProduk::where('id', $req->id)->update([
      'keterangan' => $req->keterangan
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function delete($id)
  {
    RefJenisProduk::where('id', $id)->delete();
    return back()->with('success', 'Data berhasil dihapus');
  }
}
