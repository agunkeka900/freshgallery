<?php

namespace App\Http\Controllers;

use App\RefStatus;
use Illuminate\Http\Request;

class StatusController extends Controller
{
  public static function getStatus($type, $option = null)
  {
    if($option != null){
      $ref_status = RefStatus::where('opsi', $option)
        ->where('jenis', $type)
        ->get();
    }
    else{
      $ref_status = RefStatus::where('jenis', $type)
        ->get();
    }

    return $ref_status;
  }
}
