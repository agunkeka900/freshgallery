<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  public function redirectTo($request)
  {
    if(strpos($request->redirect, '%20') !== false){
      $request->redirect = str_replace('%20', '/', $request->redirect);
    }

    if(Auth::user()->role_user == '3'){
      return 'admin/dashboard';
    }
    if(Auth::user()->role_user == '4'){
      return 'supplier/dashboard';
    }

    return $request['redirect'] != null ? $request->redirect : '/home';
//    redirect('/produk/viewDataGroup');
  }

//  protected $redirectTo = '/produk/viewDataGroup';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
  public function redirectToProvider($driver)
  {
    return Socialite::driver($driver)->redirect();
  }

  public function handleProviderCallback($driver)
  {
    try {
      $user = Socialite::driver($driver)->user();

      $create = User::firstOrCreate([
        'email' => $user->getEmail()
      ], [
        'socialite_name' => $driver,
        'socialite_id' => $user->getId(),
        'name' => $user->getName(),
        'photo' => $user->getAvatar(),
      ]);

      auth()->login($create, true);
      return redirect($this->redirectPath());
    } catch (\Exception $e) {
      return redirect()->route('login');
    }
  }
}
