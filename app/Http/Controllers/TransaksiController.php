<?php

namespace App\Http\Controllers;

use App\RefStatus;
use App\Transaksi;
use App\TransaksiDetail;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
  public static function getTransaksi($trn_id)
  {
    $trn = Transaksi::leftJoin('users','users.id','=','transaksi.id_pelanggan')
      ->leftJoin('mst_address','mst_address.id','=','transaksi.id_alamat_kirim')
      ->leftJoin('ref_waktu_kirim','ref_waktu_kirim.id','=','transaksi.id_waktu_kirim')
      ->where('transaksi.id', $trn_id)
      ->select(
        'transaksi.id',
        'transaksi.no_pesanan',
        'transaksi.total_harga',
        'mst_address.label',
        'mst_address.address',
        'mst_address.village_id',
        'ref_waktu_kirim.keterangan as waktu_kirim',
        'transaksi.catatan',
        'transaksi.estimasi_pengiriman',
        'transaksi.metode_bayar',
        'users.name as nama',
        'users.email',
        'transaksi.created_at',
        'transaksi.status'
      )
      ->first();

    $trn['region'] = LocationController::getRegion($trn['village_id']);
    $trn['tgl_pesan'] = HelperController::generateMonthName($trn['created_at']);
    $trn['flag_status'] = $trn['status'];
    $trn['status'] = RefStatus::where('jenis','beli')->where('status', $trn['status'])->first()['keterangan2'];
    $trn['qty_total'] = 0;

    $trnd = TransaksiDetail::leftJoin('mst_produk','mst_produk.id','transaksi_detail.id_barang')
      ->where('transaksi_detail.id_transaksi', $trn_id)
      ->select(
        'mst_produk.id as id_produk',
        'mst_produk.nama',
        'mst_produk.satuan',
        'transaksi_detail.harga',
        'transaksi_detail.qty',
        'transaksi_detail.status',
        'transaksi_detail.total_harga'
      )
      ->get();

    for($i=0; $i<count($trnd); $i++){
      $trnd[$i]['foto'] = ProdukController::getFotoProduk($trnd[$i]['id_produk']);
      $trn['qty_total'] += $trnd[$i]['qty'];
    }

    return [
      'trn' => $trn,
      'trnd'=> $trnd
    ];
  }
}
