<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvoiceController extends Controller
{
  public static function generateInvoice($trn_id, $data = [])
  {
    if($trn_id != null){
      $data = TransaksiController::getTransaksi($trn_id);
    }
    $pdf = \App::make('dompdf.wrapper');
    $pdf->setPaper('a4', 'portrait');
    $pdf->loadView('pdf.invoice', $data);
//    return $pdf->output();
    return $pdf->stream('Bukti pengeluaran.pdf');
//    return view('pdf.invoice');
  }
}
