<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArtikelController extends Controller
{
  public function viewAdd()
  {
    $data = false;
    return view('pages.article.admin.add')
      ->with('data', $data);
  }

  public function viewEdit($id)
  {
    $get = Artikel::where('id', $id)->first();
    return view('pages.article.admin.add')
      ->with('data', $get);
  }

  public function all()
  {
    $get = Artikel::leftJoin('users','users.id','=','artikel.created_by')
      ->leftJoin('sys_status','sys_status.id','=','artikel.status')
      ->select(
        'artikel.id',
        'artikel.judul',
        'artikel.kategori',
        'sys_status.keterangan as status',
        'artikel.hits',
        'users.name as created_by'
      )
      ->get();

    return view('pages.article.admin.all')
      ->with('data', $get);
  }

  public function store(Request $req)
  {
    $this->validateArtikel();

    $new = new Artikel();
    $new->judul       = $req->judul;
    $new->isi         = $req->isi_artikel;
    $new->kategori    = $req->kategori;
    $new->created_by  = Auth::user()->id;
    $new->thumbnail   = $req->thumbnail;
    $new->save();

    if($req->pdf_file) {
      $file = $req->pdf_file;
      $ext = $file->getClientOriginalExtension();
      $name = $file->getClientOriginalName();
      $format = ['pdf'];
      if (!in_array($ext, $format)) return [0, 'PDF tidak valid'];
      $file->move(public_path() . '/pdf/', $name);

      Artikel::where('id', $new->id)->update([
        'pdf' => $name
      ]);
    }

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function update(Request $req)
  {
    $this->validateArtikel();

    Artikel::where('id', $req->id)->update([
      'judul'     => $req->judul,
      'isi'       => $req->isi_artikel,
      'kategori'  => $req->kategori
    ]);

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function delete($id)
  {
    Artikel::where('id', $id)->delete();
    return back()->with('success', 'Data berhasil dihapus');
  }

  public function getTglArtikel($txt)
  {
    $time = strtotime($txt);
    $nama = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    return date('d ', $time).$nama[(int)date('m', $time)-1].date(' Y');
  }

  public function getImgArtikel($txt, $thumbnail = null)
  {
    if($thumbnail != null) return $thumbnail;

    $exp = explode('<img src="', $txt);
    if(count($exp) >= 2){
      return explode('"', $exp[1])[0];
    }
    return false;
  }
  public function getIsiArtikel($txt, $max = 300){
    $text = strip_tags($txt, "<style>");
    $subs = substr($text,strpos($text,"<style"),strpos($text,"</style>"));
    $text = str_replace($subs,"",$text);
    $text = str_replace(array("\t","\r","\n"),"",$text);
    $text = str_replace("&nbsp;", " ", $text);
    $text = trim($text);
    return strlen($text) > $max ? substr($text,0,$max)."..." : $text;
  }
  public function healthyInfo()
  {
    $get = Artikel::where('status','1')
      ->where('pdf', null)
      ->orderBy('id','DESC')
      ->get();

    $ret = [];
    for($i=0; $i<count($get); $i++){
      $ret[] = [
        'id'    => $get[$i]['id'],
        'img'   => $this->getImgArtikel($get[$i]['isi'], $get[$i]['thumbnail']),
        'tgl'   => $this->getTglArtikel($get[$i]['created_at']),
        'judul' => $get[$i]['judul'],
        'isi'   => $this->getIsiArtikel($get[$i]['isi'])
      ];
    }

    return view('pages.article.healthy_info')
      ->with('data', $ret);
  }

  public function searchHealthyInfo($keywords)
  {
    $keywords_explode = explode(' ', $keywords);
    $keywords_regexp = count($keywords_explode) > 1 ? implode('|', $keywords_explode) : $keywords_explode[0];
    $artikel = Artikel::where('judul','like','%'.$keywords.'%')
      ->orWhere('isi','regexp', $keywords_regexp)
      ->get();

    $ret = [];
    for($i=0; $i<count($artikel); $i++){
      if($artikel[$i]['pdf'] == null){
        $ret[] = [
          'id'    => $artikel[$i]['id'],
          'img'   => $this->getImgArtikel($artikel[$i]['isi'], $artikel[$i]['thumbnail']),
          'tgl'   => $this->getTglArtikel($artikel[$i]['created_at']),
          'judul' => $artikel[$i]['judul'],
          'isi'   => $this->getIsiArtikel($artikel[$i]['isi'])
        ];
      }
    }

    return view('pages.article.search_result')
      ->with('keywords', $keywords)
      ->with('data', $ret);
  }

  public function healthyInfoRead($id)
  {
    $get = Artikel::where('id', $id)->first();
    $get['tgl'] = $this->getTglArtikel($get['created_at']);

    Artikel::where('id', $id)->update([
      'hits' => $get['hits'] += 1
    ]);

    $get['tag'] = $get['kategori'] != null ? explode(',', $get['kategori']) : [];

    return view('pages.article.healthy_info_read')
      ->with('artikel', $get)
      ->with('artikel_lainnya', $this->getArtikelLainnya());
  }

  private function getArtikelLainnya($fresh_edu = false)
  {
    $get = Artikel::where('status','1');

    if($fresh_edu){
      $get = $get->where('pdf', '!=', null);
    }
    else{
      $get = $get->where('pdf', null);
    }
    $get = $get->orderBy('id','DESC')
    ->limit(9)
    ->get();

    $ret = [];
    for($i=0; $i<count($get); $i++){
      $ret[] = [
        'id'    => $get[$i]['id'],
        'img'   => $this->getImgArtikel($get[$i]['isi'], $get[$i]['thumbnail']),
        'tgl'   => $this->getTglArtikel($get[$i]['created_at']),
        'judul' => $get[$i]['judul'],
        'isi'   => $this->getIsiArtikel($get[$i]['isi'], 100)
      ];
    }

    return $ret;
  }

  private function validateArtikel()
  {
    return request()->validate([
      'judul'=> 'required|max:255',
      'isi_artikel'=> 'required',
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }

  public function freshEdu()
  {
    $get = Artikel::where('status','1')
      ->where('pdf', '!=', null)
      ->orderBy('id','DESC')
      ->get();

    $ret = [];
    for($i=0; $i<count($get); $i++){
      $ret[] = [
        'id'    => $get[$i]['id'],
        'img'   => $this->getImgArtikel($get[$i]['isi'], $get[$i]['thumbnail']),
        'tgl'   => $this->getTglArtikel($get[$i]['created_at']),
        'judul' => $get[$i]['judul'],
        'isi'   => $this->getIsiArtikel($get[$i]['isi'])
      ];
    }

    return view('pages.article.fresh_edu')
      ->with('data', $ret);
  }

  public function searchFreshEdu($keywords)
  {
    $keywords_explode = explode(' ', $keywords);
    $keywords_regexp = count($keywords_explode) > 1 ? implode('|', $keywords_explode) : $keywords_explode[0];
    $artikel = Artikel::where('judul','like','%'.$keywords.'%')
      ->orWhere('isi','regexp', $keywords_regexp)
      ->get();

    $ret = [];
    for($i=0; $i<count($artikel); $i++){
      if($artikel[$i]['pdf'] != null){
        $ret[] = [
          'id'    => $artikel[$i]['id'],
          'img'   => $this->getImgArtikel($artikel[$i]['isi'], $artikel[$i]['thumbnail']),
          'tgl'   => $this->getTglArtikel($artikel[$i]['created_at']),
          'judul' => $artikel[$i]['judul'],
          'isi'   => $this->getIsiArtikel($artikel[$i]['isi'])
        ];
      }
    }

    return view('pages.article.search_result')
      ->with('keywords', $keywords)
      ->with('data', $ret);
  }

  public function freshEduRead($id)
  {
    $get = Artikel::where('id', $id)->first();
    $get['tgl'] = $this->getTglArtikel($get['created_at']);

    Artikel::where('id', $id)->update([
      'hits' => $get['hits'] += 1
    ]);

    $get['tag'] = $get['kategori'] != null ? explode(',', $get['kategori']) : [];

    return view('pages.article.fresh_edu_read')
      ->with('artikel', $get)
      ->with('artikel_lainnya', $this->getArtikelLainnya(true));
  }
}
