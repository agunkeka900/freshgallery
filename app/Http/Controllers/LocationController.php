<?php

namespace App\Http\Controllers;

use App\RefCity;
use App\RefDistrict;
use App\RefVillage;
use Illuminate\Http\Request;

class LocationController extends Controller
{
  public static function getCity($provId = null)
  {
    $get = RefCity::leftJoin('ref_province', 'ref_province.id','=','ref_city.province_id');

    if($provId !== null)
      $get = $get->where('ref_province.id', $provId);

    $get = $get->select(
        'ref_province.name as province',
        'ref_city.name as city',
        'ref_city.id'
      )
      ->get();

    $arr = [];
    for($i=0; $i<count($get); $i++){
      $arr[] = [
        'id'  => $get[$i]['id'],
        'text' => $get[$i]['province'].', '.$get[$i]['city']
      ];
    }

    return json_encode($arr);
  }

  public static function getVillage($provId = 51)
  {
    $get = RefVillage::leftJoin('ref_district', 'ref_district.id', '=','ref_village.district_id')
      ->leftJoin('ref_city','ref_city.id','=','ref_district.city_id')
      ->leftJoin('ref_province','ref_province.id','=','ref_city.province_id');

    if($provId != null)
      $get = $get->where('ref_province.id', $provId);

    $get = $get->select(
        'ref_province.name as province',
        'ref_city.name as city',
        'ref_district.name as district',
        'ref_village.name as village',
        'ref_village.id as no'
      )
      ->get();

    $arr = [];
    for($i=0; $i<count($get); $i++){
      $g = $get[$i];
      $arr[] = [
        'id'  => $g['no'],
        'text' => implode(', ', [$g['province'],$g['city'],$g['district'],$g['village']])
      ];
    }

    return json_encode($arr);
  }

  public static function getIdDistrictCityProvince($village_id)
  {
    $district_id = RefVillage::where('id', $village_id)->first()['district_id'];
    $city_id = RefDistrict::where('id', $district_id)->first()['city_id'];
    $province_id = RefCity::where('id', $city_id)->first()['province_id'];

    return [
      'district_id' => $district_id,
      'city_id' => $city_id,
      'province_id' => $province_id
    ];
  }

  public static function getRegion($village_id) : string
  {
    $r = RefVillage::leftJoin('ref_district','ref_district.id','=','ref_village.district_id')
      ->leftJoin('ref_city','ref_city.id','=','ref_district.city_id')
      ->leftJoin('ref_province','ref_province.id','=','ref_city.province_id')
      ->select(
        'ref_village.name as village',
        'ref_district.name as district',
        'ref_city.name as city',
        'ref_province.name as province'
      )
      ->where('ref_village.id', $village_id)
      ->first();

    return $r['province'].', '.$r['city'].', '.$r['district'].', '.$r['village'];
  }
}
