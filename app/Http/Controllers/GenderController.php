<?php

namespace App\Http\Controllers;

use App\RefGender;
use Illuminate\Http\Request;

class GenderController extends Controller
{
  public function getGender ()
  {
    return RefGender::all();
  }
}
