<?php

namespace App\Http\Controllers;

use App\Transaksi;
use App\TransaksiDetail;
use App\User;
use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
  public static function stockAvailable($trn_id)
  {
    $data = TransaksiController::getTransaksi($trn_id);

    Mail::send('email.stock_available', $data, function ($message) use ($data) {
      $message->to($data['trn']['email'], $data['trn']['nama'])
        ->subject('Pesanan telah dikonfirmasi');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
    });

    Transaksi::where('id', $trn_id)->update([
      'flag_email' => 1
    ]);
  }
  public static function stockNotAvailable($trn_id)
  {
    $data = TransaksiController::getTransaksi($trn_id);

    Mail::send('email.stock_not_available', $data, function ($message) use ($data) {
      $message->to($data['trn']['email'], $data['trn']['nama'])
        ->subject('Produk tidak tersedia!');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
    });

    Transaksi::where('id', $trn_id)->update([
      'flag_email' => 1
    ]);
  }
  public static function someStockNotAvailable($trn_id)
  {
    $data = TransaksiController::getTransaksi($trn_id);

    Mail::send('email.some_stock_not_available', $data, function ($message) use ($data) {
      $message->to($data['trn']['email'], $data['trn']['nama'])
        ->subject('Beberapa produk tidak tersedia!');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
    });

    Transaksi::where('id', $trn_id)->update([
      'flag_email' => 1
    ]);
  }

  public static function orderCompleted($trn_id)
  {
    $data = TransaksiController::getTransaksi($trn_id);

    Mail::send('email.stock_available', $data, function ($message) use ($data) {
      $attachment = InvoiceController::generateInvoice(null, $data);
      $attachment_name = 'INVOICE '.$data['trn']['no_pesanan'].'.pdf';

      $message->to($data['trn']['email'], $data['trn']['nama'])
        ->subject('Pesanan telah dikonfirmasi');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
      $message->attachData($attachment, $attachment_name);
    });

    Transaksi::where('id', $trn_id)->update([
      'flag_email' => 2
    ]);
  }

  public static function changePassword($id)
  {
    $data = User::find($id);
    $user = [
      'name' => $data->name,
      'id' => $data->id,
      'email' => $data->email,
      'title' => 'Ubah Password'
    ];

    Mail::send('email.change_password', $user, function ($message) use ($user) {
      $message->to($user['email'], $user['name'])
        ->subject('Ubah Password');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
    });
  }

  public static function forgotPassword($email)
  {
    $data = User::where('email', $email)->first();
    $user = [
      'name' => $data->name,
      'id' => $data->id,
      'email' => $data->email,
      'title' => 'Reset Password'
    ];

    Mail::send('email.change_password', $user, function ($message) use ($user) {
      $message->to($user['email'], $user['name'])
        ->subject('Reset Password');
      $message->from('freshgallery.bali@gmail.com','Fresh Gallery');
    });
  }
}
