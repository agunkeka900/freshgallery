<?php

namespace App\Http\Controllers;

use App\HargaGrosir;
use App\MstFotoProduk;
use App\MstProduk;
use App\MstSupplier;
use App\RefJenisProduk;
use App\RelProdukSupplier;
use App\SysKategoriProduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
//  public function createSlug()
//  {
//    $get = MstProduk::all();
//
//    for ($i=0; $i<count($get); $i++){
//      MstProduk::where('id', $get[$i]['id'])->update([
//        'slug' => str_slug($get[$i]['nama'])
//      ]);
//    }
//
//    return 'okay';
//  }

  public function index()
  {
    $jenis = RefJenisProduk::all();
    $kategori = SysKategoriProduk::where('show_in_home', '1')->get();
    $data = [];

    for($i=0; $i<count($jenis); $i++){
      $data[] = [
        'jenis' => $jenis[$i]['keterangan'],
        'data'  => $this->getProduk($jenis[$i]['id'])
      ];
    }

    return view('pages.product.index')
      ->with('kategori', $kategori)
      ->with('data', $data);
  }

  public function detail($slug)
  {
    $init = MstProduk::find($slug) ?: abort('404');
    $init->view = $init['view'] += 1;
    $init->save();

    $id = $init->id;

    $data = MstProduk::leftJoin('ref_jenis_produk', 'ref_jenis_produk.id', '=', 'mst_produk.jenis')
      ->where('mst_produk.id', $id)
      ->orWhere('mst_produk.slug', $slug)
      ->select(
        'mst_produk.id',
        'mst_produk.nama',
        'mst_produk.keterangan',
        'mst_produk.stock',
        'mst_produk.harga',
        'mst_produk.view',
        'mst_produk.terjual',
        'mst_produk.min_beli',
        'mst_produk.satuan',
        'mst_produk.slug',
        'mst_produk.jenis as id_jenis',
        'ref_jenis_produk.keterangan as jenis'
      )
      ->first();

    $foto         = self::getFotoProduk($id, false);
    $data['foto'] = $foto[0]['foto'];
    $terkait      = $this->getProdukTerkait($data['nama'], $data['id_jenis']);
    $data['harga_grosir'] = HargaGrosir::where('id_produk', $id)->orderBy('qty','ASC')->get();

    return view('pages.product.detail')
      ->with('data', $data)
      ->with('foto', $foto)
      ->with('terkait', $terkait)
      ->with('id', $id);
  }

  public static function getFotoProduk($id_produk, $first = true)
  {
    $mst_foto = MstFotoProduk::where('id_produk', $id_produk)
      ->where('status','1')
      ->get();

    return $first ? $mst_foto[0]['foto'] : $mst_foto;
  }

  public function viewDetailAdmin($id)
  {
    return $id;
  }

  public function categorySelected($slug)
  {
    $init = SysKategoriProduk::where('slug', $slug)->orWhere('param', $slug)->first() ?: abort('404');
    $param = $init['param'];

    $jenis = strpos($param, ',') === false ? [$param] : explode(',', $param);
    $data = [];

    for($i=0; $i<count($jenis); $i++){
      if(in_array($jenis[$i], ['baru','terlaris']))
        $temp = $this->getProduk(null, $this->getArrBaruTerlaris($jenis[$i]));
      else
        $temp = $this->getProduk($jenis[$i]);

      $data[] = [
        'jenis' => SysKategoriProduk::where('param', $jenis[$i])->first()['nama'],
        'data'  => $temp
      ];
    }

    return view('pages.product.category_selected')
      ->with('data', $data);
  }
  public function categoryList()
  {
    $data = SysKategoriProduk::where('show_in_all', '1')->get();

    return view('pages.product.category_list')
      ->with('data', $data);
  }

  private function getArrBaruTerlaris($jenis)
  {
    $order = $jenis == 'baru' ? 'created_at' : 'terjual';
    $get = MstProduk::orderBy($order, 'DESC')
      ->limit(20)
      ->get();

    $ret = [];
    foreach ($get as $g) $ret[] = $g['id'];
    return $ret;
  }

  public function getProduk($jenis = null, $arrID = []){
    $get = MstProduk::leftJoin('ref_jenis_produk', 'ref_jenis_produk.id', '=', 'mst_produk.jenis');

    if($jenis != null && !is_array($jenis))
      $get = $get->where('mst_produk.jenis', $jenis);
    elseif(is_array($jenis))
      $get = $get->whereIn('mst_produk.jenis', $jenis);
    if(count($arrID))
      $get = $get->whereIn('mst_produk.id', $arrID);

    $get = $get->select(
      'mst_produk.id',
      'mst_produk.nama',
      'mst_produk.keterangan',
      'mst_produk.stock',
      'mst_produk.harga',
      'mst_produk.satuan',
      'mst_produk.tempat',
      'mst_produk.slug',
      'ref_jenis_produk.keterangan as jenis'
    )
    ->where('mst_produk.stock','!=','0')
    ->get();

    for($i=0; $i<count($get); $i++){
      $get[$i]['foto'] = self::getFotoProduk($get[$i]['id']);
      $get[$i]['harga_grosir'] = HargaGrosir::where('id_produk', $get[$i]['id'])->orderBy('qty', 'ASC')->get();
    }

    if(count($arrID)){
      $temp = [];
      for($i=0; $i<count($arrID); $i++){
        for($a=0; $a<count($get); $a++){
          if($arrID[$i] == $get[$a]['id']){
            $temp[] = $get[$a];
            break;
          }
        }
      }
      $get = $temp;
    }

    return $get;
  }

  public function getProdukTerkait($nama, $jenis, $notNama = true, $length = 5)
  {
    $get = MstProduk::select('id','nama','jenis');
    if($notNama) $get = $get->where('nama', '!=', $nama);
    $get = $get->where('stock','!=',0)->get();

    $arrTemp = [];

    foreach ($get as $g){
      similar_text($nama, $g['nama'], $pcnNama);
      $arrTemp[] = $g;
      $arrTemp[count($arrTemp)-1]['percent'] = $pcnNama;
      if($jenis){
        similar_text($jenis, $g['jenis'], $pcnJenis);
        $arrTemp[count($arrTemp)-1]['percent'] += $pcnJenis;
      }
    }

    usort($arrTemp, function($a, $b) {
      return $a['percent'] <=> $b['percent'];
    });

    $ret = [];
    $cArr = count($arrTemp);

    try {
      for($i=$cArr-1; $i>=$cArr-$length; $i--){
        $ret[] = $arrTemp[$i]['id'];
      }
    }
    catch (\Exception $e){}

    return $this->getProduk(null, $ret);
  }

  public function search($key)
  {
    $key = urldecode($key);

    $jenis = RefJenisProduk::where('keterangan', 'like', '%'.$key.'%')->get();
    $arrJenis = [];

    foreach($jenis as $j) $arrJenis[] = $j['id'];

    $mst = MstProduk::where('nama', 'like', '%'.$key.'%')
      ->orWhereIn('jenis', $arrJenis)
      ->orWhere('keterangan', 'like', '%'.$key.'%')
      ->orWhere('harga', 'like', '%'.$key.'%')
      ->orWhere('satuan', 'like', '%'.$key.'%')
      ->orWhere('tempat', 'like', '%'.$key.'%')
      ->get();

    $ret = [];

    foreach ($mst as $m) $ret[] = $m['id'];

    $data = count($ret) ? $this->getProduk(null, $ret) : [];
    $len = count($data);
    $terkait = !$len ? $this->getProdukTerkait($key, false, false, 10) : [];

    return view('search_result')
      ->with('len', $len)
      ->with('data', $data)
      ->with('terkait', $terkait);

  }
  public function searchRecommended($key)
  {
    $key = urldecode($key);
    $result = $this->getProdukTerkait($key, false, false);
    return response()->json($result);
  }
}
