<?php

namespace App\Http\Controllers;

use App\Seq;
use Illuminate\Http\Request;

class SeqController extends Controller
{
  public static function getCheckoutIdPesanan()
  {
    $current_date = date('Ymd');
    $init = Seq::where('type', 'transaksi')
      ->where('description', 'TRN/'.$current_date)
      ->first();

    if($init){
      $value = $init['value']+1;
      Seq::where('id', $init['id'])->update([
        'value' => $value
      ]);
    }
    else{
      $value = 1;
      $new_seq = new Seq();
      $new_seq->type = 'transaksi';
      $new_seq->description = 'TRN/'.$current_date;
      $new_seq->value = $value;
      $new_seq->save();
    }
    return 'TRN/'.$current_date.'/'.sprintf('%04d', $value);
  }
}
