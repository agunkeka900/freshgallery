<?php

namespace App\Http\Controllers;

use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
  public static function getNotif($id_user = null)
  {
    if($id_user == null) $id_user = Auth::user()->id;

    $periksa  = Transaksi::where('id_pelanggan', $id_user)->where('status', '1')->count();
    $proses   = Transaksi::where('id_pelanggan', $id_user)->where('status', '2')->count();
    $dikirim  = Transaksi::where('id_pelanggan', $id_user)->where('status', '7')->count();
    $selesai  = Transaksi::where('id_pelanggan', $id_user)->where('status', '4')->count();

    return [
      'periksa' => $periksa,
      'proses'  => $proses,
      'dikirim' => $dikirim,
      'selesai' => $selesai,
      'total'   => $periksa + $proses + $dikirim + $selesai
    ];
  }
}
