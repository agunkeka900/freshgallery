<?php

namespace App\Http\Controllers;

use App\HargaGrosir;
use App\MstFotoProduk;
use App\MstProduk;
use App\MstSupplier;
use App\RefJenisProduk;
use App\RelProdukSupplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProdukAdmCtr extends Controller
{
  public function createSlug($data)
  {
    function check($slug){
      return MstProduk::where('slug', $slug)->first();
    }

    $slug = str_slug($data['nama_produk']);
    if(check($slug))
      $slug .= '-'.$data['harga'];
    elseif($slug)
      $slug .= '-'.time();
    return $slug;
  }

  public function all()
  {
    $get = MstProduk::leftJoin('ref_jenis_produk', 'ref_jenis_produk.id', '=', 'mst_produk.jenis')
      ->select(
        'mst_produk.id',
        'mst_produk.nama',
        'mst_produk.keterangan',
        'mst_produk.stock',
        'mst_produk.harga',
        'mst_produk.satuan',
        'mst_produk.tempat',
        'ref_jenis_produk.keterangan as jenis'
      )
      ->orderBy('jenis')
      ->get();

    for($i=0; $i<count($get); $i++){
      $rel = RelProdukSupplier::leftJoin('mst_supplier', 'mst_supplier.id', 'rel_produk_supplier.id_supplier')
        ->where('rel_produk_supplier.id_produk', $get[$i]['id'])
        ->where('mst_supplier.status','1')
        ->select('mst_supplier.nama')
        ->get();
      $supp =[];
      foreach ($rel as $r){
        $supp[] = $r['nama'];
      }

      $stringSupp = count($supp) > 0 ? implode(', ', $supp) : '';

      $get[$i]['supplier'] = strlen($stringSupp) > 30 ? substr($stringSupp, 0, 30).' ...' : $stringSupp;
      $get[$i]['harga'] = number_format($get[$i]['harga'], 0, ',', '.');
    }

    return view('pages.product.admin.all')
      ->with('data', $get);
  }

  public function viewAdd()
  {
    $itemSupp = MstSupplier::where('status','1')->get();
    $itemJenis = RefJenisProduk::all();

    return view('pages.product.admin.add')
      ->with('itemSupp', $itemSupp)
      ->with('itemJenis', $itemJenis);
  }

  public function viewEdit($id)
  {
    $get = MstProduk::where('id', $id)->first();
    $foto= MstFotoProduk::where('id_produk', $id)
      ->where('status','1')
      ->get();
    $itemSupp = MstSupplier::where('status','1')->get();
    $itemJenis = RefJenisProduk::all();
    $itemGrosir = HargaGrosir::where('id_produk', $id)->get();
    $supp = [];

    foreach (RelProdukSupplier::where('id_produk', $id)->get() as $s){
      $supp[] = $s['id_supplier'];
    }
    for($i=0; $i<count($itemSupp); $i++){
      $itemSupp[$i]['check'] = in_array($itemSupp[$i]['id'], $supp);
    }
    $jenisAva = false;
    for($i=0; $i<count($itemJenis); $i++){
      $jenisCheck = $itemJenis[$i]['id'] == $get['jenis'];
      $itemJenis[$i]['check'] = $jenisCheck;
      if($jenisCheck) $jenisAva = true;
    }
    if(!$jenisAva) $get['jenis'] = null;

    return view('pages.product.admin.edit')
      ->with('id', $id)
      ->with('produk', $get)
      ->with('foto', $foto)
      ->with('itemSupp', $itemSupp)
      ->with('itemGrosir', $itemGrosir)
      ->with('itemJenis', $itemJenis);
  }

  public function store(Request $req)
  {
//    dd($req);
    $this->validateProduk();

    for($i=1; $i<=5; $i++){
      if($req->file('foto'.$i)){
        $file = $req->file('foto'.$i);
        $ext  = $file->getClientOriginalExtension();
        $format = ['png','jpeg','jpg'];
        if(!in_array($ext, $format)) return 'Foto tidak valid';
      }
    }

    for($i=1; $i<=5; $i++){
      if($req->file('foto'.$i)){
        $file = $req->file('foto'.$i);
        $ext  = $file->getClientOriginalExtension();
        $namaFoto = substr(md5(time()), 0, 31).$i.'.'.$ext;
        $file->move(public_path().'/img_produk', $namaFoto);

        $newFoto = new MstFotoProduk();
        $newFoto->id_produk = $req->id;
        $newFoto->foto = $namaFoto;
        $newFoto->save();
      }
    }

    $new = new MstProduk();
    $new->nama = $req->nama_produk;
    $new->jenis = $req->jenis;
    $new->keterangan = $req->keterangan;
    $new->stock = 0;
    $new->harga = $req->harga ?: 0;
    $new->satuan = $req->satuan;
    $new->tempat = $req->tempat;
    $new->slug   = $this->createSlug($req);
    $new->created_by = Auth::user()->id;
    $new->save();

    if(isset($req->harga_grosir_check)){
      for($i=1; $i<=5; $i++){
        if(($req['qty_grosir_'.$i] != null || $req['qty_grosir_'.$i] != 0) && ($req['harga_grosir_'.$i] != null || $req['harga_grosir_'.$i] != 0)) {
          $new_grosir = new HargaGrosir();
          $new_grosir->id_produk = $new->id;
          $new_grosir->qty = $req['qty_grosir_'.$i];
          $new_grosir->harga = $req['harga_grosir_'.$i];
          $new_grosir->save();
        }
      }
    }

    $supp = $req->supplier ?: [];
    foreach ($supp as $s){
      $newRel = new RelProdukSupplier();
      $newRel->id_produk = $new->id;
      $newRel->id_supplier = $s;
      $newRel->save();
    }

    return back()->with('success', 'Data berhasil disimpan');
  }

  public function delete($id)
  {
    MstProduk::where('id', $id)->delete();
    RelProdukSupplier::where('id_produk', $id)->delete();

    foreach (MstFotoProduk::where('id_produk', $id)->get() as $f){
      unlink(public_path().'/img_produk/'.$f['foto']);
    }
    MstFotoProduk::where('id_produk', $id)->delete();

    return redirect('produk/admin');
  }

  private function deletePhoto($string_id)
  {
    $arr_id = json_decode($string_id);
    for($i=0; $i<count($arr_id); $i++){
        MstFotoProduk::where('id', $arr_id[$i])
          ->update(['status' => '0']);
    }
  }

  public function update(Request $req)
  {
//    dd($req);
    $this->validateProduk();
    $this->deletePhoto($req->deleted_photo);

    for($i=1; $i<=5; $i++){
      if($req->file('foto'.$i)){
        $file = $req->file('foto'.$i);
        $ext  = $file->getClientOriginalExtension();
        $format = ['png','jpeg','jpg'];
        if(!in_array($ext, $format)) return 'Foto tidak valid';
      }
    }

    for($i=1; $i<=5; $i++){
      if($req->file('foto'.$i)){
        $file = $req->file('foto'.$i);
        $ext  = $file->getClientOriginalExtension();
        $namaFoto = substr(md5(time()), 0, 31).$i.'.'.$ext;
        $file->move(public_path().'/img_produk', $namaFoto);

        $newFoto = new MstFotoProduk();
        $newFoto->id_produk = $req->id;
        $newFoto->foto = $namaFoto;
        $newFoto->save();
      }
    }

    MstProduk::where('id', $req->id)->update([
      'nama'        => $req->nama_produk,
      'jenis'       => $req->jenis,
      'keterangan'  => $req->keterangan,
      'stock'       => $req->stock,
      'harga'       => $req->harga,
      'satuan'      => $req->satuan,
      'tempat'      => $req->tempat
    ]);

    $oldSupp = [];
    $newSupp = $req->supplier ?: [];
    foreach (RelProdukSupplier::where('id_produk', $req->id)->get() as $s){
      $oldSupp[] = $s['id_supplier'];
    }

    for($i=0; $i<count($oldSupp); $i++){
      if(!in_array($oldSupp[$i], $newSupp)){
        RelProdukSupplier::where('id_produk', $req->id)
          ->where('id_supplier', $oldSupp[$i])
          ->delete();
      }
    }

    for($i=0; $i<count($newSupp); $i++){
      if(!in_array($newSupp[$i], $oldSupp)){
        $newRel = new RelProdukSupplier();
        $newRel->id_produk = $req->id;
        $newRel->id_supplier = $newSupp[$i];
        $newRel->save();
      }
    }

    HargaGrosir::where('id_produk', $req->id)->delete();
    if(isset($req->harga_grosir_check)){
      for($i=1; $i<=5; $i++){
        if(($req['qty_grosir_'.$i] != null || $req['qty_grosir_'.$i] != 0) && ($req['harga_grosir_'.$i] != null || $req['harga_grosir_'.$i] != 0)) {
          $new_grosir = new HargaGrosir();
          $new_grosir->id_produk = $req->id;
          $new_grosir->qty = $req['qty_grosir_'.$i];
          $new_grosir->harga = $req['harga_grosir_'.$i];
          $new_grosir->save();
        }
      }
    }

    return back()->with('success', 'Data berhasil disimpan');
  }

  private function validateProduk()
  {
    return request()->validate([
      'nama_produk'=> 'required|max:255',
      'jenis'=> 'required|max:1',
      'satuan'=> 'max:255',
      'tempat'=> 'max:255',
    ], [
      'required' => ':attribute harus diisi!',
      'max' => ':attribute maksimal :max karakter!',
      'email' => 'Email tidak valid!',
      'numeric' => ':attribute harus berupa angka!'
    ]);
  }
}
