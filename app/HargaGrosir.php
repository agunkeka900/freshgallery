<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id_produk
 * @property mixed qty
 * @property mixed harga
 */
class HargaGrosir extends Model
{
    protected $table = 'harga_grosir';

}
