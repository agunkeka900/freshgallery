<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id_user
 * @property mixed id_produk
 * @property mixed jumlah
 */
class Cart extends Model
{
    protected $table = 'cart';
}
