<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefDistrict extends Model
{
    protected $table = 'ref_district';
}
