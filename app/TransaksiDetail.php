<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id_transaksi
 * @property int id_barang
 * @property int id_supplier
 * @property int harga
 * @property int qty
 * @property float|int total_harga
 */
class TransaksiDetail extends Model
{
    protected $table = 'transaksi_detail';
}
