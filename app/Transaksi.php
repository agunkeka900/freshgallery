<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id_pelanggan
 * @property float|int total_harga
 * @property mixed id_alamat_kirim
 * @property string id_waktu_kirim
 * @property null|string metode_bayar
 * @property mixed catatan
 * @property string no_pesanan
 * @property string estimasi_pengiriman
 */
class Transaksi extends Model
{
    protected $table = 'transaksi';
}
