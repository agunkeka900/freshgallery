<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string type
 * @property string description
 * @property int value
 */
class Seq extends Model
{
    protected $table = 'seq';
}
