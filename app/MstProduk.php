<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string nama
 * @property integer id_supplier
 * @property integer jenis
 * @property string keterangan
 * @property integer stock
 * @property integer harga
 * @property integer created_by
 * @property mixed satuan
 * @property mixed tempat
 * @property string slug
 */
class MstProduk extends Model
{
  protected $table = 'mst_produk';
//  protected $primaryKey = 'slug';

//  public function getRouteKeyName() {
//    return 'id';
//    return 'slug';
//  }

  public static function find($slugOrId)
  {
    return MstProduk::where('id', $slugOrId)
      ->orWhere('slug', $slugOrId)
      ->first();
  }
}
