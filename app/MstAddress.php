<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string province_id
 * @property string city_id
 * @property string district_id
 * @property string village_id
 * @property string label
 * @property string address
 * @property string latitude
 * @property string longitude
 * @property int primary
 * @property string id_user
 */
class MstAddress extends Model
{
    protected $table = 'mst_address';
}
