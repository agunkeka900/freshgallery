<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed keterangan
 */
class RefJenisProduk extends Model
{
    protected $table = 'ref_jenis_produk';
}
