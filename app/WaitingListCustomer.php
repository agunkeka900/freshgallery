<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaitingListCustomer extends Model
{
  protected $table = 'waiting_list_customer';
}
