<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaitingListProduct extends Model
{
  protected $table = 'waiting_list_product';
}
