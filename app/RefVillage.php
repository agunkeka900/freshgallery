<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefVillage extends Model
{
    protected $table = 'ref_village';
}
