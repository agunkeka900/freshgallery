<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysKategoriProduk extends Model
{
    protected $table = 'sys_kategori_produk';
}
