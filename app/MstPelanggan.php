<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstPelanggan extends Model
{
    protected $table = 'mst_pelanggan';
}
