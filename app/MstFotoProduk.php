<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id_produk
 * @property string foto
 */
class MstFotoProduk extends Model
{
    protected $table = 'mst_foto_produk';
}
