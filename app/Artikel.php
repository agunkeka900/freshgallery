<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed judul
 * @property mixed isi
 * @property integer created_by
 * @property mixed kategori
 */
class Artikel extends Model
{
    protected $table = 'artikel';
}
