<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed nama
 * @property integer id_user
 * @property mixed alamat
 * @property mixed deskripsi
 * @property mixed telp
 * @property mixed kota
 */
class MstSupplier extends Model
{
    protected $table = 'mst_supplier';
}
