<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id_produk
 * @property int id_supplier
 */
class RelProdukSupplier extends Model
{
    protected $table = 'rel_produk_supplier';
}
