<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefGender extends Model
{
    protected $table = 'ref_gender';
}
